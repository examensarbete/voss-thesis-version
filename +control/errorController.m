function r = errorController(controllerVec, order, errorVec, hVec, unit)
%Calculates the new step size ratio r.
%
%Uses previous errors and step size ratios to calculate a new step size
%ratio using a user supplied filter.
%
%Parameters
%----------
%controllerVec : vector
%   The vector of filter parameters: [gamma_1, gamma_2, ...,
%   gamma_k -alpha_2, -alpha_3, ..., -alpha_k].
%order : int
%   The order of the method used.
%errorVec: vector
%   A row matrix containing the k previous errors, where k is the number of
%   gammas. The elements should be ordered according to: [e_{n-k-1}, ...,
%   e_{n-1}, e_{n}] where e_{n} is the latest error.
%hVec : vector
%   A row matrix containing the k previous step sizes, where k is the number of
%   gammas. The elements should be ordered according to: [h_{n-k-1}, ...,
%   h_{n-1}, h_{n}] where h_{n} is the latest step size.
%unit : boolean
%   If true, "error per unit step" is used, else, "error per step" is used.
%
%Returns
%-------
%r : float
%   The new stepsize ratio r.

errorVec = flip(errorVec);
hVec = flip(hVec);

p = order+1-unit;
if unit == 1
    errorVec = errorVec./hVec;
end
scaledErrorVec = (1./(errorVec + 1e-16)).^(1/p);
stepSizeRatios = hVec(1:end-1)./hVec(2:end);
temp1 = [scaledErrorVec stepSizeRatios];
temp2 = temp1.^controllerVec;
r = prod(temp2);
end
