function [parvec] = getFilterVec(filter, varargin)
%Creates a vector containing the filter parameters
%
%Parameters
%----------
%filter : string
%   The name of the filter to use. The following filters exist:
%   
%  - fixedStepSize
%  - H211D
%  - H211b
%  - H211PI
%  - PI3333
%  - PI3040
%  - PI4020
%
%  - H312D
%  - H312b
%  - H312PID
%  - H321D
%  - H321
%  - PID
%
%varargin : options list
%   Depending on which filter is chosen, a number of parameters may be
%   supplied:
%
%   - fixedStepSize, H211D, H211b, H211PI, PI3333, PI3040, PI4020, H312D,
%                                                   H312PID, H321D, H321:
%
%       No parameters
%
%   - H211b, H312b:
%
%       'b'
%           A non-default value of the b parameter
%
%   - PID:
%       
%       'kI'
%           The coefficient for the integral term
%       'kP'
%           The coefficient for the proportional term
%       'kD'
%           The coefficient for the derivative term
%       'order'
%           The order of the numerical method which is to be used in
%           conjunction with the filter
%
%Returns
%-------
%parvec : row vector
%   vector containing the filter parameters (real values)

switch filter
    %Fixed step size (choose this if you want fixed step size instead
    %of variable step size)
    case 'fixedStepSize'
        parvec = [0];
    %Second order filters
    case 'H211D'
        parvec = [1 1 -1]/2;
    case 'H211b'
        b = 4; %default value of 'b'
        if(length(varargin) == 2)
            b = varargin{2};
        end
        parvec = [1 1 -1]/b;
    case 'H211PI'
        parvec = [1 1 0]/6;
    case 'PI3333'
        parvec = [2 -1 0]/3;
    case 'PI3040'
        parvec = [7 -4 0]/10;
    case 'PI4020'
        parvec = [3 -1 0]/5;
    %Third order filters
    case 'H312D'
        parvec = [1 2 1 -3 -1]/4;
    case 'H312b'
        b = 8; %default value of 'b'
        if(length(varargin) == 2)
            b = varargin{2};
        end
        parvec = [1 2 1 -3 -1]/b;
    case 'H312PID'
        parvec = [1 2 1 0 0]/18;
    case 'H321D'
        parvec = [5 2 -3 1 3]/4;
    case 'H321'
        parvec = [6 1 -5 15 3]/18;
    case 'PID'
        kI = -0.1;
        kP = -0.1;
        kD = -0.2;
        order = 4;
        for i=1:length(varargin)-1
            name = varargin{i};
            value = varargin{i+1};
            switch name
                case 'kI'
                    kI = value; 
                case 'kP'
                    kP = value;
                case 'kD'
                    kD = value;
                case 'order'
                    order = value;
            end    
        end
        beta1 = kI + kP + kD;
        beta2 = -(kP + 2*kD);
        beta3 = kD;
        parvec = [beta1*order beta2*order beta3*order 0 0]
    otherwise
        ME = MException('Solver:NoSuchFilter',...
                sprintf('The filter "%s" does not exist.',filter));
        throw(ME);
end
end
