function jac = emep_jac(t,y)

[rc, dj, H2O] = ode.stiff.emep.emepcf(t);

%distribution of VOC emissions among species:
frac6 = 0.07689;
frac7 = 0.41444;
frac8 = 0.03642;
frac9 = 0.03827;
frac10 = 0.24537;
frac13 = 0.13957;

vekt6 = 30;
vekt7 = 58;
vekt8 = 28;
vekt9 = 42;
vekt10 = 106;
vekt13 = 46;

vmhc = 1/(frac6/vekt6 + frac7/vekt7 + frac8/vekt8 + frac9/vekt9 + ...
    frac10/vekt10 + frac13/vekt13);

%choose values for NOX and HC emissions in molecules/cm**2xs
emnox = 2.5e11;
emhc = 2.5e11;

%rural case
faciso = 1.0;
fachc = 1.0;
facnox = 1.0;

emis1 = emnox*facnox;
emis2 = 0;
emis3 = emnox*facnox;
emis4 = emhc*10*fachc;
emis5 = 0;
emis6 = emhc*frac6/vekt6*vmhc*fachc;
emis7 = emhc*frac7/vekt7*vmhc*fachc;
emis8 = emhc*frac8/vekt8*vmhc*fachc;
emis9 = emhc*frac9/vekt9*vmhc*fachc;
emis10 = emhc*frac10/vekt10*vmhc*fachc;
emis11 = 0.5*faciso*emhc;
emis12 = 0;
emis13 = emhc*frac13/vekt13*vmhc*fachc;

%..  pathways for decay of secc4h9o:
%..  Newer assumption, from Atkisnon , 1991
rpath3 = 0.65;
rpath4 = 0.35;
      
%Creating the matrix jac
jac = zeros(66,66);

jac(1,1) = -rc(5)*y(36) - rc(11)*y(14) - rc(17)*y(15) - rc(72)*y(23) - ...
    rc(79)*(y(24) + y(57)) - rc(15)*y(39) - rc(60)*(y(19) + y(26) + ...
    y(27) + y(29) + y(31) + y(33) + y(35) + y(43) + y(45) + y(59) + ...
    y(61) + y(60));
jac(1,2) = dj(3) + rc(19)*y(39);
jac(1,14) = -rc(11)*y(1);
jac(1,15) = -rc(17)*y(1);
jac(1,19) = -rc(60)*y(1);
jac(1,23) = -rc(72)*y(1);
jac(1,24) = -rc(79)*y(1);
jac(1,26) = -rc(60)*y(1);
jac(1,27) = -rc(60)*y(1);
jac(1,29) = -rc(60)*y(1);
jac(1,31) = -rc(60)*y(1);
jac(1,33) = -rc(60)*y(1);
jac(1,35) = -rc(60)*y(1);
jac(1,36) = -rc(5)*y(1);
jac(1,39) = dj(13) + rc(19)*y(2) - rc(15)*y(1);
jac(1,43) = -rc(60)*y(1);
jac(1,45) = -rc(60)*y(1);
jac(1,57) = -rc(79)*y(1);
jac(1,59) = -rc(60)*y(1);
jac(1,60) = -rc(60)*y(1);
jac(1,61) = -rc(60)*y(1);

jac(2,1) = rc(5)*y(36) + rc(11)*y(14) + rc(17)*y(15) + rc(72)*y(23) + ...
    rc(79)*(y(24) + y(57)) + 0.2e1*rc(15)*y(39) + rc(60)*(y(19) + y(26)...
    + y(27) + y(29) + y(31) + y(33) + y(35) + y(59)) + rc(60)*(0.86*y(43)...
    + 0.19e1*y(61) + 0.11e1*y(60) + 0.95*y(45));
jac(2,2) = -dj(3) - rc(12)*y(14) - rc(20)*y(39) - rc(21)*y(37) - rc(48)...
    - rc(77)*(y(24) + y(57));
jac(2,14) = rc(11)*y(1) - rc(12)*y(2);
jac(2,15) = rc(17)*y(1);
jac(2,16) = dj(5);
jac(2,19) = rc(60)*y(1);
jac(2,23) = rc(72)*y(1);
jac(2,24) = rc(79)*y(1) - rc(77)*y(2);
jac(2,25) = rc(78);
jac(2,26) = rc(60)*y(1);
jac(2,27) = rc(60)*y(1);
jac(2,29) = rc(60)*y(1);
jac(2,31) = rc(60)*y(1);
jac(2,33) = rc(60)*y(1);
jac(2,35) = rc(60)*y(1);
jac(2,36) = rc(5)*y(1);
jac(2,37) = -rc(21)*y(2);
jac(2,39) = 0.2e1*rc(15)*y(1) + dj(14) - rc(20)*y(2);
jac(2,40) = rc(29) + dj(15);
jac(2,43) = 0.86*rc(60)*y(1);
jac(2,45) = 0.95*rc(60)*y(1);
jac(2,57) = rc(79)*y(1) - rc(77)*y(2);
jac(2,58) = rc(78);
jac(2,59) = rc(60)*y(1);
jac(2,60) = 0.11e1*rc(60)*y(1);
jac(2,61) = 0.19e1*rc(60)*y(1);

jac(3,3) = -rc(39)*y(37) - rc(40)*y(19) - rc(47);
jac(3,19) = -rc(40)*y(3);
jac(3,37) = -rc(39)*y(3);

jac(4,4) = -rc(70)*y(37);
jac(4,8) = 0.44*rc(112)*y(14);
jac(4,9) = 0.4*rc(123)*y(14);
jac(4,11) = rc(66)*y(37) + dj(6) + dj(7) + rc(69)*y(39);
jac(4,12) = dj(8);
jac(4,14) = 0.44*rc(112)*y(8) + 0.4*rc(123)*y(9) + 0.5e-1*rc(160)*y(44)...
    + 0.5e-1*rc(150)*y(41);
jac(4,30) = dj(11) + rc(222)*y(37);
jac(4,32) = 2*rc(221)*y(37) + 2*dj(7);
jac(4,37) = rc(66)*y(11) + 2*rc(221)*y(32) + rc(222)*y(30) - rc(70)*y(4);
jac(4,39) = rc(69)*y(11);
jac(4,41) = 0.5e-1*rc(150)*y(14);
jac(4,44) = 0.5e-1*rc(160)*y(14);

jac(5,5) = -rc(59)*y(37);
jac(5,9) = 0.7e-1*rc(123)*y(14);
jac(5,14) = 0.7e-1*rc(123)*y(9);
jac(5,37) = -rc(59)*y(5);

jac(6,6) = -rc(71)*y(37);
jac(6,37) = -rc(71)*y(6);

jac(7,7) = -rc(81)*y(37);
jac(7,37) = -rc(81)*y(7);

jac(8,8) = -rc(109)*y(37) - rc(112)*y(14);
jac(8,14) = -rc(112)*y(8);
jac(8,37) = -rc(109)*y(8);

jac(9,9) = -rc(123)*y(14) - rc(125)*y(37);
jac(9,14) = 0.7e-1*rc(150)*y(41) - rc(123)*y(9);
jac(9,37) = -rc(125)*y(9);
jac(9,41) = 0.7e-1*rc(150)*y(14);

jac(10,10) = -rc(234)*y(37);
jac(10,37) = -rc(234)*y(10);

jac(11,1) = y(19)*rc(60) + rc(60)*(2*y(29) + y(31) + 0.74*y(43) + ...
    0.266*y(45) + 0.15*y(60));
jac(11,3) = rc(40)*y(19);
jac(11,8) = rc(112)*y(14);
jac(11,9) = 0.5*rc(123)*y(14);
jac(11,11) = -rc(66)*y(37) - dj(6) - dj(7) - rc(69)*y(39) - rc(53);
jac(11,14) = 0.5*rc(123)*y(9) + rc(112)*y(8) + 0.7*rc(157)*y(56) + ...
    0.8*rc(160)*y(44) + 0.8*rc(150)*y(41);
jac(11,19) = rc(60)*y(1) + 2*(2*rc(61) + rc(62))*y(19) + rc(80)*y(24) + ...
    rc(40)*y(3);
jac(11,22) = y(37)*rc(67) + dj(16);
jac(11,24) = rc(80)*y(19);
jac(11,29) = 2*rc(60)*y(1);
jac(11,31) = rc(60)*y(1);
jac(11,32) = 2*dj(7);
jac(11,37) = rc(63)*y(46) + rc(67)*y(22) - rc(66)*y(11);
jac(11,39) = -rc(69)*y(11);
jac(11,41) = 0.8*rc(150)*y(14);
jac(11,43) = 0.74*rc(60)*y(1);
jac(11,44) = 0.8*rc(160)*y(14);
jac(11,45) = 0.266*rc(60)*y(1);
jac(11,46) = y(37)*rc(63);
jac(11,50) = 0.156e1*dj(16);
jac(11,51) = dj(16);
jac(11,56) = 0.7*rc(157)*y(14);
jac(11,60) = 0.15*rc(60)*y(1);

jac(12,1) = rc(72)*y(23) + rc(83)*y(26)*rpath4 + rc(105)*y(27) + ...
    rc(126)*y(31) + 0.95*rc(162)*y(61) + 0.684*rc(154)*y(45);
jac(12,9) = 0.5*rc(123)*y(14);
jac(12,12) = -dj(8) - rc(75)*y(37) - rc(53);
jac(12,14) = 0.5*rc(123)*y(9) + 0.4e-1*rc(160)*y(44);
jac(12,20) = y(37)*rc(64);
jac(12,23) = rc(72)*y(1);
jac(12,26) = y(1)*rc(83)*rpath4;
jac(12,27) = rc(105)*y(1);
jac(12,28) = rc(76)*y(37) + dj(16);
jac(12,31) = rc(126)*y(1);
jac(12,37) = rc(64)*y(20) + rc(76)*y(28) + rc(76)*y(50) - rc(75)*y(12);
jac(12,44) = 0.4e-1*rc(160)*y(14);
jac(12,45) = 0.684*rc(154)*y(1);
jac(12,49) = 0.35*dj(16);
jac(12,50) = rc(76)*y(37) + 0.22*dj(16);
jac(12,51) = dj(16);
jac(12,52) = dj(16);
jac(12,61) = 0.95*rc(162)*y(1);

jac(13,1) = rc(83)*y(26)*rpath3 + rc(159)*y(59) + 0.95*rc(162)*y(61);
jac(13,13) = -dj(9) - rc(86)*y(37) - rc(53);
jac(13,26) = rc(83)*y(1)*rpath3;
jac(13,37) = rc(76)*y(49) + rc(76)*y(51) - rc(86)*y(13);
jac(13,49) = 0.65*dj(16) + rc(76)*y(37);
jac(13,51) = rc(76)*y(37);
jac(13,59) = rc(159)*y(1) + dj(16);
jac(13,61) = 0.95*rc(162)*y(1);

jac(14,1) = -rc(11)*y(14);
jac(14,2) = -rc(12)*y(14);
jac(14,8) = -rc(112)*y(14);
jac(14,9) = -rc(123)*y(14);
jac(14,14) = -rc(11)*y(1) - rc(12)*y(2) - rc(13)*y(37) - rc(14)*y(15)...
    - rc(49) - rc(112)*y(8) - rc(123)*y(9) - rc(157)*y(56) - ...
    rc(160)*y(44) - rc(150)*y(41) - dj(1) - dj(2);
jac(14,15) = rc(89)*y(24) - rc(14)*y(14);
jac(14,24) = rc(89)*y(15);
jac(14,36) = rc(1);
jac(14,37) = -rc(13)*y(14);
jac(14,41) = -rc(150)*y(14);
jac(14,44) = -rc(160)*y(14);
jac(14,56) = -rc(157)*y(14);

jac(15,1) = rc(60)*(y(19) + y(29) + y(31) + y(33) + y(35) + 0.95*y(45) ...
    + y(26)*rpath3 + 0.78*y(43) + y(59) + 0.5e-1*y(61) + 0.8*y(60)) + ...
    rc(72)*y(23) - rc(17)*y(15);
jac(15,3) = rc(40)*y(19) + rc(39)*y(37);
jac(15,4) = rc(70)*y(37);
jac(15,8) = 0.12*rc(112)*y(14);
jac(15,9) = 0.28*rc(123)*y(14);
jac(15,11) = rc(66)*y(37) + 2*dj(6) + rc(69)*y(39);
jac(15,12) = dj(8);
jac(15,14) = rc(13)*y(37) + 0.12*rc(112)*y(8) + 0.28*rc(123)*y(9) + ...
    0.6e-1*rc(160)*y(44) + 0.6e-1*rc(150)*y(41) - rc(14)*y(15);
jac(15,15) = -4*rc(36)*y(15) - rc(14)*y(14) - rc(17)*y(1) - rc(30)*y(37)...
    - rc(65)*y(19) - rc(74)*y(23) - (rc(88) + rc(89))*y(24) - ...
    rc(85)*(y(26) + y(29) + y(31) + y(27) + y(57) + y(45) + y(61) + ...
    y(59) + y(33) + y(35) + y(43) + y(60));
jac(15,17) = rc(31)*y(37) + rc(26)*y(39);
jac(15,18) = y(37)*rc(33);
jac(15,19) = rc(40)*y(3) + 4*rc(61)*y(19) + 0.5*rc(80)*y(24) + ...
    rc(60)*y(1) - rc(65)*y(15);
jac(15,20) = y(37)*rc(64);
jac(15,22) = dj(16);
jac(15,23) = rc(72)*y(1) - rc(74)*y(15);
jac(15,24) = 0.5*rc(80)*y(19) - (rc(88) + rc(89))*y(15);
jac(15,26) = y(1)*rc(60)*rpath3 - rc(85)*y(15);
jac(15,27) = -rc(85)*y(15);
jac(15,28) = dj(16);
jac(15,29) = rc(60)*y(1) - rc(85)*y(15);
jac(15,30) = dj(11);
jac(15,31) = rc(60)*y(1) - rc(85)*y(15);
jac(15,32) = rc(221)*y(37);
jac(15,33) = rc(60)*y(1) - rc(85)*y(15);
jac(15,35) = rc(60)*y(1) - rc(85)*y(15);
jac(15,37) = rc(13)*y(14) + rc(31)*y(17) + rc(33)*y(18) + rc(39)*y(3)...
    + rc(63)*y(46) + rc(64)*y(20) + rc(66)*y(11) + rc(70)*y(4) + ...
    rc(221)*y(32) - rc(30)*y(15);
jac(15,39) = rc(26)*y(17) + rc(69)*y(11);
jac(15,41) = 0.6e-1*rc(150)*y(14);
jac(15,43) = 0.78*rc(60)*y(1) - rc(85)*y(15);
jac(15,44) = 0.6e-1*rc(160)*y(14);
jac(15,45) = 0.95*rc(60)*y(1) - rc(85)*y(15);
jac(15,46) = y(37)*rc(63);
jac(15,48) = dj(16);
jac(15,49) = 0.65*dj(16);
jac(15,50) = dj(16);
jac(15,51) = dj(16);
jac(15,53) = dj(16);
jac(15,57) = -rc(85)*y(15);
jac(15,59) = rc(60)*y(1) - rc(85)*y(15);
jac(15,60) = 0.8*rc(60)*y(1) - rc(85)*y(15);
jac(15,61) = 0.5e-1*rc(60)*y(1) - rc(85)*y(15);

jac(16,2) = rc(21)*y(37);
jac(16,11) = rc(69)*y(39);
jac(16,16) = -rc(35)*y(37) - dj(5) - rc(45);
jac(16,17) = rc(26)*y(39);
jac(16,37) = rc(21)*y(2) - rc(35)*y(16);
jac(16,39) = rc(26)*y(17) + rc(69)*y(11);

jac(17,15) = 2*rc(36)*y(15);
jac(17,17) = -rc(31)*y(37) - dj(4) - rc(43) - rc(26)*y(39) - rc(47);
jac(17,37) = -rc(31)*y(17);
jac(17,39) = -rc(26)*y(17);

jac(18,8) = 0.13*rc(112)*y(14);
jac(18,9) = 0.7e-1*rc(123)*y(14);
jac(18,11) = dj(7);
jac(18,14) = 0.13*rc(112)*y(8) + 0.7e-1*rc(123)*y(9);
jac(18,18) = -y(37)*rc(33);
jac(18,37) = -rc(33)*y(18);

jac(19,1) = y(24)*rc(79) - y(19)*rc(60);
jac(19,3) = -rc(40)*y(19);
jac(19,5) = rc(59)*y(37);
jac(19,9) = 0.31*rc(123)*y(14);
jac(19,12) = dj(8);
jac(19,14) = 0.31*rc(123)*y(9);
jac(19,15) = -rc(65)*y(19);
jac(19,19) = -(2*rc(61) + 2*rc(62))*y(19) - rc(40)*y(3) - rc(60)*y(1)...
    - 2*rc(61)*y(19) - 2*rc(62)*y(19) - rc(65)*y(15) - 0.5*rc(80)*y(24);
jac(19,22) = rc(68)*y(37);
jac(19,24) = rc(79)*y(1) + 4*rc(94)*y(24) - 0.5*rc(80)*y(19);
jac(19,37) = rc(59)*y(5) + rc(68)*y(22);
jac(19,47) = dj(16);

jac(20,20) = -y(37)*rc(64);
jac(20,37) = -rc(64)*y(20);

jac(21,3) = rc(40)*y(19) + rc(39)*y(37);
jac(21,19) = rc(40)*y(3);
jac(21,21) = -rc(51);
jac(21,37) = rc(39)*y(3);

jac(22,15) = rc(65)*y(19);
jac(22,19) = rc(65)*y(15);
jac(22,22) = -rc(43) - dj(16) - (rc(67) + rc(68))*y(37);
jac(22,37) = -(rc(67) + rc(68))*y(22);

jac(23,1) = rc(83)*y(26)*rpath4 - rc(72)*y(23);
jac(23,6) = rc(71)*y(37);
jac(23,13) = dj(9);
jac(23,15) = -rc(74)*y(23);
jac(23,23) = -rc(72)*y(1) - rc(74)*y(15);
jac(23,26) = y(1)*rc(83)*rpath4;
jac(23,28) = rc(68)*y(37);
jac(23,37) = rc(71)*y(6) + rc(68)*y(28);
jac(23,49) = 0.35*dj(16);

jac(24,1) = rc(105)*y(27) + 0.684*rc(154)*y(45) - y(24)*rc(79);
jac(24,2) = -rc(77)*y(24);
jac(24,12) = rc(75)*y(37);
jac(24,13) = dj(9);
jac(24,15) = -(rc(88) + rc(89))*y(24);
jac(24,19) = -rc(80)*y(24);
jac(24,24) = -4*rc(94)*y(24) - rc(77)*y(2) - rc(79)*y(1) - rc(80)*y(19)...
    - (rc(88) + rc(89))*y(15);
jac(24,25) = rc(78);
jac(24,27) = rc(105)*y(1);
jac(24,30) = dj(11) + rc(222)*y(37);
jac(24,37) = rc(75)*y(12) + rc(222)*y(30) + rc(68)*y(47);
jac(24,45) = 0.684*rc(154)*y(1);
jac(24,47) = rc(68)*y(37);
jac(24,52) = dj(16);

jac(25,2) = rc(77)*y(24);
jac(25,24) = rc(77)*y(2);
jac(25,25) = -rc(50) - rc(78);

jac(26,1) = -rc(83)*y(26);
jac(26,7) = rc(81)*y(37);
jac(26,15) = -rc(85)*y(26);
jac(26,26) = -rc(83)*y(1) - rc(85)*y(15);
jac(26,37) = rc(81)*y(7) + rc(68)*y(49);
jac(26,49) = rc(68)*y(37);

jac(27,1) = -rc(105)*y(27);
jac(27,13) = rc(86)*y(37);
jac(27,15) = -rc(85)*y(27);
jac(27,27) = -rc(105)*y(1) - rc(85)*y(15);
jac(27,37) = rc(86)*y(13) + rc(87)*y(52);
jac(27,52) = rc(87)*y(37);

jac(28,15) = rc(74)*y(23);
jac(28,23) = rc(74)*y(15);
jac(28,28) = -(rc(76) + rc(68))*y(37) - dj(16) - rc(52);
jac(28,37) = -(rc(76) + rc(68))*y(28);

jac(29,1) = -rc(110)*y(29);
jac(29,8) = rc(109)*y(37);
jac(29,15) = -rc(85)*y(29);
jac(29,29) = -rc(110)*y(1) - rc(85)*y(15);
jac(29,37) = rc(109)*y(8) + rc(68)*y(50);
jac(29,50) = rc(68)*y(37);

jac(30,1) = rc(236)*y(33) + rc(220)*y(35) + 0.266*rc(154)*y(45);
jac(30,14) = 0.82*rc(160)*y(44);
jac(30,30) = -dj(11) - rc(222)*y(37);
jac(30,33) = rc(236)*y(1);
jac(30,35) = rc(220)*y(1);
jac(30,37) = -rc(222)*y(30);
jac(30,44) = 0.82*rc(160)*y(14);
jac(30,45) = 0.266*rc(154)*y(1);
jac(30,48) = dj(16);
jac(30,53) = dj(16);

jac(31,1) = -rc(126)*y(31);
jac(31,9) = rc(125)*y(37);
jac(31,15) = -rc(85)*y(31);
jac(31,31) = -rc(126)*y(1) - rc(85)*y(15);
jac(31,37) = rc(125)*y(9) + rc(68)*y(51);
jac(31,51) = rc(68)*y(37);

jac(32,1) = rc(220)*y(35);
jac(32,32) = -2*dj(7) - rc(221)*y(37);
jac(32,35) = rc(220)*y(1);
jac(32,37) = -rc(221)*y(32);
jac(32,53) = dj(16);

jac(33,1) = -rc(236)*y(33);
jac(33,10) = rc(234)*y(37);
jac(33,15) = -rc(85)*y(33);
jac(33,33) = -rc(236)*y(1) - rc(85)*y(15);
jac(33,37) = rc(234)*y(10) + rc(235)*y(48);
jac(33,48) = rc(235)*y(37);

jac(34,1) = rc(236)*y(33);
jac(34,33) = rc(236)*y(1);
jac(34,34) = -rc(219)*y(37);
jac(34,37) = -rc(219)*y(34);
jac(34,48) = dj(16);

jac(35,1) = -rc(220)*y(35);
jac(35,15) = -rc(85)*y(35);
jac(35,34) = rc(219)*y(37);
jac(35,35) = -rc(220)*y(1) - rc(85)*y(15);
jac(35,37) = rc(219)*y(34) + rc(223)*y(53);
jac(35,53) = rc(223)*y(37);

jac(36,1) = -rc(5)*y(36);
jac(36,2) = dj(3);
jac(36,14) = dj(1) + 0.2*rc(160)*y(44) + 0.3*rc(150)*y(41);
jac(36,36) = -rc(1) - rc(5)*y(1);
jac(36,38) = rc(7);
jac(36,39) = dj(14);
jac(36,41) = 0.3*rc(150)*y(14);
jac(36,44) = 0.2*rc(160)*y(14);

jac(37,1) = rc(17)*y(15);
jac(37,2) = -rc(21)*y(37);
jac(37,3) = -rc(39)*y(37);
jac(37,4) = -rc(70)*y(37);
jac(37,5) = -rc(59)*y(37);
jac(37,6) = -rc(71)*y(37);
jac(37,7) = -rc(81)*y(37);
jac(37,8) = -rc(109)*y(37);
jac(37,9) = 0.15*rc(123)*y(14) - rc(125)*y(37);
jac(37,10) = -rc(234)*y(37);
jac(37,11) = -rc(66)*y(37);
jac(37,12) = -rc(75)*y(37);
jac(37,13) = -rc(86)*y(37);
jac(37,14) = rc(14)*y(15) + 0.15*rc(123)*y(9) + 0.8e-1*rc(160)*y(44)...
    + 0.55*rc(150)*y(41) - rc(13)*y(37);
jac(37,15) = rc(14)*y(14) + rc(17)*y(1) - rc(30)*y(37);
jac(37,16) = dj(5) - rc(35)*y(37);
jac(37,17) = 2*dj(4) - rc(31)*y(37);
jac(37,18) = -y(37)*rc(33);
jac(37,20) = -y(37)*rc(64);
jac(37,22) = dj(16) - rc(68)*y(37);
jac(37,28) = dj(16) - rc(68)*y(37);
jac(37,30) = -rc(222)*y(37);
jac(37,32) = -rc(221)*y(37);
jac(37,34) = -rc(219)*y(37);

s1 = -rc(149)*(y(65) + y(66)) - rc(21)*y(2) - rc(35)*y(16) - ...
    rc(87)*y(52) - rc(147)*y(64) - rc(75)*y(12) - rc(148)*y(62) - ...
    rc(31)*y(17) - rc(64)*y(20) - rc(70)*y(4) - rc(153)*y(44) - ...
    rc(86)*y(13) - rc(13)*y(14) - rc(109)*y(8) - rc(235)*y(48) - ...
    rc(234)*y(10) - rc(81)*y(7);

jac(37,37) = s1 - rc(59)*y(5) - rc(219)*y(34) - rc(30)*y(15) - ...
    rc(221)*y(32) - rc(63)*y(46) - rc(66)*y(11) - rc(222)*y(30) - ...
    rc(33)*y(18) - rc(39)*y(3) - rc(223)*y(53) - rc(151)*y(41) - ...
    rc(71)*y(6) - rc(158)*y(54) - rc(161)*y(55) - rc(125)*y(9) - ...
    rc(68)*(y(22) + y(28) + y(47) + y(50) + y(51) + y(49)) - rc(146)*y(63);
jac(37,38) = 2*rc(8)*H2O;
jac(37,41) = 0.55*rc(150)*y(14) - rc(151)*y(37);
jac(37,44) = 0.8e-1*rc(160)*y(14) - rc(153)*y(37);
jac(37,46) = -y(37)*rc(63);
jac(37,47) = dj(16) - rc(68)*y(37);
jac(37,48) = dj(16) - rc(235)*y(37);
jac(37,49) = dj(16) - rc(68)*y(37);
jac(37,50) = dj(16) - rc(68)*y(37);
jac(37,51) = -rc(68)*y(37);
jac(37,52) = dj(16) - rc(87)*y(37);
jac(37,53) = dj(16) - rc(223)*y(37);
jac(37,54) = -rc(158)*y(37);
jac(37,55) = -rc(161)*y(37);
jac(37,62) = -rc(148)*y(37);
jac(37,63) = -rc(146)*y(37);
jac(37,64) = -rc(147)*y(37);
jac(37,65) = -rc(149)*y(37);
jac(37,66) = -rc(149)*y(37);

jac(38,14) = dj(2);
jac(38,38) = -rc(7) - rc(8)*H2O;

jac(39,1) = -rc(15)*y(39);
jac(39,2) = rc(12)*y(14) - (rc(19) + rc(20))*y(39);
jac(39,11) = -rc(69)*y(39);
jac(39,14) = rc(12)*y(2);
jac(39,16) = rc(35)*y(37);
jac(39,17) = -rc(26)*y(39);
jac(39,37) = rc(35)*y(16);
jac(39,39) = -rc(15)*y(1) - rc(26)*y(17) - rc(163)*y(41) - rc(19)*y(2)...
    - rc(20)*y(2) - dj(13) - dj(14) - rc(69)*y(11);
jac(39,40) = rc(29) + dj(15);
jac(39,41) = -rc(163)*y(39);

jac(40,2) = rc(20)*y(39);
jac(40,39) = rc(20)*y(2);
jac(40,40) = -rc(29) - dj(15) - rc(45);

jac(41,14) = -rc(150)*y(41);
jac(41,37) = -rc(151)*y(41);
jac(41,39) = -rc(163)*y(41);
jac(41,41) = -rc(151)*y(37) - rc(163)*y(39) - rc(150)*y(14);

jac(42,16) = rc(45);
jac(42,40) = 2*rc(44);
jac(42,42) = -rc(51);

jac(43,1) = -0.88e0*rc(152)*y(43);
jac(43,15) = -rc(155)*y(43);
jac(43,37) = rc(151)*y(41) + rc(156)*y(56);
jac(43,41) = rc(151)*y(37);
jac(43,43) = -0.88*rc(152)*y(1) - rc(155)*y(15);
jac(43,56) = rc(156)*y(37);

jac(44,1) = rc(60)*(0.42*y(43) + 0.5e-1*y(60));
jac(44,14) = 0.26*rc(150)*y(41) - rc(160)*y(44);
jac(44,37) = -rc(153)*y(44);
jac(44,41) = 0.26*rc(150)*y(14);
jac(44,43) = 0.42*rc(60)*y(1);
jac(44,44) = -rc(153)*y(37) - rc(160)*y(14);
jac(44,60) = 0.5e-1*rc(60)*y(1);

jac(45,1) = -rc(154)*y(45);
jac(45,15) = -rc(85)*y(45);
jac(45,37) = rc(153)*y(44) + rc(148)*y(62);
jac(45,44) = rc(153)*y(37);
jac(45,45) = -rc(154)*y(1) - rc(85)*y(15);
jac(45,62) = rc(148)*y(37);

jac(46,19) = 2*rc(62)*y(19);
jac(46,37) = -rc(63)*y(46);
jac(46,46) = -y(37)*rc(63);

jac(47,15) = rc(88)*y(24);
jac(47,24) = rc(88)*y(15);
jac(47,37) = -rc(68)*y(47);
jac(47,47) = -rc(68)*y(37) - dj(16)-rc(52);

jac(48,15) = rc(85)*y(33);
jac(48,33) = rc(85)*y(15);
jac(48,37) = -rc(235)*y(48);
jac(48,48) = -rc(235)*y(37) - dj(16) - rc(52);

jac(49,15) = rc(85)*y(26);
jac(49,26) = rc(85)*y(15);
jac(49,37) = -(rc(76) + rc(68))*y(49);
jac(49,49) = -(rc(76) + rc(68))*y(37) - dj(16) - rc(52);

jac(50,15) = rc(85)*y(29);
jac(50,29) = rc(85)*y(15);
jac(50,37) = -(rc(76) + rc(68))*y(50);
jac(50,50) = -(rc(76) + rc(68))*y(37) - dj(16) - rc(52);

jac(51,15) = rc(85)*y(31);
jac(51,31) = rc(85)*y(15);
jac(51,37) = -(rc(76) + rc(68))*y(51);
jac(51,51) = -(rc(76) + rc(68))*y(37) - dj(16) - rc(52);

jac(52,15) = rc(85)*y(27);
jac(52,27) = rc(85)*y(15);
jac(52,37) = -rc(87)*y(52);
jac(52,52) = -rc(87)*y(37) - dj(16) - rc(52);

jac(53,15) = rc(85)*y(35);
jac(53,35) = rc(85)*y(15);
jac(53,37) = -rc(223)*y(53);
jac(53,53) = -rc(223)*y(37) - dj(16) - rc(52);

jac(54,1) = rc(60)*(0.32*y(43) + 0.1*y(60));
jac(54,14) = 0.67*rc(150)*y(41);
jac(54,37) = -rc(158)*y(54);
jac(54,41) = 0.67*rc(150)*y(14);
jac(54,43) = 0.32*rc(60)*y(1);
jac(54,54) = -rc(158)*y(37);
jac(54,60) = 0.1*rc(60)*y(1);

jac(55,1) = rc(60)*(0.14*y(43) + 0.5e-1*y(45) + 0.85*y(60));
jac(55,37) = -rc(161)*y(55);
jac(55,43) = 0.14*rc(60)*y(1);
jac(55,45) = 0.5e-1*rc(60)*y(1);
jac(55,55) = -rc(161)*y(37);
jac(55,60) = 0.85*rc(60)*y(1);

jac(56,14) = -rc(157)*y(56);
jac(56,15) = rc(155)*y(43);
jac(56,37) = -rc(156)*y(56);
jac(56,43) = rc(155)*y(15);
jac(56,56) = -rc(156)*y(37) - rc(157)*y(14) - rc(52);

jac(57,1) = -rc(79)*y(57);
jac(57,2) = -rc(77)*y(57);
jac(57,15) = -rc(85)*y(57);
jac(57,37) = 0.5*rc(158)*y(54) + rc(149)*y(66);
jac(57,54) = 0.5*rc(158)*y(37);
jac(57,57) = -rc(77)*y(2) - rc(79)*y(1) - rc(85)*y(15);
jac(57,58) = rc(78);
jac(57,66) = rc(149)*y(37);

jac(58,2) = rc(77)*y(57);
jac(58,57) = rc(77)*y(2);
jac(58,58) = -rc(50) - rc(78);

jac(59,1) = rc(79)*y(57) - rc(159)*y(59);
jac(59,15) = -rc(85)*y(59);
jac(59,37) = rc(146)*y(63);
jac(59,57) = rc(79)*y(1);
jac(59,59) = -rc(159)*y(1) - rc(85)*y(15);
jac(59,63) = rc(146)*y(37);

jac(60,1) = -rc(164)*y(60);
jac(60,15) = -rc(85)*y(60);
jac(60,37) = rc(147)*y(64);
jac(60,39) = rc(163)*y(41);
jac(60,41) = rc(163)*y(39);
jac(60,60) = -rc(164)*y(1) - rc(85)*y(15);
jac(60,64) = rc(147)*y(37);

jac(61,1) = -rc(162)*y(61);
jac(61,15) = -rc(85)*y(61);
jac(61,37) = rc(161)*y(55) + rc(149)*y(65);
jac(61,55) = rc(161)*y(37);
jac(61,61) = -rc(162)*y(1) - rc(85)*y(15);
jac(61,65) = rc(149)*y(37);

jac(62,15) = rc(85)*y(45);
jac(62,37) = -rc(148)*y(62);
jac(62,45) = rc(85)*y(15);
jac(62,62) = -rc(148)*y(37) - rc(52);

jac(63,15) = rc(85)*y(59);
jac(63,37) = -rc(146)*y(63);
jac(63,59) = rc(85)*y(15);
jac(63,63) = -rc(146)*y(37) - rc(52);

jac(64,15) = rc(85)*y(60);
jac(64,37) = -rc(147)*y(64);
jac(64,60) = rc(85)*y(15);
jac(64,64) = -rc(147)*y(37) - rc(52);

jac(65,15) = rc(85)*y(61);
jac(65,37) = -rc(149)*y(65);
jac(65,61) = rc(85)*y(15);
jac(65,65) = -rc(149)*y(37) - rc(52);

jac(66,15) = rc(85)*y(57);
jac(66,37) = -rc(149)*y(66);
jac(66,57) = rc(85)*y(15);
jac(66,66) = -rc(149)*y(37) - rc(52);
end

