function [f,x0,tspan,jac,sol] = emep_problem()
%Defined
f = @ode.stiff.emep.emep_rhs;
jac = @ode.stiff.emep.emep_jac;
tspan = [14400, 417600];
x0 = zeros(66,1);
x0(1,1) = 1e9;
x0(2:3,1) = 5e9;
x0(4,1) = 3.8e12;
x0(5,1) = 3.5e13;
x0(6:13,1) = 1e7;
x0(14,1) = 5e11;
x0(15:37,1) = 1e2;
x0(38,1) = 1e-3;
x0(39:end,1) = 1e2;

%Not defined
sol = NaN;
end