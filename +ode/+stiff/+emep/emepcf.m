function [rc, dj, H2O] = emepcf(time)
% Compute time-dependent EMEP coefficients
%   rc: reaction coefficients
%   dj: dissociation rate coefficient
%   H2O: water vapour concentrations
%
% A and B: dj=A*exp(-B*SEC)
% SEC = 1/cos(THETA) where THETA is solar zenith angle
% temp: temperature in K

nbrOfSpec = 66;
nbrOfrc = 266;
nbrOfDJ = 16;
hmix = 1.2e5;

rc = zeros(nbrOfrc,1);
dj = zeros(nbrOfDJ,1);

A = [1.23e-3; 2.00e-4; 1.45e-2; 2.20e-5; 3.00e-6; 5.40e-5; 6.65e-5; ...
    1.35e-5; 2.43e-5; 5.40e-4; 2.16e-4; 5.40e-5; 3.53e-2; 8.94e-2; ...
    3.32e-52; 27e-5];

B = [0.60; 1.40; 0.40; 0.75; 1.25; 0.79; 0.60; 0.94; 0.88; 0.79; 0.79; ...
    0.79; 0.081; 0.059; 0.57; 0.62];

timeHours = time/3600;
intTimeHours = floor(timeHours);
intTimeDays = floor(intTimeHours/24) + 1;
timeOD = timeHours - (intTimeDays - 1)*24;

%
% Meteorology
%
% XLHA: local hour angle
XLHA = (1 + timeOD*3600/4.32e4)*pi;

% FI: (Norwegian PHI!) latitude, dekl solar declination (here latitude =
% 50 deg. N)
FI = 50*pi/180;
DEKL = 23.5*pi/180;
SEC = 1/(cos(XLHA)*cos(FI)*cos(DEKL) + sin(FI)*sin(DEKL));

% Def of temperature variation
% XP = 8.7e-5*timeOD*3600 - 2.83
% T = 8.3*sin(XP) + 289.86
% We choose this temp. for simplicity
temp=298;

% Def of water vapor concentration
XQ = -7.93e-5*timeOD*3600 + 2.43;
rh = 23*sin(XQ) + 66.5;
XZ = (597.3 - 0.57*(temp - 273.16))*18/1.986*(1/temp - 1/273.16);

H2O = 6.1078*exp(-XZ)*10/(1.38e-16*temp)*rh;

% --------------------------------------------------------------------------
% Calculate  values of photolysis rates dj(1..16)
% --------------------------------------------------------------------------
% based upon RGD A & B coefficients and correction factors from
% HOUGH (1988)
if (timeOD < 4 || timeOD >= 20)
    % in the dark:
    for i = 1:nbrOfDJ
        dj(i) = 1e-40;
    end
else
    % in daytime:
    for i = 1:nbrOfDJ
        dj(i) = A(i) * exp(-B(i)*SEC);
        if(dj(i) < 0)
            ME = MException('emepcf:BadRate','dj(i) < 0');
            throw(ME);
        end
    end
end

% --------------------------------------------------------------------------
% Set up chemical reaction rate coefficients rc(i):
% --------------------------------------------------------------------------
%     16/6/92: inclusion of M, N2, O2 values in rate-constants
%     reaction rate coefficient definition. units: 2-body reactions
%     cm**3/(molecule x s), unimolecular 1.D0/s, 3-body
%     cm**6/(molecule**2 x s)

m  = 2.55e19;
o2 = 5.2e18;
xn2= 1.99e19;

%..A92, assuming 80% N2, 20% O2
rc(1)  = 5.7e-34*(temp/300)^(-2.8)*m*o2;
%..unchanged, as source of O+NO+O2 reaction rate unknown.
rc(5)  = 9.6e-32*(temp/300)^(-1.6)*m;
%..estimate from Demore(1990) (=A92) O(d)+N2 and DeMore O(D)+O2
rc(7)  = 2.0e-11*exp(100/temp)*m;
%.. A92    >>>>>
rc(11) = 1.8e-12*exp(-1370/temp);
rc(12) = 1.2e-13*exp(-2450/temp);
rc(13) = 1.9e-12*exp(-1000/temp);
rc(14) = 1.4e-14*exp(-600/temp);
rc(15) = 1.8e-11*exp(+110/temp);
rc(17) = 3.7e-12*exp(240/temp);
%.. <<<<<<    A92
%..M.J (from Wayne et al.)
rc(19) = 7.2e-14*exp(-1414/temp);
%fix  rc(19) =7.2e-13*exp(-1414/temp);
%M.J suggests that rc(27) be omitted:
% rc(27) = 8.5e-13*exp(-2450/temp);
%.. My change to get similar to A92 troe.
rc(29) = 7.1e14*exp(-11080/temp);
%..A92
rc(30) = 4.8e-11*exp(250/temp);
%..A92, De More,1990 .. no change : oh + h2o2
rc(31) = 2.9e-12*exp(-160/temp);
%..A92 : oh + h2
rc(33) = 7.7e-12*exp(-2100/temp);
%..My, similar to DeMore et al complex : oh+hno3
rc(35) = 1.0e-14*exp(785/temp);
%.. Mike Jenkin`s suggestion for ho2 + ho2 reactions: (from DeMore et al.)
rc(36) = 2.3e-13*exp(600/temp);
rc(36) = rc(36) + m*1.7e-33*exp(1000/temp);
rc(36) = rc(36)*(1 + 1.4e-21*H2O*exp(2200/temp));
%..A92
rc(59) = 3.9e-12*exp(-1885/temp);
% A92 : ch3o2 + no
rc(60) = 4.2e-12*exp(180/temp);
% A92 + A90 assumption that ka = 0.5D0 * k
rc(61) = 5.5e-14*exp(365/temp);
% A92 + A90 assumption that kb = 0.5D0 * k
rc(62) = 5.5e-14*exp(365/temp);
% A92
rc(63) = 3.3e-12*exp(-380/temp);
% A92 : ho2 + ch3o2
rc(65) = 3.8e-13*exp(780/temp);
% A92 new: ch3ooh + oh -> hcho + oh
rc(67) = 1.0e-12*exp(190/temp);
% A92 new: ch3ooh + oh -> ch3o2
rc(68) = 1.9e-12*exp(190/temp);
%.. A92
rc(71) = 7.8e-12*exp(-1020/temp);
% A92 new: c2h5o2 + ho2 -> c2h5ooh (r2ooh)
rc(74) = 6.5e-13*exp(650/temp);
%.. A92
rc(75) = 5.6e-12*exp(310/temp);
% TOR90 assumption w.r.t. rc(67) : c2h5ooh + oh -> ch3cho + oh
%     rc(76) = 5.8 * rc(67)
rc(76) = 5.8e-12*exp(190/temp);
%A92 : approximation to troe expression
rc(78) = 1.34e16*exp(-13330/temp);
% additional reactions :-
% A92 : ho2 + ch3coo2 -> rco3h
rc(88) = 1.3e-13*exp(1040/temp);
% A92 : ho2 + ch3coo2 -> rco2h + o3
rc(89) = 3.0e-13*exp(1040/temp);
%.. A92
rc(94) = 2.8e-12*exp(530/temp);
%.. D & J, gives results very close to A92
rc(81) = 1.64e-11*exp(-559/temp);

%.. A90
rc(83) = rc(60);
rc(105) = rc(60);
%A90
rc(110) = rc(60);
%A90/PS  isnir + no
rc(162) = rc(60);
%A90/PS  isono3 + no
rc(164) = rc(60);

%.. From RGD, but very similar to A92 Troe
rc(109) = 1.66e-12*exp(474/temp);
%A92
rc(112) = 1.2e-14*exp(-2630/temp);
%A92
rc(123) = 6.5e-15*exp(-1880/temp);
%A90
rc(126) = rc(60);
%..A90
rc(220) = rc(60);
%..A90
rc(236) = rc(60);
%ooooooooooooo   natural voc reactions 00000000000000000
%..A90  isoprene + o3 -> products
rc(150) = 12.3e-15*exp(-2013/temp);
%..A90  isoprene + oh -> isro2
rc(151) = 2.54e-11*exp(410/temp);
%A90  isoprene-RO2 + no
rc(152) = rc(60);
%A90  methylvinylketone (mvk) + oh
rc(153) = 4.13e-12*exp(452/temp);
%A90  mvko2 + no
rc(154) = rc(60);
%A90  macr + oh
rc(158) = 1.86e-11*exp(175/temp);
%A90  ch2cch3 + no
rc(159) = rc(60);
%A90  mvk + o3
rc(160) = 4.32e-15*exp(-2016/temp);
%
% rc(255) = 9.9e-16*exp(-731/temp);
%.......................................................................
%
%oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
%     aerosol formation and depositions ..x
%     parameterization of heterogeneous loss in 1./s for humidities
%     less than 90%.  applied to hno3, h2o2, h2so4, ch3o2h.
rc(43) = 5e-6;
if(rh > 0.90)
    rc(43) = 1e-4;
end
rc(44) = rc(43);
rc(45) = rc(43);

%.. A92
rc(8)  = 2.2e-10;
%.. My (?) , similar to A92 troe expression.
rc(20) = 1.4e-12;
%.. My (?) , similar to A92 troe expression.
rc(21) = 1.4e-11;
%.. RGD Note (=DeMore?)
rc(26) = 4.1e-16;
%.. RGD
rc(39) = 1.35e-12;
%.. RGD
rc(40) = 4.0e-17;
%.. A92, assuming all products are ch3cho
rc(64) = 3.2e-12;
%.. A92, with temperature dependance neglected.
rc(66) = 9.6e-12;
%.. A92 >>>>>>>>
rc(69) = 5.8e-16;
rc(70) = 2.4e-13;
rc(72) = 8.9e-12;
%.. <<<<<<<<<< A92
%.. A92 : approximation to troe expression
rc(77) = 1.0e-11;
%.. A92 : ch3coo2 + no
rc(79) = 2.0e-11;
%.. A92 : sum of two pathways
rc(80) = 1.1e-11;
%mj   rc(84) =2.5d-14
%ya   kho2ro2 : estimate of rate of ho2 + higher RO2, suggested by Yvonne
rc(85) = 1.0e-11;
%..A90, ignoring slight temperature dependance
rc(86) = 1.15e-12;
%..MJ suggestion.. combine oh + secc4h9o2, meko2 rates   =>
%     rc(87)= rc(68) + rc(86), approx. at 298
rc(87)= 4.8e-12;
%mj..new A92 : oh + ch3co2h -> ch3o2
rc(90) = 8.0e-13;
%.. Approximates to A92 Troe ...
rc(125)= 2.86e-11;
%.. rate for ch2chr + oh, = k68+k125 (propene), Yv.
rc(146) = 3.2e-11;
%.. rate for isono3h + oh, = k156 (isro2h), Yv.
rc(147) = 2.0e-11;
%.. MY GUESS rate of oh + mvko2h
rc(148) = 2.2e-11;
%.. MY GUESS rate of oh + other biogenic ooh
rc(149) = 3.7e-11;
%ya   kho2ro2 : estimate of rate of ho2 + higher RO2, suggested by Yvonne
%A90  isro2 + ho2
rc(155) = rc(85);
%PS   isro2h + oh
rc(156) = 2.0e-11;
%PS   isro2h + o3
rc(157) = 8.0e-18;
%PS   isni + oh
rc(161) = 3.35e-11;
%PS   isopre + no3
rc(163) = 7.8e-13;
% rc(163) = 7.8e-16;
%.. Unchanged, also in IVL scheme
rc(219) = 2.0e-11;
%..A92
rc(221) = 1.1e-11;
%..A92
rc(222) = 1.7e-11;
%..MJ suggestion.. combine oh + malo2h rates   =>
% rc(223)= rc(68) + rc(219);
rc(223) = 2.4e-11;
%..A90
rc(234) = 1.37e-11;
%..MJ suggestion.. combine rc(68) with rc(234) =>
% rc(235)= rc(68) + rc(234);
rc(235) = 1.7e-11;

%..............................................
%         deposition loss rate coefficients vd/hmix, vd in cm/s.
%         hno3     calculated     rc(46)
%         so2      0.5            rc(47)
%         h2o2     0.5            rc(47)
%         no2      0.2            rc(48)
%         o3       0.5            rc(49)
%         pan      0.2            rc(50)
%         h2so4    0.1            rc(51)
%... simple approx. for now - reduce all vg by 4 at night to allow
%    for surface inversion......
delta = 1;
if(timeOD >= 20 || timeOD < 4)
    delta = 0.25;
end
% if(timeOD >= 20 || timeOD <= 4)
%   delta = 0.25;
% end
% if(night)
%   delta=0.25;
% end
rc(46) = 2.0*delta/hmix;
rc(47) = 0.5*delta/hmix;
rc(48) = 0.2*delta/hmix;
rc(49) = 0.5*delta/hmix;
rc(50) = 0.2*delta/hmix;
rc(51) = 0.1*delta/hmix;
%. dep. of organic peroxides = 0.5 cms-1
rc(52) = 0.5*delta/hmix;
%. dep. of ketones, rcHO  = 0.3 cms-1
rc(53) = 0.3*delta/hmix;
end

