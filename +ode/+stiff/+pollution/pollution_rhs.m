function [dy] = pollution_rhs(t,y)
%Pollution problem, a stiff system of 20 non-linear Ordinary Differential
%Equations. Models the chemical reaction part of the air pollution model
%developed at The Dutch National Institute of Public Health and
%Environmental Protection (RIVM).
%
%   Detailed explanation goes here
%
% See Test Set for Initial Value Problem Solvers Francesca Mazzia and
%   Felice Iavernaro Report 40/2003
%
% 0 \leq t \ leq 60
% y_0 = (0,0.2,0,0.04,0,0,0.1,0,0,0,0,0,0,0,0.007,0,0,0)

%Defining the vector k
k = zeros(25,1);

k(1)  = 0.350;
k(2)  = 0.266e2;
k(3)  = 0.123e5;
k(4)  = 0.860e-3;
k(5)  = 0.820e-3;
k(6)  = 0.150e5;
k(7)  = 0.130e-3;
k(8)  = 0.240e5;
k(9)  = 0.165e5;
k(10) = 0.900e4;
k(11) = 0.220e-1;
k(12) = 0.120e5;
k(13) = 0.188e1;
k(14) = 0.163e5;
k(15) = 0.480e7;
k(16) = 0.350e-3;
k(17) = 0.175e-1;
k(18) = 0.100e9;
k(19) = 0.444e12;
k(20) = 0.124e4;
k(21) = 0.210e1;
k(22) = 0.578e1;
k(23) = 0.474e-1;
k(24) = 0.178e4;
k(25) = 0.312e1;

%Defining the vector r
r = zeros(25,1);

r(1)  = k(1)*y(1);
r(2)  = k(2)*y(2)*y(4);
r(3)  = k(3)*y(5)*y(2);
r(4)  = k(4)*y(7);
r(5)  = k(5)*y(7);
r(6)  = k(6)*y(7)*y(6);
r(7)  = k(7)*y(9);
r(8)  = k(8)*y(9)*y(6);
r(9)  = k(9)*y(11)*y(2);
r(10) = k(10)*y(11)*y(1);
r(11) = k(11)*y(13);
r(12) = k(12)*y(10)*y(2);
r(13) = k(13)*y(14);
r(14) = k(14)*y(1)*y(6);
r(15) = k(15)*y(3);
r(16) = k(16)*y(4);
r(17) = k(17)*y(4);
r(18) = k(18)*y(16);
r(19) = k(19)*y(16);
r(20) = k(20)*y(17)*y(6);
r(21) = k(21)*y(19);
r(22) = k(22)*y(19);
r(23) = k(23)*y(1)*y(4);
r(24) = k(24)*y(19)*y(1);
r(25) = k(25)*y(20);

%Creating the vector df
dy = zeros(20,1);

dy(1)  = -r(1)-r(10)-r(14)-r(23)-r(24)+r(2)+r(3)+r(9)+r(11)+r(12)+r(22)+r(25);
dy(2)  = -r(2)-r(3)-r(9)-r(12)+r(1)+r(21);
dy(3)  = -r(15)+r(1)+r(17)+r(19)+r(22);
dy(4)  = -r(2)-r(16)-r(17)-r(23)+r(15);
dy(5)  = -r(3)+r(4)+r(4)+r(6)+r(7)+r(13)+r(20);
dy(6)  = -r(6)-r(8)-r(14)-r(20)+r(3)+r(18)+r(18);
dy(7)  = -r(4)-r(5)-r(6)+r(13);
dy(8)  = r(4)+r(5)+r(6)+r(7);
dy(9)  = -r(7)-r(8);
dy(10) = -r(12)+r(7)+r(9);
dy(11) = -r(9)-r(10)+r(8)+r(11);
dy(12) = r(9);
dy(13) = -r(11)+r(10);
dy(14) = -r(13)+r(12);
dy(15) = r(14);
dy(16) = -r(18)-r(19)+r(16);
dy(17) = -r(20);
dy(18) = r(20);
dy(19) = -r(21)-r(22)-r(24)+r(23)+r(25);
dy(20) = -r(25)+r(24);
end

