function [f,x0,tspan,jac,sol] = pollution_problem()
%Defined
f = @ode.stiff.pollution.pollution_rhs;
jac = @ode.stiff.pollution.pollution_jac;
tspan = [0,60];
x0 = [0; 0.2; 0; 0.04; 0; 0; 0.1; 0.3; 0.01; 0; 0; 0; 0; 0; 0; 0; 0.007; 0; 0; 0];

%Not defined
sol = NaN;
end