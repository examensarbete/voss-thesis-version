function jac = oregonator_jac(t,y)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

%Parameters
s = 77.27;
w = 0.161;
q = 8.375e-6;

% Creating the vector yd
jac = zeros(3,3);

jac(1,1) = s*(-y(2) + 1 - 2*q*y(1));
jac(1,2) = s*(1 - y(1));
jac(1,3) = 0;

jac(2,1) = (1/s)*(-y(2));
jac(2,2) = (1/s)*(-1 -y(1));
jac(2,3) = 1/s;

jac(3,1) = w;
jac(3,2) = 0;
jac(3,3) = -w;
end

