function [yd] = oregonator_rhs(t,y)
%oregonator: Summary of this function goes here
%   Detailed explanation goes here
%
% 0 \leq t \leq 360
% y_0 = (1;2;3)

%Parameters
s = 77.27;
w = 0.161;
q = 8.375e-6;

% Creating the vector yd
yd = zeros(3,1);

yd(1) = s*(y(2)-y(1)*y(2)+y(1)-q*y(1)^2);
yd(2) = (1/s)*(-y(2)-y(1)*y(2)+y(3));
yd(3) = w*(y(1)-y(3));
end

