function [f,x0,tspan,jac,sol] = oregonator_problem()
%Defined
f = @ode.stiff.oregonator.oregonator_rhs;
jac = @ode.stiff.oregonator.oregonator_jac;
tspan = [0,360];
x0 = [1; 2; 3];

%Not defined
sol = NaN;
end