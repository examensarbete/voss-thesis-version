function jac = robertson_jac(t,y)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

%Parameters
k1 = 0.04;
k2 = 3e7;
k3 = 1e4;

%Creating the vector yd
jac = zeros(3,3);

jac(1,1) = -k1;
jac(1,2) = k3*y(3);
jac(1,3) = k3*y(2);

jac(2,1) = k1;
jac(2,2) = -2*k2*y(2) - k3*y(3);
jac(2,3) = -k3*y(2);

jac(3,1) = 0;
jac(3,2) = 2*k2*y(2);
jac(3,3) = 0;
end

