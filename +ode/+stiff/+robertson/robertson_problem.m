function [f,x0,tspan,jac,sol] = robertson_problem()
%Defined
f = @ode.stiff.robertson.robertson_rhs;
jac = @ode.stiff.robertson.robertson_jac;
tspan = [0, 1e11];
x0 = [1; 0; 0];

%Not defined
sol = NaN;
end