function [yd] = robertson_rhs(t,y)
%robertson: Summary of this function goes here
%   Detailed explanation goes here
%
% y_0 = (1;0;0)
%

%Parameters
k1 = 0.04;
k2 = 3e7;
k3 = 1e4;

%Creating the vector yd
yd = zeros(3,1);

yd(1) = -k1*y(1) + k3*y(2)*y(3);
yd(2) = k1*y(1) - k2*y(2)^2 - k3*y(2)*y(3);
yd(3) = k2*y(2)^2;
end

