function [yd] = ringModulator_rhs(t,y)
%ringModulator: A stiff system of 15 non-linear ordinary differential
%equations (when C_s \neq 0).
%   Detailed explanation goes here

% Parameters
c     = 1.6e-8;
cs    = 2e-12;
cp    = 1e-8;

lh    = 4.45;
ls1   = 2e-3;
ls2   = 5e-4;
ls3   = 5e-4;

r     = 25000;
rp    = 50;
rg1   = 36.3;
rg2   = 17.3;
rg3   = 17.3;
ri    = 50;
rc    = 600;

gamma = 40.67286402e-9;
delta = 17.7493332;

% The u parameters
uin1  = 0.5*sin(2000*pi*t);
uin2  = 2*sin(20000*pi*t);

ud1   = +y(3) - y(5) - y(7) - uin2;
ud2   = -y(4) + y(6) - y(7) - uin2;
ud3   = +y(4) + y(5) + y(7) + uin2;
ud4   = -y(3) - y(6) + y(7) + uin2;

% The qud parameters
qud1  = gamma*(exp(delta*ud1) - 1);
qud2  = gamma*(exp(delta*ud2) - 1);
qud3  = gamma*(exp(delta*ud3) - 1);
qud4  = gamma*(exp(delta*ud4) - 1);

% The vector yd
yd = zeros(15,1);

yd(1)  = (y(8) - 0.5*y(10) + 0.5*y(11) + y(14) - y(1)/r)/c;
yd(2)  = (y(9) - 0.5*y(12) + 0.5*y(13) + y(15) - y(2)/r)/c;
yd(3)  = (y(10) - qud1 + qud4)/cs;
yd(4)  = (-y(11) + qud2 - qud3)/cs;
yd(5)  = (y(12) + qud1 - qud3)/cs;
yd(6)  = (-y(13) - qud2 + qud4)/cs;
yd(7)  = (-y(7)/rp + qud1 + qud2 - qud3 - qud4)/cp;
yd(8)  = -y(1)/lh;
yd(9)  = -y(2)/lh;
yd(10) = (0.5*y(1) - y(3) - rg2*y(10))/ls2;
yd(11) = (-0.5*y(1) + y(4) - rg3*y(11))/ls3;
yd(12) = (0.5*y(2) - y(5) - rg2*y(12))/ls2;
yd(13) = (-0.5*y(2) + y(6) - rg3*y(13))/ls3;
yd(14) = (-y(1) + uin1 - (ri + rg1)*y(14))/ls1;
yd(15) = (-y(2) - (rc + rg1)*y(15))/ls1;
end

