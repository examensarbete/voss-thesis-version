function [f,x0,tspan,jac,sol] = ringModulator_problem()
%Defined
f = @ode.stiff.ringModulator.ringModulator_rhs;
tspan = [0,1e-3];
x0 = zeros(15,1);

%Not defined
jac = NaN;
sol = NaN;
end