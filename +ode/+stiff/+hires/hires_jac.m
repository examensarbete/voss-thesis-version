function jac = HIRES_jac(t,y)
%HIRES, a stiff system of 8 non-linear Ordinary Differential Equations. The
%name refers to "High Irradiance RESponse".
%   Detailed explanation goes here
%
% See Test Set for Initial Value Problem Solvers Francesca Mazzia and
%   Felice Iavernaro Report 40/2003
%
% 0 \leq t \ leq 321.8122
% y_0 = (1; 0; 0; 0; 0; 0; 0; 0.0057)

%Creating the vector df
jac = zeros(8,8);

jac(1,1) = -1.71;
jac(1,2) = 0.43;
jac(1,3) = 8.32;
jac(1,4) = 0;
jac(1,5) = 0;
jac(1,6) = 0;
jac(1,7) = 0;
jac(1,8) = 0;

jac(2,1) = 1.71;
jac(2,2) = - 8.75;
jac(2,3) = 0;
jac(2,4) = 0;
jac(2,5) = 0;
jac(2,6) = 0;
jac(2,7) = 0;
jac(2,8) = 0;

jac(3,1) = 0;
jac(3,2) = 0;
jac(3,3) = -10.03;
jac(3,4) = 0.43;
jac(3,5) = 0.035;
jac(3,6) = 0;
jac(3,7) = 0;
jac(3,8) = 0;

jac(4,1) = 0;
jac(4,2) = 8.32;
jac(4,3) = 1.71;
jac(4,4) = -1.12;
jac(4,5) = 0;
jac(4,6) = 0;
jac(4,7) = 0;
jac(4,8) = 0;

jac(5,1) = 0;
jac(5,2) = 0;
jac(5,3) = 0;
jac(5,4) = 0;
jac(5,5) = -1.745;
jac(5,6) = 0.43;
jac(5,7) = 0.43;
jac(5,8) = 0;

jac(6,1) = 0;
jac(6,2) = 0;
jac(6,3) = 0;
jac(6,4) = 0.69;
jac(6,5) = 1.71;
jac(6,6) = -280*y(8) - 0.43;
jac(6,7) = 0.69;
jac(6,8) = -280*y(6);

jac(7,1) = 0;
jac(7,2) = 0;
jac(7,3) = 0;
jac(7,4) = 0;
jac(7,5) = 0;
jac(7,6) = 280*y(8);
jac(7,7) = -1.81;
jac(7,8) = 280*y(6);

jac(8,1) = 0;
jac(8,2) = 0;
jac(8,3) = 0;
jac(8,4) = 0; 
jac(8,5) = 0;
jac(8,6) = -280*y(8);
jac(8,7) = 1.81;
jac(8,8) = -280*y(6);
end

