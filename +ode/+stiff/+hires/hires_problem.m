function [f,x0,tspan,jac,sol] = hires_problem()
%Defined
f = @ode.stiff.hires.hires_rhs;
jac = @ode.stiff.hires.hires_jac;
tspan = [0, 321.8122];
x0 = [1; 0; 0; 0; 0; 0; 0; 0.0057];

%Not defined
sol = NaN;
end