function [dy] = hires_rhs(t,y)
%HIRES, a stiff system of 8 non-linear Ordinary Differential Equations. The
%name refers to "High Irradiance RESponse".
%   Detailed explanation goes here
%
% See Test Set for Initial Value Problem Solvers Francesca Mazzia and
%   Felice Iavernaro Report 40/2003
%
% 0 \leq t \ leq 321.8122
% y_0 = (1; 0; 0; 0; 0; 0; 0; 0.0057)

%Creating the vector df
dy = zeros(8,1);

dy(1)  = -1.71*y(1) + 0.43*y(2) + 8.32*y(3) + 0.0007;
dy(2)  = 1.71*y(1) - 8.75*y(2);
dy(3)  = -10.03*y(3) + 0.43*y(4) + 0.035*y(5);
dy(4)  = 8.32*y(2) + 1.71*y(3) - 1.12*y(4);
dy(5)  = -1.745*y(5) + 0.43*y(6) + 0.43*y(7);
dy(6)  = -280*y(6)*y(8) + 0.69*y(4) + 1.71*y(5) - 0.43*y(6) + 0.69*y(7);
dy(7)  = 280*y(6)*y(8) - 1.81*y(7);
dy(8)  = -280*y(6)*y(8) + 1.81*y(7);
end

