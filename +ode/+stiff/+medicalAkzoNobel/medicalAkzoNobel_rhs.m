function [yd] = medicalAkzoNobel_rhs(y,t,N)
%medicalAkzoNobel: System consisting of 2 partial differential equations.
%   Detailed explanation goes here
%
% 0 \leq t \leq 20
%

%Parameters
k = 100;
c = 4;
dzeta = 1/N;

%Get the correct value of phi
if (t >= 0 && t <= 5)
    phi = 2;
else 
    phi = 0;
end

%Create the vector dy
yd = zeros(15,1);

constant = dzeta - 1;
alpha = (2*constant^3)/(c^2);
beta = (constant^4)/(c^2);
yd(1) = alpha*(y(3) - phi)/(2*dzeta) + beta*(phi - 2*y(1) + y(3))/(dzeta^2) - k*y(1)*y(2);
yd(2) = -k*y(1)*y(2);

for j=2:N-1
    constant = j*dzeta - 1;
    alpha = (2*constant^3)/(c^2);
    beta = (constant^4)/(c^2);
    yd(2*j-1) = alpha*(y(2*j+1) - y(2*j-3))/(2*dzeta) + beta*(y(2*j-3) - 2*y(2*j-1) + y(2*j+1))/(dzeta^2) - k*y(2*j-1)*y(2*j);
    yd(2*j) = -k*y(2*j)*y(2*j-1);
end

constant = N*dzeta - 1;
alpha = (2*constant^3)/(c^2);
beta = (constant^4)/(c^2);
yd(2*N-1) = alpha*(y(2*N-1) - y(2*N-3))/(2*dzeta) + beta*(y(2*N-3) - 2*y(2*N-1) + y(2*N-1))/(dzeta^2) - k*y(2*N-1)*y(2*N);
yd(2*N)   = -k*y(2*N-1)*y(2*N);
end

