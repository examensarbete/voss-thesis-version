function [f,x0,tspan,jac,sol] = medicalAkzoNobel_problem(n)
%Defined
f = @(t,y) ode.stiff.medicalAkzoNobel.medicalAkzoNobel_rhs(y,t,n);
tspan = [0,20];
x0 = zeros(2*n,1);
x0(2:2:end) = 1;

%Not defined
sol = NaN;
jac = NaN;
end