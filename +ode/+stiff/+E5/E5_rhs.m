function [yd] = E5_rhs(t,y)
%E5: Summary of this function goes here
%   Detailed explanation goes here
%
% y_0 = (1.76e-3;0;0;0)
%

% Creating the k vector
k = zeros(4,1);

k(1) = 7.89e-10;
k(2) = 1.13e9;
k(3) = 1.1e7;
k(4) = 1.13e3;

% Creating the vector yd
yd = zeros(4,1);

yd(1) = -k(1)*y(1) - k(3)*y(1)*y(3);
yd(2) = k(1)*y(1) - k(2)*y(2)*y(3);
yd(3) = k(1)*y(1) - k(2)*y(2)*y(3) - k(3)*y(1)*y(3) + k(4)*y(4);
yd(4) = k(3)*y(1)*y(3) - k(4)*y(4);
end

