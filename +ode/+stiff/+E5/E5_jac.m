function jac = E5_jac(t,y)
%
%

k = zeros(4,1);

k(1) = 7.89e-10;
k(2) = 1.13e9;
k(3) = 1.1e7;
k(4) = 1.13e3;

% Creating the vector yd
jac = zeros(4,4);

jac(1,1) = -k(1) - k(3)*y(3);
jac(1,2) = 0;
jac(1,3) = -k(3)*y(1);
jac(1,4) = 0;

jac(2,1) = k(1);
jac(2,2) = - k(2)*y(3);
jac(2,3) = - k(2)*y(2);
jac(2,4) = 0;

jac(3,1) = k(1) - k(3)*y(3);
jac(3,2) = - k(2)*y(3);
jac(3,3) = - k(2)*y(2) - k(3)*y(1);
jac(3,4) = k(4);

jac(4,1) = k(3)*y(3);
jac(4,2) = 0;
jac(4,3) = k(3)*y(1);
jac(4,4) = -k(4);
end

