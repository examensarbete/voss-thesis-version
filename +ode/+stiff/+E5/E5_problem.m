function [f,x0,tspan,jac,sol] = E5_problem()
%Defined
f = @ode.stiff.E5.E5_rhs;
jac = @ode.stiff.E5.E5_jac;
tspan = [0,1e13];
x0 = [1.76e-3; 0; 0; 0];

%Not Defined
sol = NaN;
end