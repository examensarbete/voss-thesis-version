function [f,x0,tspan,jac,sol] = B3_problem()
f = @ode.nonStiff.B3.B3_rhs;
jac = @ode.nonStiff.B3.B3_jac;
tspan = [0,20];
x0 = [0;1;0;1;0;1];

sol = NaN;
end

