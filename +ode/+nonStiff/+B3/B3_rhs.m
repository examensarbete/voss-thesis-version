function dy = B3_rhs(t,y)
%RHS function for B3

%Creating the vector dy
dy = zeros(6,1);

dy(1)  = -10*y(1) + 50*y(2);
dy(2)  = -50*y(1) - 10*y(2);
dy(3)  = -40*y(3) + 200*y(4);
dy(4)  = -200*y(3) - 40*y(4);
dy(5)  = -0.2*y(5) + 2*y(6);
dy(6)  = -2*y(5) - 0.2*y(6);
end

