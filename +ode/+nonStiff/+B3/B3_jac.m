function jac = B3_jac(t,y)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
dy(1)  = -10*y(1) + 50*y(2);
dy(2)  = -50*y(1) - 10*y(2);
dy(3)  = -40*y(3) + 200*y(4);
dy(4)  = -200*y(3) - 40*y(4);
dy(5)  = -0.2*y(5) + 2*y(6);
dy(6)  = -2*y(5) - 0.2*y(6);

%jac function
jac = zeros(6,6);

jac(1,1) = -10;
jac(1,2) = 50;

jac(2,1) = -50;
jac(2,2) = -10;

jac(3,3) = -40;
jac(3,4) = 200;

jac(4,3) = -200;
jac(4,4) = -40;

jac(5,5) = -0.2;
jac(5,6) = 2;

jac(6,5) = -2;
jac(6,6) = -0.2;
end

