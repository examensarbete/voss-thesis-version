function [yp]=lotka_rhs(t,y)

alpha= 0.1;
beta = 0.3;
gamma= 0.5;
delta = 0.5;
yp = 30*[alpha*y(1)-beta*y(1).*y(2);
 -gamma*y(2)+delta*y(1).*y(2)];
