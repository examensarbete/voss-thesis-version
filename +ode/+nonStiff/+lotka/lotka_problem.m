function [f,x0,tspan,jac,sol] = lotka_problem(cycles)
f = @ode.nonStiff.lotka.lotka_rhs;
jac = @ode.nonStiff.lotka.lotka_jac;
tspan = [0,cycles];
x0 = [1;1];

sol = NaN;
end
