function jac=lotka_jac(t,y)
alpha= 0.1;
beta = 0.3;
gamma= 0.5;
delta = 0.5;
jac = 30*[alpha-beta*y(2)     -beta*y(1);
         delta*y(2)          -gamma+delta*y(1)];
