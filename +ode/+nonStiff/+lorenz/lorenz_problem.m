function [f,x0,tspan,jac,sol] = lorenz_problem()
f = @ode.nonStiff.lorenz.lorenz_rhs;
jac = @ode.nonStiff.lorenz.lorenz_jac;
tspan = [0,7];
x0 = [-8; 8; 27];

sol = NaN;
end