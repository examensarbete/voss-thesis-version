function dy = lorenz_rhs(t,y)
%The Lorenz problem
%
%Use the following:
%   y0 = [-8,8,27]
%   tspan = [0,7]

%Parameters
b = 8/3;
lambda = 28;
sigma = 10;

%RHS
dy = zeros(3,1);

dy(1) = -sigma*(y(1) - y(2));
dy(2) = y(1)*(lambda - y(3)) - y(2);
dy(3) = y(1)*y(2) - b*y(3);
end

