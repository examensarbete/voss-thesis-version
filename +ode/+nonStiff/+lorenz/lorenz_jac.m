function jac = lorenz_jac(t,y)
%The Lorenz problem
%

%Parameters
b = 8/3;
lambda = 28;
sigma = 10;

%jac function
jac = zeros(3,3);

jac(1,1) = -sigma;
jac(1,2) = sigma;
jac(1,3) = 0;

jac(2,1) = lambda - y(3);
jac(2,2) = -1;
jac(2,3) = -y(1);

jac(3,1) = y(2);
jac(3,2) = y(1);
jac(3,3) = -b;
end

