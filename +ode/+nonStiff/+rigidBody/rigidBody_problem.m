function [f,x0,tspan,jac,sol] = rigidBody_problem()
%Defined
f = @ode.nonStiff.rigidBody.rigidBody_rhs;
jac = @ode.nonStiff.rigidBody.rigidBody_jac;

tspan = [0,3];
x0 = [1;1;1];

%Not defined
sol = NaN;
end