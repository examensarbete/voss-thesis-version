function yp = rigidBody_rhs(t,y)
%RHS function of a nonstiff equation for testing purposes, modelling
%a rigid body
%
%This is a nonstiff system of Euler equations, modelling a rigid body, for
%benchmarking and testing purposes. The system of equations is
%
%.. math::
%
%   \begin{cases}
%   \dot y_1 =y_2 y_3
%   \dot y_2 = -y_1 y_3
%   \dot y_3 = -0.51y_1 y_2
%   \end{cases}
%
%source: http://se.mathworks.com/help/matlab/math/ordinary-differential-equations.html#f1-40077
%
%Parameters
%----------
%t : double
%   The current time
%y : vector
%   The current solution
%
%Returns
%-------
%yp : vector
%   The derivative of y in the current point

yp = [y(2)*y(3) ; -y(1)*y(3) ; -0.51*y(1)*y(2)];
end
