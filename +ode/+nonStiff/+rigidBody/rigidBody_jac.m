function jac = rigidBody_jac(t,y)
%

jac = zeros(3,3);

jac(1,1) = 0;
jac(1,2) = y(3);
jac(1,3) = y(2);

jac(2,1) = -y(3);
jac(2,2) = 0;
jac(2,3) = -y(1);

jac(3,1) = -0.51*y(2);
jac(3,2) = -0.51*y(1);
jac(3,3) = 0;
end

