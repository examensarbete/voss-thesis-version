function jac = brusselator_jac(t,y)
%the Brusselator
%

%Parameters
A = 2;
B = 8.533;

%jac function
jac = zeros(2,2);

jac(1,1) = 2*y(1)*y(2) - (B + 1);
jac(1,2) = y(1)^2;

jac(2,1) = B - 2*y(1)*y(2);
jac(2,2) = - y(1)^2;
end

