function [f,x0,tspan,jac,sol] = brusselator_problem()
f = @ode.nonStiff.brusselator.brusselator_rhs;
jac = @ode.nonStiff.brusselator.brusselator_jac;
tspan = [0,20];
x0 = [1;4.2665];

sol = NaN;
end