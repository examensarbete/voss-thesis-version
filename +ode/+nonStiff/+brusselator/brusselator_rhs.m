function dy = brusselator_rhs(t,y)
%the Brusselator
%
%Use the following:
%   y0 = [1,4.2665]
%   tspan = [0,20]

%Parameters
A = 2; %2
B = 5; %8.533

%RHS function
dy = zeros(2,1);

dy(1) = A + y(1)^2*y(2) - (B + 1)*y(1);
dy(2) = B*y(1) - y(1)^2*y(2);
end

