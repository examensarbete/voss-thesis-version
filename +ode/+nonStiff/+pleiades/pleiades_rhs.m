function [f] = pleiades_rhs(t,y)
%pleiades: Summary of this function goes here
%   Detailed explanation goes here
%
% 0 \leq t \leq 3
%
% x_0 = (3;3;-1;-3;2;-2;2)
% y_0 = (3;-3;2;0;0;-4;4)
% xd_0 = (0;0;0;0;0;1.75;-1.5)
% yd_0 = (0;0;0;-1.25;1;0;0)
% Y_0 = (x_0; y_0; xd_0; yd_0)
%

%Creating the vector dy
f = zeros(28,1);

for i=1:7
    sumX = 0;
    sumY = 0;
    for j=1:7
        rij = (y(i) - y(j))^2 + (y(i+7) - y(j+7))^2;
        rij32 = rij^(3/2);
        if (j ~= i)
            sumX = sumX + j*(y(j) - y(i))/rij32;
            sumY = sumY + j*(y(j+7) - y(i+7))/rij32;
        end
    end
    f(i) = y(i+14);
    f(i+14) = sumX;
    f(i+21) = sumY;
end
for i=8:14
    f(i) = y(i+14);
end
end

