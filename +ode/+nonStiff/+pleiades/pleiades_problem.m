function [f,x0,tspan,jac,sol] = pleiades_problem()
%Defined
f = @ode.nonStiff.pleiades.pleiades_rhs;
tspan = [0,3];
x_0 = [3; 3; -1; -3; 2; -2; 2];
y_0 = [3; -3; 2; 0; 0; -4; 4];
xd_0 = [0; 0; 0; 0; 0; 1.75; -1.5];
yd_0 = [0; 0; 0; -1.25; 1; 0; 0];
x0 = [x_0; y_0; xd_0; yd_0];

%Not defined
jac = NaN;
sol = NaN;
end