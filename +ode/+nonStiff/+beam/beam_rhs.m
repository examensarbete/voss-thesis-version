function [f] = beam_rhs(t,y)
%RHS function modelling the motion of an elastic, inextensible beam of
%length 1. The beam is assumed to be thin and is clamped at one end and a
%force $F = (F_u, F_v)$ acts on the other (free) end.
%
% y will be of size 2*n. The first n values are z, the second n values are
% z'.
%
%:param t: Current time
%:param y: Current solution ()
%:returns: :math:`\dot y`

%If y is send in as a row vector we change it to a column vector otherwise
%we will have incorrect dimensions.
rowVectorIn = false;
if(tools.rowVector(y))
    y = y';
    rowVectorIn = true;
end

n = length(y)/2;
C = zeros(n,n);
D = zeros(n,n);
v = zeros(n,1);

%Create the forces Fx and Fy
if  (t >= 0 && t <= pi)
    phi = 1.5*(sin(t))^2;
else
    phi = 0;
end
Fx =-phi;
Fy = phi;

% Create the matrices C and D
C(1,1) = 1;
C(1,2) = -cos(y(1)-y(2));
D(1,2) = -sin(y(1)-y(2));
v(1) = n^4*(-y(1) - 2*y(1) + y(2)) + n^2*(cos(y(1))*Fy - sin(y(1))*Fx);
for i=2:n-1
    C(i,i) = 2;
    C(i,i+1) = -cos(y(i)-y(i+1));
    C(i,i-1) = -cos(y(i)-y(i-1));
    D(i,i+1) = -sin(y(i)-y(i+1));
    D(i,i-1) = -sin(y(i)-y(i-1));
    v(i) = n^4*(y(i-1) - 2*y(i) + y(i+1)) + n^2*(cos(y(i))*Fy - sin(y(i))*Fx);
end
C(n,n) = 3;
C(n,n-1) = -cos(y(n)-y(n-1));
D(n,n-1) = -sin(y(n)-y(n-1));
v(n) = n^4*(y(n-1) - 2*y(n) + y(n)) + n^2*(cos(y(n))*Fy - sin(y(n))*Fx);

% Creating the vector g and f
g = D*v + y(n+1:end).^2;
u = C\g;
f1 = C*v + D*u;
f = [y(n+1:end);f1];

%If y was sent in as a row vector, then we send out f as a row vector
%(and the other way around)
if(rowVectorIn)
    f = f';
end

end
