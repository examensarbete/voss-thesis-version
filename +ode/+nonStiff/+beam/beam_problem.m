function [f,x0,tspan,jac,sol] = beam_problem(n)
%Defined
f = @ode.nonStiff.beam.beam_rhs;
tspan = [0,5];
x0 = zeros(2*n,1);

%Not defined
sol = NaN;
jac = NaN;
end