function jac = rtbo1_jac(t,y)
%RTBo1
%

%Parameters and temps
re = 0.01227747;
req = 1 - re;
rn1 = ((y(1) + re)^2 + y(2)^2)^(1.5);
rn2 = ((y(1) - req)^2 + y(2)^2)^(1.5);
rn1pow = rn1^(5/3);
rn2pow = rn2^(5/3);

%jac function
jac = zeros(4,4);

jac(1,1) = 0;
jac(1,2) = 0;
jac(1,3) = 1;
jac(1,4) = 0;

jac(2,1) = 0;
jac(2,2) = 0;
jac(2,3) = 0;
jac(2,4) = 1;

jac(3,1) = 1 - req/rn1 + 3*req*(y(1) + re)^2/rn1pow - re/rn2 + 3*re*(y(1) - req)^2/rn2pow;
jac(3,2) = 3*req*(y(1) + re)*y(2)/rn1pow + 3*re*(y(1) - req)*y(2)/rn2pow;
jac(3,3) = 0;
jac(3,4) = 2;

jac(4,1) = jac(3,2);
jac(4,2) = 1 - req/rn1 + 3*req*y(2)^2/rn1pow - re/rn2 + 3*re*y(2)^2/rn2pow;
jac(4,3) = -2;
jac(4,4) = 0;
end

