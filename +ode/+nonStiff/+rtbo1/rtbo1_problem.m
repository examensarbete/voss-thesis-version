function [f,x0,tspan,jac,sol] = rtbo1_problem()
%Defined
f = @ode.nonStiff.rtbo1.rtbo1_rhs;
jac = @ode.nonStiff.rtbo1.rtbo1_jac;
tspan = [0,35];
x0 = [0.994; 0; 0; -2.001585106];

%Not defined
sol = NaN;
end
