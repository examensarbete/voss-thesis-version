function dy = rtbo1_rhs(t,y)
%RTBo1
%
%Use the following:
%   y0 = [0.994,0,0,-2.001585106]
%   tspan = [0,35]

%Parameters and temps
re = 0.01227747;
req = 1 - re;
rn1 = ((y(1) + re)^2 + y(2)^2)^(1.5);
rn2 = ((y(1) - req)^2 + y(2)^2)^(1.5);

%RHS function
dy = zeros(4,1);

dy(1) = y(3);
dy(2) = y(4);
dy(3) = y(1) + 2*y(4) - req*(y(1) + re)/rn1 - re*(y(1) - req)/rn2;
dy(4) = y(2) - 2*y(3) - req*y(2)/rn1 - re*y(2)/rn2;
end

