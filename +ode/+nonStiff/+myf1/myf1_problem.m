function [f,x0,tspan,jac,sol] = myf1_problem()
%Defined
mu = 1;
[B,sol,analyticalFuncDot, nonLinPart, f, jac, lambda] = ode.both.f1(mu);
tspan = [0,10];
x0 = sol(0)';
end