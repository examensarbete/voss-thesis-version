function [f,x0,tspan,jac,sol] = twoExponentials_problem(lambda)
f = @(t,y)ode.both.twoExponentials.twoExponentials_rhs(t,y,lambda(1),lambda(2));
jac = @(t,y)ode.both.twoExponentials.twoExponentials_jac(t,y,lambda(1),lambda(2));
sol = @(t)ode.both.twoExponentials.twoExponentials_sol(t,lambda(1),lambda(2));
x0 = [0;2];
tspan = [0,10];
end
