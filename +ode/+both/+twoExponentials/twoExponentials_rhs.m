function dy = twoExponentials_rhs(t,y,lambda1,lambda2)
%Two Exponentials
%
%Use the following:
%   y0 = [0,2];
%   tspan = [0,10];
%
%This artificial test problem (very stiff using the lambdas below) has the known solution
%
% y_1 = -exp(lambda1*t) + exp(lambda2*t)
% y_2 = exp(lambda1*t) + exp(lambda2*t)
%
%The parameters lambda1 and lambda2 are the eigenvalues of the Jacobian.
%
%Reference:
%http://num-lab.zib.de/public/cgi-bin/odelabnew?formid=2&userid=josefine&workbench=Documents

%lambda1 = -100000;
%lambda2 = -1;

dy = zeros(2,1);

dy(1) = (lambda1 + lambda2)*y(1)/2 + (-lambda1 + lambda2)*y(2)/2;
dy(2) = (-lambda1 + lambda2)*y(1)/2 + (lambda1 + lambda2)*y(2)/2;
end

