function [y] = twoExponentials_sol(t,lambda1,lambda2)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if (length(t) == 1)
    f = @(t) [-exp(lambda1*t) + exp(lambda2*t);
    exp(lambda1*t) + exp(lambda2*t)];
else
    f = @(t) [-exp(lambda1*t') + exp(lambda2*t');
    exp(lambda1*t') + exp(lambda2*t')]';
end
y = f(t);
end

