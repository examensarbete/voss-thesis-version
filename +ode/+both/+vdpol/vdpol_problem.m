function [f,x0,tspan,jac,sol] = vdpol_problem(mu)
%Defined
f = @(t,y)ode.both.vdpol.vdpol_rhs(t,y,mu);
jac = @(t,y)ode.both.vdpol.vdpol_jac(t,y,mu);
tspan = [0,4*mu];
x0 = [2;0];

%Not defined
sol = NaN;
end