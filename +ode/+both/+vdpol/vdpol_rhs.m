function xd = vdpol_rhs(t,x,mu)
% This is the RHS function to the Van der Pol equation which models stable
% oscillations in electrical circuits.
% 
% The equation (formulated as a system of first order equations) is as
% follows:
% 
% .. math::
% 
%    \begin{cases}
%    x_1 = x_2 \\
%    x_2 = \mu(1-x_1^2)x_2 - x_1
%    \end{cases}
% 
% Stiffness of the eqution can be regulated by changing the parameter
% :math:`\mu` (increasing values of :math:`\mu` gives an increasing
% stiffness).
% 
% Parameters
% ----------
% t : float
%    The current time
% x : column vector
%    The current solution
% mu : float
%    Damping and non-linearity factor (increasing this increases the
%    stiffness)
% 
% Returns
% -------
% xd : column vector
%    The derivative of the current solution

xd = [x(2); mu*(1-x(1)^2)*x(2)-x(1)];
end
