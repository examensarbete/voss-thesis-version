function jac = vdpol_jac(t,x,mu)
%The Jacobian of the of the Van der Pol function.
%
%This function may be used when there is a need for supplying a Jacobian of
%the equation. It is important that all the parameters have the same values
%as those used with the original RHS function.
%
%Parameters
%----------
%t : float
%   The current time
%x : column vector
%   The current solution
%mu : float
%   Damping and non-linearity factor (increasing this increases the
%   stiffness)
%
%Returns
%-------
%jac : matrix
%   The Jacobian of the RHS function

jac = [      0                        1;
      -2*mu*x(1)*x(2)-1  mu*(1-x(1)^2)];
end
