function [f,x0,tspan,jac,sol] = decayingExpo_problem(p)
f = @(t,y)ode.both.decayingExpo.decayingExpo_rhs(p,t,y);
jac = @(t,y)ode.both.decayingExpo.decayingExpo_jac(p,t,y);
sol = @(t)ode.both.decayingExpo.decayingExpo_sol(p,t);
tspan = [0, 10];
x0 = 1;
end