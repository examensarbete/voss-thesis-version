function dy = decayingExpo_rhs(p,t,y)
%Decaying Exponential
%
%Use the following:
%   y0 = [1]
%   tspan = [0,10]
%
%Reference:
%http://num-lab.zib.de/public/cgi-bin/odelabnew?formid=2&userid=josefine&workbench=Documents

dy = -p*y;
end

