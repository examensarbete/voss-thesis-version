function jac = ribbons_jac(t,x,mu,gamma)
%The Jacobian of the of the ribbon function.
%
%This function may be used when there is a need for supplying a Jacobian of
%the equation. It is important that all the parameters have the same values
%as those used with the original RHS function.
%
%Parameters
%----------
%t : float
%   The current time
%x : column vector
%   The current solution in the form of a column vector of length 2
%mu : float
%   This parameter controls the stiffnes of the equation. Larger values
%   gives a stiffer equation.
%gamma : float
%   This parameter controls the non-linearity of the equation. A larger
%   value gives an equation which exhibits more non-linearity.
%
%Returns
%-------
%jac : matrix
%   The Jacobian of the RHS function 

f = @(t) [6*sin(2.31*t).*cos(cos(16.1*t)).^6;
    6*cos(2.31*t).^2.*sin(sin(16.1*t)).^2];
A = [-2,-1;-3,-2];
J_phi = @(x) A*mu+gamma*[x(2)-3*exp(-3*x(1)),x(1);cos(x(1)-cos(x(2))),sin(x(2))*cos(x(1)-cos(x(2)))];
jac = J_phi(x-f(t));
end
