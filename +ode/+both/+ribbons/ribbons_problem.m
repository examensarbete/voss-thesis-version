function [f,x0,tspan,jac,sol] = ribbons_problem(mu,gamma)
f = @(t,y)ode.both.ribbons.ribbons_rhs(t,y,mu,gamma);
jac = @(t,y)ode.both.ribbons.ribbons_jac(t,y,mu,gamma);
sol = @ode.both.ribbons.ribbons_sol;
x0 = sol(0);
tspan = [0,3];
end
