function xd = ribbons_rhs(t,x,mu,gamma)
%RHS function of an equation for testing purposes.
%
%This eqution is not based on a physical model, but is constructed to have
%an analytical solution. The phase plot of this equation creates a ribbon
%like pattern.
%
%Stiffnes can be regulated by changing the paramater
%
%Parameters
%----------
%t : float
%   The current time
%x : column vector
%   The current solution in the form of a column vector of length 2
%mu : float
%   This parameter controls the stiffnes of the equation. Larger values
%   gives a stiffer equation.
%gamma : float
%   This parameter controls the non-linearity of the equation. A larger
%   value gives an equation which exhibits more non-linearity.
%
%Returns
%-------
%xd : column vector
%   The time derivative of the solution in the current point

f = @(t) [6*sin(2.31*t).*cos(cos(16.1*t)).^6;
    6*cos(2.31*t).^2.*sin(sin(16.1*t)).^2];
fd = @(t)[2.31.*cos(2.31.*t).*cos(cos(16.1.*t)).^6.*6.0+16.1.*sin(cos(16.1.*t)).*sin(2.31.*t).*sin(16.1.*t).*cos(cos(16.1.*t)).^5.*3.6e1;2.31.*sin(sin(16.1.*t)).^2.*cos(2.31.*t).*sin(2.31.*t).*-1.2e1+16.1.*cos(sin(16.1.*t)).*sin(sin(16.1.*t)).*cos(2.31.*t).^2.*cos(16.1.*t).*1.2e1];
A = [-2,-1;-3,-2];
nonLinearPart = @(x) [x(1).*x(2)+exp(-3*x(1));sin(x(1) - cos(x(2)))];

phi = @(x) mu*A*x + gamma*nonLinearPart(x);

xd = phi(x - f(t)) + fd(t);

end
