function sol = ribbons_sol(t)
%This function provides the solution (calculated from the analytical
%solution) to the ribbon equation.
%
%This function can be used when one needs the exact solution (up to machine
%precision) of the butterfly equation, for example when the error is to be
%calculated.
%
%Parameters
%----------
%t : float
%   The current time
%
%Returns
%-------
%sol : column vector
%   The solution to the butterfly equation
if (length(t) == 1)
    f = @(t) [6*sin(2.31.*t).*cos(cos(16.1.*t)).^6;
    6*cos(2.31.*t).^2.*sin(sin(16.1.*t)).^2];
else
    f = @(t) [6*sin(2.31.*t').*cos(cos(16.1.*t')).^6;
    6*cos(2.31.*t').^2.*sin(sin(16.1.*t')).^2]';
end
sol = f(t);
end
