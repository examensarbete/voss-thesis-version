function jac = flameprop_jac(t,y)
%The Jacobian of the of the flame propagation function.
%
%This function may be used when there is a need for supplying a Jacobian of
%the equation.
%
%Parameters
%----------
%t : float
%   The current time
%x : float
%   The current solution (flame radius)
%
%Returns
%-------
%jac : float
%   The Jacobian of the RHS function 

jac = 2*y - 3*y.^2;
end
