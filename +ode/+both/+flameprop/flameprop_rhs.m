function xd = flameprop_rhs(t,x)
%RHS function of an equation for testing purposes modelling flame
%propagation
%
%This is an equation modelling flame propagation which can be used for
%benchmarking and testing purposes. The equation is
%
%.. math::
%
%   \begin{cases}
%   \dot x = x^2 - x^3\\
%   x(0) = \delta\\
%   0 < t < 2/\delta
%   \end{cases}
%
%Stiffnes can be regulated by changing the initial condition
%smaller :math:`\delta` gives a stiffer equation
%
%source: http://se.mathworks.com/company/newsletters/articles/stiff-differential-equations.html
%
%Parameters
%----------
%t : float
%   The current time
%x : float
%   The current solution (flame radius)
%
%Returns
%-------
%xd : float
%   The time derivative of the current solution
xd = [x^2-x^3];
end
