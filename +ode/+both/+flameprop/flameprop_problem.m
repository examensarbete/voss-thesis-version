function [f,x0,tspan,jac,sol] = flameprop_problem(a)
%Defined
f = @ode.both.flameprop.flameprop_rhs;
jac = @ode.both.flameprop.flameprop_jac;
x0 = 10^-a;
tspan = [0,2/x0];

sol = @(t)ode.both.flameprop.flameprop_sol(x0,t);
end
