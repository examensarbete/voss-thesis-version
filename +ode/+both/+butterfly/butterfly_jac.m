function jac = butterfly_jac(t,x,mu,gamma)
%The Jacobian of the of the butterfly function.
%
%This function may be used when there is a need for supplying a Jacobian of
%the equation. It is important that all the parameters have the same values
%as those used with the original RHS function.
%
%Parameters
%----------
%t : float
%   The current time
%x : column vector
%   The current solution in the form of a column vector of length 2
%mu : float
%   This parameter controls the stiffnes of the equation. Larger values
%   gives a stiffer equation.
%gamma : float
%   This parameter controls the non-linearity of the equation. A larger
%   value gives an equation which exhibits more non-linearity.
%
%Returns
%-------
%jac : matrix
%   The Jacobian of the RHS function 

f = @(t) [(exp(cos(t)) - sin(t/12).^5 - 2.*cos(4.*t)).*sin(t);...
    (exp(cos(t)) - sin(t/12).^5 - 2.*cos(4.*t)).*cos(t)];
A = [-2,-1;-3,-2];
J_phi = @(x) A*mu+gamma*[x(2), x(1); -1, 3*x(2).^2];
jac = J_phi(x-f(t));
end
