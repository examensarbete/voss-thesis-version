function sol = butterfly_sol(t)
%This function provides the solution (calculated from the analytical
%solution) to the butterfly equation.
%
%This function can be used when one needs the exact solution (up to machine
%precision) of the butterfly equation, for example when the error is to be
%calculated.
%
%Parameters
%----------
%t : float
%   The current time
%
%Returns
%-------
%sol : column vector
%   The solution to the butterfly equation

if (length(t) == 1)
    f = @(t) [(exp(cos(t)) - sin(t/12).^5 - 2.*cos(4.*t)).*sin(t);...
        (exp(cos(t)) - sin(t/12).^5 - 2.*cos(4.*t)).*cos(t)];
else
    f = @(t) [(exp(cos(t')) - sin(t'/12).^5 - 2.*cos(4.*t')).*sin(t');...
        (exp(cos(t')) - sin(t'/12).^5 - 2.*cos(4.*t')).*cos(t')]';
end
sol = f(t);
end
