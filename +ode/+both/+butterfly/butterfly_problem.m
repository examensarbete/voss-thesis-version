function [f,x0,tspan,jac,sol] = butterfly_problem(mu,gamma)
f = @(t,y)ode.both.butterfly.butterfly_rhs(t,y,mu,gamma);
jac = @(t,y)ode.both.butterfly.butterfly_jac(t,y,mu,gamma);
sol = @ode.both.butterfly.butterfly_sol;
x0 = sol(0);
tspan = [0,10];
end