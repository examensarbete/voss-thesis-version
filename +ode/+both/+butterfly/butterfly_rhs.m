function xd = butterfly_rhs(t,x,mu,gamma)
%RHS function of an equation for testing purposes.
%
%This eqution is not based on a physical model, but is constructed to have
%an analytical solution. The phase plot of this equation creates a so called
%butterfly curve.
%
%Stiffnes can be regulated by changing the paramater
%
%Parameters
%----------
%t : float
%   The current time
%x : column vector
%   The current solution in the form of a column vector of length 2
%mu : float
%   This parameter controls the stiffnes of the equation. Larger values
%   gives a stiffer equation.
%gamma : float
%   This parameter controls the non-linearity of the equation. A larger
%   value gives an equation which exhibits more non-linearity.
%
%Returns
%-------
%xd : column vector
%   The time derivative of the solution in the current point

f = @(t) [(exp(cos(t)) - sin(t/12).^5 - 2.*cos(4.*t)).*sin(t);...
    (exp(cos(t)) - sin(t/12).^5 - 2.*cos(4.*t)).*cos(t)];
fd = @(t) [(-exp(cos(t)).*sin(t)-5.*sin(t/12).^4.*cos(t/12)/12+...
    8.*sin(4.*t)).*sin(t)+(exp(cos(t))-sin(t/12).^5-2.*cos(4.*t)).*cos(t);...
    (-exp(cos(t)).*sin(t)-5.*sin(t/12).^4.*cos(t/12)/12+...
    8.*sin(4.*t)).*cos(t)-(exp(cos(t))-sin(t/12).^5-2.*cos(4.*t)).*sin(t)];

A = [-2,-1;-3,-2];
nonLinearPart = @(x) [x(1).*x(2); x(2).^3 - x(1)];
phi = @(x) mu*A*x + gamma*nonLinearPart(x);

xd = phi(x - f(t)) + fd(t);

end
