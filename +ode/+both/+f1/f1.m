function [B,f,fd, nonLinPart, RHS, jac, lambda] = f1(mu)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%Creating the matrix
A = [-2 -1; -1 -2];
B = mu*A;
lambda = eig(B);

%Creating the analytical solution and it's derivative
f = @(t) [sin(t), cos(t)];
fd = @(t) [cos(t), -sin(t)];

%Creating the nonlinear part
nonLinPart = @(y) [y(1)*y(2);y(2)^3];

%Creating RHS and its jacobian
RHS = @(t,y) tools.newFunc(B, f, fd, nonLinPart, t, y);
jac = @(t,y) solverFunctions.getJac(RHS,t,y);
end

