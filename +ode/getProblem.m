function [f,x0,tspan,jac,sol] = getProblem(problemName,varargin)
%Wrapper function used to get the availible quantities belonging to the
%problem problemName.
%
%All problems are defined in codebase/+ode/+both, codebase/+ode/+stiff and
%codebase/+ode/+nonStiff depending on the stiffness of the problem. Every
%problem is in its own subpackage .../+problemName. Inside this folder
%there is at most four functions called problemName_problem.m, problemName_rhs.m,
%(these two has to exist, otherwise will you get a matlab error)
%problemName_jac.m,and problemName_sol.m.
%
%Parameters
%----------
%problemName : function handle
%   The jacobian of which we want to calculate the eigenvalues over
%   time.
%varargin : parameters belonging to the problem
%   If the problem problemName has a parameter, this has to be supplied.
%   For example, to shoose Van der Pol (problemName == vdp) you have to set
%   the parameter mu. This means you have to do the following call:
%       getProblem(vdp,mu),
%   where mu is an appropriate float. Some problems do not have a
%   parameter, then you make the following call instead (oregonator):
%       getProblem(oregonator).
%
%Returns
%-------
%f : vector
%   The RHS function belonging to the problem problemName.
%x0 : vector
%   The initial condition belonging to the problem problemName.
%tspan : vector
%   The time span belonging to the problem problemName. tspan = [t_0,t_f],
%   where t_0 is the initial time and t_f is the final time between which
%   we want a solution by the solver.
%jac : vector
%   The jacobian belonging to the problem problemName. If unknown this will
%   have the value None.
%sol : vector
%   The analytical solution belonging to the problem problemName. If
%   unknown this will have the value None.

import ode.both.*
import ode.nonStiff.*
import ode.stiff.*

name = [problemName,'.',problemName,'_problem']; 
func = str2func(name);
[f,x0,tspan,jac,sol] = func(varargin{:});


