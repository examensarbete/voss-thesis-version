function [r,a,parvec] = getPIDpoly(kI,kP,kD,order)
%Calculates the eigenvalues and filter vector of the specified
%PID-controller.
%
%Parameters
%----------
%kI : float
%   The integral gain of the specified filter.
%kP : float
%   The proportional gain of the specified filter.
%kD : float
%   The derivative gain of the specified filter.
%order : int
%   The order of the method the filter is going to use.
%
%Returns
%-------
%r : vector
%   Row matrix containing the eigenvalues belonging to the specified
%   filter.
%a : vector
%   Row matrix containing the absolute values of the eigenvalues belonging
%   to the specified filter.
%parvec : vector
%   Row matrix. The filter vector corresponding to the specified filter.

c1 = 1;
c2 = (kI*order + kP*order + kD*order - 1);
c3 = (kP*order + 2*kD*order);
c4 = kD;

p = @(q) c1*q^3 + c2*q^2 + c3*q + c4;
pCooef = [c1 c2 c3 c4];
r = roots(pCooef);
a = abs(r);
beta1 = kI + kP + kD;
beta2 = -(kP + 2*kD);
beta3 = kD;
parvec = [beta1*order beta2*order beta3*order 0 0]
end