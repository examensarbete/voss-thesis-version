function [acc,tolVec,statistics] = accVsTolPar(solver, RHS, tolVec, stiff, tSpan, x0, varargin)
% Calculates the accuracy of a method for a given set of tolerances.
%
% This function may be used to facilitate testing. It automatically
% calculates the accuracy of a given method for a set of tolerances. The
% reference solution used to determine the accuracy is calculated by using
% Matlab's own solvers with high accuracy.
%
% Parameters
% ----------
% solver : function handle
%   The solver, which takes the parameters (rhs,tSpan,x0,tol)
%
% rhs : function handle
%   The right hand side function
%
% tolVec : vector
%   The tolerances for which the accuracy is to be calculated
%
% stiff : boolean
%   Wether or not the equation to be solved is stiff (used for choosing
%   solver when calculating the reference solution)
%
% tSpan : vector
%   A vector with two elements, containing the initial and final time.
%
% x0 : (column) vector
%   The initial conditions
%
% Returns
% -------
% acc : vector
%   Vector containing the accuracies

%Initialization
nbrOfArgsOutSolver = nargout(solver);
acc = zeros(1,length(tolVec));
i = 0;
T = [];
myT = cell(1,length(tolVec));
myX = cell(1,length(tolVec));
statistics = cell(1,length(tolVec));
useAnalytical = false;

%Default values on the extra arguments
optionsReference = odeset('relTol',1e-13,'absTol',1e-16);
errorcalcmode = 'abserr';

%Check for user arguments overriding the default values
for j = 1:2:length(varargin)
    name = varargin{j};
    value = varargin{j+1};
    switch lower(name)
        case 'refoptions'
            optionsReference = value;
        case 'errorcalcmode'
            errorcalcmode = value;
        case 'analyticalsolution'
            x = value;
            useAnalytical = true;
    end
end


display('Calculating solutions');
parfor i=1:length(tolVec)
    % Solution of the solver we are analyzing
    if (nbrOfArgsOutSolver == 2)
        %Matlab solvers only outputs [t, x]
        [myT{i},myX{i}] = solver(RHS,tSpan,x0,tolVec(i));
        statistics{i} = {};
    else
        %Our solvers output [t, x,statistics]
        [myT{i},myX{i},statistics{i}] = solver(RHS,tSpan,x0,tolVec(i));
    end
    T = [T; myT{i}];
end

T = unique(T);
display('Calculating reference solution using matlab')

if useAnalytical
    matlabTRefMy = T;
    matlabXRefMy = x(T')';
else
    try
        if stiff
            [matlabTRefMy,matlabXRefMy] = ode23s(RHS, T, x0, optionsReference);
        else
            [matlabTRefMy,matlabXRefMy] = ode45(RHS, T, x0, optionsReference);
        end
    catch ME
        disp(ME.message)
        return
    end
end

display('Calculating errors')
for i=1:length(tolVec)
    nbrOfPoints = length(myT{i});
    [values,iT] = intersect(T,myT{i});
    refX = matlabXRefMy(iT,:);
    err = 0;
    h = diff(myT{i});
    [~,nbrOfVars] = size(refX);
    switch lower(errorcalcmode)
        case 'relerr'
            errorVec = abs(refX-myX{i})./(abs(refX) + 1e-16);
        case 'abserr'
            errorVec = abs(refX-myX{i});
        otherwise
            ME = MException('accVsTol:BadErrorCalcMode',['This',...
            'errorCalcMode does not exist. You can choose relerror',...
            'or abserror']);
            throw(ME);
    end
    errorVec = sum(errorVec, 2)/nbrOfVars;
    mid = (errorVec(1:end-1,:) + errorVec(2:end,:))/2;
    err = sum(h.*mid)/(diff(tSpan) + 1e-16);
    acc(i) = err;
end
end
