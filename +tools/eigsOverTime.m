function [eigs_real,eigs_imag] = eigsOverTime(jac, x, t, varargin)
%Let the function f have the time-derivative RHS, which has the jacobian
%jac. This function calculates the eigenvalues of the jacobian over time.
%
%Parameters
%----------
%jac : function handle
%   The jacobian of which we want to calculate the eigenvalues over
%   time.
%x : matrix
%   The solution points of f at the time points in t.
%t : vector
%   The time points at which we want to calculate the eigenvalues of the
%   jacobian.
%varargin : options list
%   Options replacing default values on the form of strings
%   with corresponding value (see example above). The following options are
%   available:
%
%   'scale'
%       If you want to use this option, a vector of the step sizes at the
%       time points t shall be supplied. Instead of calculateing the
%       eigenvalues, this will calculate eigenvalue*stepSize such that we
%       can see if the product is inside the stability region of the
%       methode used.
%
%Returns
%-------
%eigs_real : vector
%   The real part of the eigenvalues of jac over time.
%eigs_imag : vector
%   The imaginary part of the eigenvalues of jac over time.

scale = false;
for j = 1:2:length(varargin)
    name = varargin{j};
    value = varargin{j+1};
    switch lower(name)
        case 'scale'
            scale = value;
    end
end
[m,n] = size(x);
if scale
    h = diff(t);
else
    h = ones(1,m-1);
end
lambdaVec = zeros(m,n);
for i=1:m - 1
    A = jac(t(i),x(i,:));
    lambda = eig(A);
    lambdaVec(i,:) = lambda*h(i);
end
eigs_real = real(lambdaVec);
eigs_imag = imag(lambdaVec);
end

