function [alphas,betas] = getAlphasAndBetas_pmmi(theta)
%Calculates the alphas and betas belonging to the method (fixed step size
%version) corresponding to the theta-vector. The theta-vector belongs to a
%method used in solver pmmi (k-step, implicit LMM of order k used to
%solve stiff equations).
%
%The corresponing fixed step size LMM is given by:
%   alpha_{k}*x_{n} + ... + alpha_{1}*x_{n-k+1} + alpha_{0}*x_{n-k} 
%   = h (beta_{k}*f_{n} + ... + beta_{1}*f_{n-k+1} + beta_{0}*f_{n-k})
%
%Parameters
%----------
%theta : vector
%   Row matrix (length = k) containing all thetas corresponding to the LMM.
%   The vector is ordered according to:
%   [theta_{1}, theta_{2}, ..., theta_{k-2}, theta_{k}].
%
%Returns
%-------
%alphas : vector
%   Row matrix (length = k+1) containing all alphas corresponding to the LMM.
%   The vector is ordered according to:
%   [alpha_{k}, alpha_{k-1}, ..., alpha_{1}, alpha_{0}].
%betas : vector
%   Row matrix (length = k+1) containing all betas corresponding to the LMM.
%   The vector is ordered according to:
%   [beta_{k}, beta_{k-1}, ..., beta_{1}, beta_{0}].

%Initiation
k = length(theta);
sinTheta = sin(theta);
cosTheta = cos(theta);
sinTheta(find(abs(sinTheta) < 1e-16)) = 0;
cosTheta(find(abs(cosTheta) < 1e-16)) = 0;

%symbols
t = sym('t');
tn = sym('tn');
h = sym('h');
H = sym('H');
x0 = sym('x0');
X = sym('x', [1 k+1]);
F = sym('f', [1 k+1]);
C = sym('c', [1 k+1]);

%Creating the polynomial P
vec=0:k;
P = @(C) sum(C.*((t - tn).^(vec)));

%Creating the derivative of the polynomial
Pdiff = @(C) diff(P(C), 't', 1);

P = @(C,h) subs(P(C),tn - t,h);
Pdiff = @(C,h) subs(Pdiff(C),tn - t,h);

%The implicit collocation condition
C(2) = F(1);

%Slack balance conditions
cond = @(C) (P(C,h) - X(2))*cosTheta(1) + h*(Pdiff(C,h) - F(2))*sinTheta(1);
C(1) = solve(cond(C) == 0, C(1));
C = subs(C, sym('c1'),C(1));
for j=2:k
    H = h*j;
    cond = @(C) (P(C,H) - X(j+1))*cosTheta(j) + h*(Pdiff(C,H) - F(j+1))*sinTheta(j);
    C(j+1) = solve(cond(C) == 0, C(j+1));
    C = subs(C, sym(sprintf('c%d', j+1)),C(j+1));
end

%
poly = simplify(P(C,0));
poly = symfun(poly,[X,F]);
X = zeros(size(X));
F = zeros(size(F));
vars = [X,F];
vars = num2cell(vars);
for i=1:k+1
    vars{i} = 1;
    Xcoeff(i) = -poly(vars{:});
    vars{i} = 0;
    vars{i+k+1} = 1;
    Fcoeff(i) = poly(vars{:})/h;
    vars{i+k+1} = 0;
end
Xcoeff(1) = Xcoeff(1) + 1;

%Create the sigma and rho polynomials
vec = 0:k;
vec = flip(vec);
alphas = double(Xcoeff);
%rho = @(w) sum(Xcoeff.*(w.^(vec)));
betas = double(Fcoeff);
%sigma = @(w) sum(Fcoeff.*(w.^(vec)));
end

