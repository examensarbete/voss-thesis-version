function [Cmin,Cmax,C] = getAllCombinations(alphas,betas,p,k,perc)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
C = zeros(1,2^(k+1)-1);
for i=0:2^(k+1)-1
    val = i;
    binI = de2bi(val);
    tempVec = ones(1,k+1)*perc(1);
    tempVec(find(binI == 1)) = perc(2)
    C(i+2) = func(alphas,betas,p,k,tempVec)
end

Cmin = min(C);
Cmax = max(C);

function C = func(alphas,betas,p,k,r)
s = 0;
for j=1:k
    rBar = sum(r(1:j));
    s = s + (alphas(j)*(rBar)^(p+1))/(p+1) - betas(j)*(rBar)^(p);
end
C = s/factorial(p);
end

end