%test
clf
names = fieldnames(statisticsStruct);
len = length(names);
legendNames = {};
for i = 1:len
    name = names(i);
    name = name{1};
    try
        error = statisticsStruct.([name]).errorFromAccVsTol;
        nbrOfSteps = statisticsStruct.([name]).numberOfSteps;
    catch ME
        name
        disp(ME.message)
        continue;
    end
    loglog(error,nbrOfSteps)
    hold on
    legendNames{end + 1,1} = name;
    
end
title(strcat(plotTitle,', stepsVsAcc'))
legend(legendNames)
xlabel('accuracy (error)'); % x-axis label
ylabel('nbr of steps'); % y-axis label
grid on
fig2plotly();