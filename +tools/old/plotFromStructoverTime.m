function legendNames = plotFromStructoverTime(struct,methodVec,filterVec,tolVec,yString)
%Plots from the struct returned by the solver. What is plotted is yString
%over time.
%
%Deeper explaination...
%
%Parameters
%----------
%struct : struct
%   The struct returned by one of the solvers.
%methodVec : cell array
%   Containes the names (strings) of all methods you want to plot for.
%filterVec : cell array
%   Containes the names (strings) of all filters you want to plot for.
%tolVec : vector
%   Row matrix containing all tolerances you want to plot for.
%yString : string
%   The name of what you want to plot.
%
%Returns
%-------
%legendNames : cell array
%   Containing the names of the legends in the plot.

clf

switch yString
    case 'myX'
        yLable = 'solution';
    case 'myH'
        yLable = 'stepSize';
    case 'myR'
        yLable = 'stepSizeRatio';
    case 'errorVecFromAccVsTol'
        yLable = 'errorVecFromAccVsTol';
    case 'errorEstimate'
        yLable = 'errorEstimate';
    otherwise
        ME = MException('Solver:BadYString','This yString can not be',...
            'chosen. Try another one!');
        throw(ME);
end

legendNames = {};
nbrMethods = length(methodVec);
nbrFilters = length(filterVec);
nbrTols = length(tolVec);
method = methodVec{1};
filter = filterVec{1};
name = [method,'_',filter];
perc = struct.([name]).perc;
myT = struct.([name]).myT{1};
tspan = [myT(1), myT(end)];

for i = 1:nbrMethods
    for j = 1:nbrFilters
        method = methodVec{i};
        filter = filterVec{j};
        name = [method,'_',filter];
        try
            t = struct.([name]).myT;
            succeededTols = struct.([name]).succeededTols;
            if (strcmp(yString,'myH') || strcmp(yString,'myR'))
                h = {};
                for k=1:length(t)
                    h{end+1} = diff(t{k});
                    temp = t{k};
                    t{k} = temp(1:end-1);
                end
                if (strcmp(yString,'myH'))
                    y = h;
                else
                    r = {};
                    for k=1:length(t)
                        T = t{k};
                        H = h{k};
                        r{end+1} = H(2:end)./H(1:end-1);
                        t{k} = T(1:end-1);
                    end
                    y = r;
                end
            else
                y = struct.([name]).([yString]);
            end
        catch ME
            name
            disp(ME.message)
            continue;
        end
        indices = [];
        for k=1:nbrTols
            for m=1:length(succeededTols)
                if(tolVec(k) > succeededTols(m))
                    indices(end + 1) = m;
                    break;
                end
            end
        end
        for k=1:length(indices)
            T = t{indices(k)};
            Y = y{indices(k)};
            semilogy(T,Y)
            hold on
            legendNames{end + 1,1} = [name,'_',num2str(succeededTols(indices(k)))];
        end
    end
end
% if (strcmp(yString,'myR'))
%     semilogy([tspan],[perc(1),perc(1)])
%     semilogy([tspan],[perc(2),perc(2)])
% end
xLable = 'Time'
titleName = strcat(struct.plotTitle,', ',' y = ',yLable,', ',' x = ',xLable)
title(titleName)
legend(legendNames)
xlabel(xLable); % x-axis label
ylabel(yLable); % y-axis label

%Note
dummy = struct.([name]);
try
    line1 = sprintf('MySolver: %s \n',dummy.MySolver);
    str = [line1];
    line2 = sprintf('Reference: %s \n',dummy.Reference);
    str = [str,line2];
    line3 = sprintf('errorMode: %s \n',dummy.errorMode);
    str = [str,line3];
    line4 = sprintf('norm: %s',dummy.norm);
    str = [str,line4];
    line5 = sprintf('unit: %s',dummy.unit);
    str = [str,line5];
    line6 = sprintf('perc: %s',dummy.perc);
    str = [str,line6];
catch
    display('OOPS!');
end
text(0.02, 1, str,'Units','normalized');
%

grid on
fig2plotly();
end

