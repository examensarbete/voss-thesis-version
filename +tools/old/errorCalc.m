function error = errorCalc(y1,y2,t)
%UNTITLED14 Summary of this function goes here
%   Detailed explanation goes here
    nbrOfPoints = length(myT{i});
    [values,iT] = intersect(T,myT{i});
    refX = matlabXRefMy(iT,:);
    error = 0;
    h = diff(myT{i});
    [~,nbrOfVars] = size(refX);
    switch lower(errorcalcmode)
        case 'relerr'
            errorVec = abs(refX-myX{i})./(abs(refX) + 1e-16);
        case 'abserr'
            errorVec = abs(refX-myX{i});
    end
    errorVec = sum(errorVec, 2)/nbrOfVars;
    mid = (errorVec(1:end-1,:) + errorVec(2:end,:))/2;
    try
        error = sum(h.*mid)/(diff(tSpan) + 1e-16);
    catch ME
        disp(ME.message)
    end
    acc(end + 1) = error;
    statisticsVec.errorFromAccVsTol(end+1) = error;

end

