function C = errorConstantVariableH(alphas,betas,order,k,r)
%UNTITLED Summary of this function goes here
%   betas: vector of length k+1
%   alphas:vector of length k
%   order: scalar, order of convergence of the method
%   k: scalar, k-step method
%   r:vector of length k
%
%Example: AB2:  y_n = y_{n-1} + h*(3/2*f_{n-1} - 1/2*f_{n-2})
%                           =>
%               alphas = [1,-1,0], betas = [0,3/2,-1/2]

vecR = zeros(k,1);
for i=1:k
    vecR(i) = sum(r(1:i));
end
sum1 = sum(vecR.^(order+1).*alphas);
sum2 = sum(vecR.^(order).*betas);
C = 1/factorial(order+1)*(sum1 - (order+1)*sum2);
end