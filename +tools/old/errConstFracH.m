function f = errConstFracH(alphas,betas,p,k,h)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%
% h: vector, at least length k+1 h = [h_{1}...,h_{n-k-3},...,h_{n-2},h_{n-1}]
% p: scalar, order
% k: scalar, nbrOfsteps in method
% alphas: vector, length k
% betas: vector, length k
%
%C_{prediction}/C_{current}

r = h(end-k:end)./h(end-k-1:end-1);
rPred = [r(1:end-2),r(end-1)+r(end)*r(end-1)];
rCurr = r(2:end);
f = tools.errorConstantVarH(alphas,betas,p,k,rPred)/tools.errorConstantVarH(alphas,betas,p,k,rCurr);
end

