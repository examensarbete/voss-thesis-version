function legendNames = plotFromStructHvsT(struct,methodVec,filterVec,tolVec)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

clf
legendNames = {};
nbrMethods = length(methodVec);
nbrFilters = length(filterVec);
nbrTols = length(tolVec);

for i = 1:nbrMethods
    for j = 1:nbrFilters
        method = methodVec{i};
        filter = filterVec{j};
        name = [method,'_',filter];
        try
            t = struct.([name]).myT;
            succeededTols = struct.([name]).succeededTols;
        catch ME
            name
            disp(ME.message)
            continue;
        end
        h = {};
        for k=1:length(t)
            h{end+1} = diff(t{k});
        end
        indeces = [];
        for k=1:nbrTols
            for m=1:length(succeededTols)
                if(tolVec(k) > succeededTols(m))
                    indeces(end + 1) = m;
                    break;
                end
            end
        end
        for k=1:length(indeces)
            T = t{indeces(k)};
            T = T(2:end);
            H = h{indeces(k)};
            plot(T,H)
            hold on
            legendNames{end + 1,1} = [name,'_',num2str(succeededTols(indeces(k)))];
        end
        
    end
end
yLable = 'Step size'
xLable = 'Time'
titleName = strcat(struct.plotTitle,', ',' y = ',yLable,', ',' x = ',xLable)
title(titleName)
legend(legendNames)
xlabel(xLable); % x-axis label
ylabel(yLable); % y-axis label

%Note
dummy = struct.([name]);
try
    line1 = sprintf('MySolver: %s \n',dummy.MySolver);
    str = [line1];
    line2 = sprintf('Reference: %s \n',dummy.Reference);
    str = [str,line2];
    line3 = sprintf('errorMode: %s \n',dummy.errorMode);
    str = [str,line3];
    line4 = sprintf('norm: %s',dummy.norm);
    str = [str,line4];
    line5 = sprintf('unit: %s',dummy.unit);
    str = [str,line5];
    line6 = sprintf('perc: %s',dummy.perc);
    str = [str,line6];
catch
    display('OOPS!');
end
text(0.02, 1, str,'Units','normalized');
%

grid on
fig2plotly();
end

