function [cMinPred,cMaxPred,cMinCurr,cMaxCurr] = errConstFracBounds(alphas,betas,p,k,perc)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%
% perc: vector, length 2, perc = [r_{min},r_{max}]
% p: scalar, order
% k: scalar, nbrOfsteps in method
% alphas: vector, length k
% betas: vector, length k
%
%C_{prediction}/C_{current}

rPredMin = [ones(1,k-1)*perc(1),perc(1)+perc(1)^2];
rPredMax = [ones(1,k-1)*perc(2),perc(2)+perc(2)^2];

rCurrMin = ones(1,k)*perc(1);
rCurrMax = ones(1,k)*perc(2);

cMinCurr = tools.errorConstantVarH(alphas,betas,p,k,rCurrMin);
cMaxCurr = tools.errorConstantVarH(alphas,betas,p,k,rCurrMax);
cMinPred = tools.errorConstantVarH(alphas,betas,p,k,rPredMin);
cMaxPred = tools.errorConstantVarH(alphas,betas,p,k,rPredMax);
end

