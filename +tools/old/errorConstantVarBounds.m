function [Cmin,Cmax] = errorConstantVarBounds(alphas,betas,p,k,perc)
%UNTITLED Summary of this function goes here
%   betas: vector of length k+1
%   alphas:vector of length k
%   order: scalar, order of convergence of the method
%   k: scalar, k-step method
%   perc: vector of length 2, [r_{min}, r_{max}]
%
%Example: AB2:  y_n = y_{n-1} + h*(3/2*f_{n-1} - 1/2*f_{n-2})
%                           =>
%               alphas = [1,-1,0], betas = [0,3/2,-1/2]

sum1 = sum([0:k].^(p+1).*alphas)/(p+1);
sum2 = sum([0:k].^(p).*betas);

Cmin = 1/factorial(p)*(perc(1)*sum1 - sum2)*perc(1);
Cmax = 1/factorial(p)*(perc(2)*sum1 - sum2)*perc(2);

%Normalize
sigma1 = sum(betas);
Cmin = Cmin/sigma1;
Cmax = Cmax/sigma1;
end

