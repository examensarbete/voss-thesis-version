function [Cmin,Cmax] = errorConstantVarBounds_new(alphas,betas,p,k,perc)
%UNTITLED Summary of this function goes here
%   betas: vector of length k+1
%   alphas:vector of length k
%   order: scalar, order of convergence of the method
%   k: scalar, k-step method
%   perc: vector of length 2, [r_{min}, r_{max}]
%
%Example: AB2:  y_n = y_{n-1} + h*(3/2*f_{n-1} - 1/2*f_{n-2})
%                           =>
%               alphas = [1,-1,0], betas = [0,3/2,-1/2]
PminVec = [];
PmaxVec = [];
for i=1:k
    TrMin = alphas(i)*(i*perc(1))/(p+1) - betas(i);
    TrMax = alphas(i)*(i*perc(2))/(p+1) - betas(i);
    if TrMin <= TrMax
        Tmin = TrMin;
        Tmax = TrMax;
    else
        Tmin = TrMax;
        Tmax = TrMin;
    end
    if Tmin > 0
        Pmin = (i*perc(1))^p*Tmin;
        Pmax = (i*perc(2))^p*Tmax;
    elseif Tmax < 0
        Pmin = (i*perc(2))^p*Tmin;
        Pmax = (i*perc(1))^p*Tmin;
    elseif Tmin <= 0 && Tmax >= 0
        Pmin = (i*perc(2))^p*Tmin;
        Pmax = (i*perc(2))^p*Tmax;
    else
        %Should never reach this
        display('Something is wrong');
    end
    PminVec = [PminVec,Pmin];
    PmaxVec = [PmaxVec,Pmax];  
end
Cmin = sum(PminVec)/factorial(p);
Cmax = sum(PmaxVec)/factorial(p);
end

