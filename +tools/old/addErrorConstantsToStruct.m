function struct = addErrorConstantsToStruct(struct,perc)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

clf
classes = fieldnames(struct);
    
for i = 1:length(classes)
    class = classes(i);
    class = class{1};
    methods = fieldnames(struct.([class]));
    figure();
    for j = 1:length(methods)
        method = methods(j);
        method = method{1};
        p = length(struct.([class]).([method]).alphas) - 1;
        [Cmin,Cmax] = tools.errorConstantVarBounds(struct.([class]).([method]).alphas,struct.([class]).([method]).betas,p,p,perc)
        struct.([class]).([method]).perc = perc;
        struct.([class]).([method]).Cmin = Cmin;
        struct.([class]).([method]).Cmax = Cmax;
        plot(j,Cmin,'xb')
        hold on
        plot(j,Cmax,'xr')
    end
end
end

