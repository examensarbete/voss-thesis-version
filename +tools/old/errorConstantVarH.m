function C = errorConstantVarH(alphas,betas,p,k,r)
%UNTITLED Summary of this function goes here
%   betas: vector of length k+1
%   alphas:vector of length k
%   p: scalar, order of convergence of the method
%   k: scalar, k-step method
%   r: vector of length k
%
%Example: AB2:  y_n = y_{n-1} + h*(3/2*f_{n-1} - 1/2*f_{n-2})
%                           =>
%               alphas = [1,-1,0], betas = [0,3/2,-1/2]

s = 0;
for i=1:k
    rBar = sum(r(1:i));
    s = s + (alphas(i)*(rBar)^(p+1))/(p+1) - betas(i)*(rBar)^(p);
end
C = s/factorial(p);