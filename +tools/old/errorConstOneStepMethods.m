function [Cmin, Cmax, Rmin, Rmax] = errorConstOneStepMethods(alpha, beta,p,perc)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
k = 1;
KKTmin = [];
KKTmax = [];
f = @(r) 1/factorial(p)*(alpha.*r.^(p+1)/(p+1) - beta.*r.^p);

%Case I
if alpha ~= 0 && beta ~= 0
    temp = beta*p/alpha
    if temp > perc(1) && temp < perc(2)
        KKTmin(end + 1) = temp;
        KKTmax(end + 1) = temp;
    end
end

%Case II
temp = beta*p/perc(2);
if alpha < temp
    KKTmin(end + 1) = perc(2);
elseif alpha > temp
    KKTmax(end + 1) = perc(2);
end

%Case III
temp = beta*p/perc(1);
if alpha > temp
    KKTmin(end + 1) = perc(1);
elseif alpha < temp
    KKTmax(end + 1) = perc(1);
end

%Find the min value
fmin = [];
for i = 1:length(KKTmin)
    fmin(end+1) = f(KKTmin(i));
end

Cmin = min(fmin);
i = find(fmin == Cmin);
Rmin = KKTmin(i);
Cmin = Cmin/sum(beta);

%Find the max value
fmax = [];
for i = 1:length(KKTmax)
    fmax(end+1) = f(KKTmax(i));
end

Cmax = max(fmax);
i = find(fmax == Cmax);
Rmax = KKTmax(i(1));
Cmax = Cmax/sum(beta);

rRange = perc(1):0.01:perc(2);
plot(rRange,f(rRange))
end

