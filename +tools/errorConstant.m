function C = errorConstant(alphas,betas,order,k)
%Calculates the normalized error constant C of LMM belonging to the alphas
%and betas.
%
%The alpha and beta vector should be formatted as following example shows:
%       AB2:  y_n = y_{n-1} + h*(3/2*f_{n-1} - 1/2*f_{n-2})
%                           =>
%               alphas = [1,-1,0], betas = [0,3/2,-1/2]
%
% Parameters
% ----------
% alphas : vector
%   Vecor containing the alphas of the LMM.
% betas : vector
%   Vecor containing the betas of the LMM.
% order : int
%   The order of the LMM.
% k : int
%   The number of steps used by the LMM (k-step method).
%
% Returns
% -------
% C : float
%   The value of the normalized error constant belonging to the LMM.
%

sum1 = sum([0:k].^(order+1).*flip(alphas));
sum2 = sum([0:k].^(order).*flip(betas));
C = 1/factorial(order+1)*(sum1 - (order+1)*sum2);

%Normalization
C = C/sum(betas);
end

