function [statisticsVec] = accVsTolVarOrd(f, tspan, x0, methodName, filterName, tolVec, mySolver, refSolverName, varargin)
% Calculates the accuracy of a method-filter pair for a given set of tolerances.
%
% This function may be used to facilitate testing. It calculates the 
% accuracy of a given method-filter pair for a set of tolerances. The
% reference solution used to determine the accuracy is calculated by using
% Matlab's own solvers with high accuracy or, if applied, the analytical
% solution.
%
% Parameters
% ----------
% f : function handle
%   The RHS function.
%
% tspan : vector
%   A vector with two elements, containing the initial and final time.
%
% x0 : (column) vector
%   The initial conditions.
%
% methodName : string
%   The name of the method.
%
% filterName : string
%   The name of the filter.
%
% tolVec : vector
%   The tolerances for which the accuracy is to be calculated.
%
% mySolver : function handle
%   The solver that you want to test, which takes the parameters
%   (f,tspan,x0,method,filter,tol,'norm',normFunc).
%
% refSolverName : string
%   The name of the reference solver.
%
% varargin : options list
%   Options replacing default values on the form of strings
%   with corresponding value. The following options are
%   available:
%
%   'refoptions'
%       The options that shall be given to the matlab reference solver. It
%       shall be on the form odeset('relTol',...,'absTol',...).
%
%   'errorcalcmode'
%       String that takes either 'relerr' or 'abserr' as a value depending
%       on if you want to calculate using a relative error or an absolute
%       error.
%
%   'analyticalfunction'
%       Function handle of the form f(x) that is the analytical solution
%       of RHS function.
%
%   'refsolvername'
%       String -- The name of the matlab solver that you want to use as a
%       reference.
%
%   'norm'
%      The norm used to calculate the error (used when going from a vector
%      with all error components to a scalar). Default is the 1-norm. To use
%      another norm supply a function handle.
%
% Returns
% -------
% statisticsVec : struct
%   Struct containing the statistics from all solutions.

% --------------------------------------------------------------------------
% Initialization of variables and starting paramaters
% --------------------------------------------------------------------------

%Default values on the extra arguments
optionsRefSolver = odeset('relTol',1e-13,'absTol',1e-16);
errorcalcmode = 'abserr';
normFunc = @(x) norm(x,1);

%Check for user arguments overriding the default values
for j = 1:2:length(varargin)
    name = varargin{j};
    value = varargin{j+1};
    switch lower(name)
        case 'refoptions'
            optionsRefSolver = value;
        case 'errorcalcmode'
            errorcalcmode = value;
        case 'analyticalfunction'
            analyticalFunc = value;
        case 'normfunc'
            normFunc = value;
        case 'initialsteps'
            h0 = value;
        otherwise
            msgID = 'accVsTol:BadOption';
            msg = sprintf('The option %s does not exist.',name);
            ME = MException(msgID,msg);
            throw(ME);
    end
end

%Initialization
refSolver = str2func(refSolverName);
runtimeErrorsMySolver = {};
runtimeErrorsRefSolver = {};
nbrOfArgsOutSolver = nargout(mySolver);
i = 0;
T = [];
myT = {};
myX = {};
myO = {};
statisticsVec = struct('MySolver','','RefSolver',refSolverName,'Method',...
    methodName,'Filter',filterName,'errorMode',errorcalcmode,'cpuTime',...
    [],'rejectedSteps',[],'numberOfSteps',[],'nbrStepsRTooBig', [],...
    'allTols',tolVec,'succeededTols',[],'errorFromAccVsTol',[],...
    'meanOrder',[])

% --------------------------------------------------------------------------
% Calculating solutions using mySolver
% --------------------------------------------------------------------------
display('Calculating solutions');
tic
num = -1;
num = tools.progressBar(0,'percentage',true,'character','#','numChar',num);
for tol = tolVec
    i = i + 1;
    % Solution of the solver we are analyzing
   % try
        if (nbrOfArgsOutSolver == 2)
            %Matlab solvers only outputs [t, x]
            options = odeset('relTol',tol,'absTol',tol*1e-3);
            [my_t,my_x] = mySolver(f,tspan,x0,options);
        else
        %Our solvers output [t, x, statistics]
            %if h0 exists, we should set the initial step size manually
            if exist('h0','var')
                [my_t,my_x,statistics] = mySolver(f,tspan,x0,methodName,...
                    filterName,tol,strcmp('relerr',errorcalcmode),...
                    normFunc,h0(i));
            else
                [my_t,my_x,statistics] = mySolver(f,tspan,x0,methodName,...
                    filterName,tol,strcmp('relerr',errorcalcmode),normFunc);
            end
        end

    %If we for some reason get an empty x and t vector, jump to next tol.
    if (length(my_t) == 0)
        continue;
    else
        myT{end + 1} = my_t;
        myX{end + 1} = my_x;
        myO{end + 1} = statistics.orders;
        T = [T; my_t];
        statisticsVec.succeededTols(end+1) = tol;
        statisticsVec.rejectedSteps(end+1) = statistics.rejectedSteps;
        statisticsVec.numberOfSteps(end+1) = statistics.numberOfSteps;
        statisticsVec.nbrStepsRTooBig(end+1) = statistics.nbrStepsRTooBig;
        statisticsVec.cpuTime(end+1) = statistics.cpuTime;
        %statisticsVec.meanOrder(end+1) = mean(statistics.orders(find(...
        %    statistics.orders > 0)));
    end
    num = tools.progressBar(i/length(tolVec),'percentage',true,...
           'character','#','numChar',num);
end
fprintf('\n');
T = unique(T);

% --------------------------------------------------------------------------
% Calculating reference solution using refSolver
% --------------------------------------------------------------------------
display('Calculating reference solution')
if(exist('analyticalFunc','var'))
    refSolverX = analyticalFunc(T);
else
    try
        [refSolverT,refSolverX] = refSolver(f, T, x0, optionsRefSolver);
    catch ME
        %If the reference solver gets an error, display error message, set
        %statisticsVec to 0 and return.
        runtimeErrorsRefSolver = ME.message;
        disp(ME.message)
        fprintf('\n');
        statisticsVec = 0;
        return
    end
end

% --------------------------------------------------------------------------
% Calculating errors
% --------------------------------------------------------------------------
display('Calculating errors')
for i=1:length(statisticsVec.succeededTols)
    [nbrOfPoints,nbrOfVars] = size(myX{i});
    [values,iT] = intersect(T,myT{i});
    if (nbrOfPoints ~= length(iT))
        %In rare cases it happens that nbrOfPoints ~= length(iT) (probably
        %due to rounding errors). In these cases we can not use this
        %tolerance. Instead we put the error to NaN and jump to next tol.
        statisticsVec.errorFromAccVsTol(end+1) = NaN;
        continue;
    end
    error = 0;
    refX = refSolverX(iT,:);    %The reference sol
    testX = myX{i};             %The test sol
    h = diff(myT{i});
    errorVec = zeros(1,nbrOfPoints); %Will contain the local errors (or estimates of the local errors)
    switch lower(errorcalcmode)
        case 'relerr'
            for j=1:nbrOfPoints
                errorVec(j) = normFunc(refX(j,:)-testX(j,:))/(normFunc(refX(j,:)) + 1e-16);
            end
        case 'abserr'
            for j=1:nbrOfPoints
                errorVec(j) = normFunc(refX(j,:)-testX(j,:));
            end
        otherwise
            msgID = 'accVsTol:BadOption';
            msg = 'This option does not exist.';
            ME = MException(msgID,msg);
            throw(ME);
    end
    mid = (errorVec(1:end-1) + errorVec(2:end))'/2;
    error = sum(h.*mid)/(diff(tspan) + 1e-16); %Weighted mean of the local errors
    statisticsVec.errorFromAccVsTol(end+1) = error;

    %Integrate the order
    O = myO{i};
    O = O(find(O > 0));
    Tord = myT{i};
    Tord = Tord(end-length(O):end);
    hord = diff(Tord);
    meanOrder=(O*hord)/(diff(tspan) + 1e-16);
    if isempty(meanOrder)
        meanOrder=NaN;
    end
    statisticsVec.meanOrder(end+1) = meanOrder;
end

% --------------------------------------------------------------------------
% Add the last stuff to the struct
% --------------------------------------------------------------------------
totalCpuTime = toc;
statisticsVec.MySolver = statistics.solver;
statisticsVec.runtimeErrorsMySolver = runtimeErrorsMySolver;
statisticsVec.runtimeErrorsRefSolver = runtimeErrorsRefSolver;
statisticsVec.totalCpuTime = totalCpuTime;
statisticsVec.norm = normFunc;
statisticsVec.errorcalcmode = errorcalcmode;
statisticsVec.myX = myX;
statisticsVec.myT = myT;
end
