function legendName = plotFromStructoverTimeOne(struct,method,filter,tol,yString,plotly)
%Plots from the struct returned by 'codebase/+tools/accVsTol_loop.m' (in
%this package). The quantity that plotted is 'yString over time' for one
%method, one filter and one tolerance.
%
%Parameters
%----------
%struct : struct
%   The struct returned by 'codebase/+tools/accVsTol_loop.m'.
%method : string
%   The name of the method you want to plot for. If this method (in
%   combination with the supplied filter) does not exist in the supplied
%   struct, a matlab error will be thrown.
%filter : string
%   The name of the filter you want to plot for. If this filter (in
%   combination with the supplied method) does not exist in the supplied
%   struct, a matlab error will be thrown.
%tol : float
%   The value of the tolerance you want to plot for. If this (exact)
%   tolerance does not exist, the tolerance closest to it is chosen.
%yString : string
%   The name of what you want to plot. Availible things to plot are:
%       -'myX' (The solution points)
%       -'myH' (The step sizes)
%       -'myR' (The step size ratios)
%       -'errorVecFromAccVsTol' (The accuracy from the accVsTol-function)
%       -'errorEstimate' (The error estimates used inside the solver to
%                   controll the step size)
%   If none of these things are chosen, an error is thrown.
%
%plotly : boolean
%   If true, the plot is sent to plotly (and plotted in matlab),
%   otherwise it is just plotted in matlab. For it to work with plotly yo
%   have to set up an account at plot.ly and follow the instructions on how
%   to sync plots with plotly using matlab.
%
%Returns
%-------
%legendName : string
%   The name of the legend in the plot.
%
%Raises
%------
%'PlotFunction:BadYString'
%   If the value of yString is now allowed to plot, this error will be
%   thrown.

clf

switch yString
    case 'myX'
        yLable = 'solution';
    case 'myH'
        yLable = 'stepSize';
    case 'myR'
        yLable = 'stepSizeRatio';
    case 'errorVecFromAccVsTol'
        yLable = 'errorVecFromAccVsTol';
    case 'errorEstimate'
        yLable = 'errorEstimate';
    otherwise
        ME = MException('Solver:BadYString','This yString can not be',...
            'chosen. Try another one!');
        throw(ME);
end

name = [method,'_',filter];
myT = struct.([name]).myT{1};
tspan = [myT(1), myT(end)];

Extract the values
try
    t = struct.([name]).myT;
    succeededTols = struct.([name]).succeededTols;
    if (strcmp(yString,'myH') || strcmp(yString,'myR'))
        h = {};
        for k=1:length(t)
            h{end+1} = diff(t{k});
            temp = t{k};
            t{k} = temp(1:end-1);
        end
        if (strcmp(yString,'myH'))
            y = h;
        else
            r = {};
            for k=1:length(t)
                T = t{k};
                H = h{k};
                r{end+1} = H(2:end)./H(1:end-1);
                t{k} = T(1:end-1);
            end
            y = r;
        end
    else
        y = struct.([name]).([yString]);
    end
catch ME
    name
    disp(ME.message)
end

%Find the tolerance closest to tol in succeededTols
for m=1:length(succeededTols)
    if(tol > succeededTols(m))
        tol2 = abs(tol-succeededTols(m));
        tol1 = abs(tol-succeededTols(m-1));
        if (tol1 < tol2)
            index = m-1;
        else
            index = m;
        end
        break;
    end
end

%Plot
T = t{m};
Y = y{m};
semilogy(T,Y)
hold on
legendName = [name,'_',num2str(succeededTols(m))];

%Add xlabel, ylabel, title, legend and note to the plot.
xLable = 'Time'
titleName = strcat(struct.plotTitle,', ',' y = ',yLable,', ',' x = ',xLable)
title(titleName)
legend(legendNames)
xlabel(xLable); % x-axis label
ylabel(yLable); % y-axis label

%Note
dummy = struct.([name]);
try
    line1 = sprintf('MySolver: %s \n',dummy.MySolver);
    str = [line1];
    line2 = sprintf('Reference: %s \n',dummy.Reference);
    str = [str,line2];
    line3 = sprintf('errorMode: %s \n',dummy.errorMode);
    str = [str,line3];
    line4 = sprintf('norm: %s',dummy.norm);
    str = [str,line4];
    line5 = sprintf('unit: %s',dummy.unit);
    str = [str,line5];
    line6 = sprintf('perc: %s',dummy.perc);
    str = [str,line6];
catch ME
    disp(ME.message)
end
text(0.02, 1, str,'Units','normalized');

%Send to plotly or not
grid on
if (plotly == true)
    fig2plotly();
end
end

