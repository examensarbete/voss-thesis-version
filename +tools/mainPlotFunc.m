function [statistics,myT,myX] = mainPlotFunc(f,tspan,x0,tol,mySolver,matlabSolverName,reference,varargin)
%The main ploting function. It is used to plot various things, for example
%solution, step size sequence, etc., from the solutions given by the fixed
%order solvers.
%
%In codebase/scripts/explicit, codebase/scripts/implicitK and
%codebase/scripts/implicitKPlus1 you find scripts called
%solverName_plot_problemName.m. These script are example scripts created to
%get an idea of how to use the solver and plot interesting quatities
%calculated from the solutions.
%
%Parameters
%----------
%f : function handle
%   Function handle to the right hand side (RHS) function for the system of
%   differential equations. For a scalar t and a vector x it must return a
%   column vector corresponding to f(t,x).
%
%tspan : vector (lenght 2, row matrix)
%   tSpan = [$T_{initial}$, $T_{final}$], where $T_{initial}$ is the initial
%   integration time and $T_{final}$ is the final integration time.
%
%x0 : vector (column matrix)
%   The initial condition x(0) = x0, given as a column matrix.
%
%tol : float
%   The tolerance at which we want to calculate the solution of f, x0, span.
%
%mySolver : function handle
%   Row matrix
%
%matlabSolverName : string
%   The name of the matlab solver you want to compare your solution with. 
%
%reference : string or function handle
%   This will be used as reference.
%       if string:
%           The name of the matlab solver you want to use as reference solver.
%       if function handle:
%           The analytical solution to the RHS-function f.
%
%varargin : options list
%   Options replacing default values on the form of strings
%   with corresponding value (see example above). The following options are
%   available:
%
%   'refoptions'
%       If you are using a matlab solver as reference, you are able to set
%       the options for it using odeset(...).
%   'matlaboptions'
%       You are able to set the options for the matlab solver you are
%       comparing with it using odeset(...).
%   'relerr'
%       Boolean
%   'jac'
%       Function handle
%   'normfunc'
%       Function handle
%   'plotlist'
%       A vector containing booleans telling the function which quantities
%       to plot. If, for example, plot_mySol == true, then the function
%       plots the solution calculated by mySolver.
%           plotList = [plot_mySol, plot_refSol, plot_stiffness,
%           plot_stepSize, plot_errorReal, plot_errorController,
%           plot_stepSizeRatios, plot_order];
%       Explaination of the quatities:
%           plot_mySol -- the solution of mySolver
%           plot_refSol --the reference solution
%           plot_stiffness -- the stiffness of the solution to f, x0 in tspan.
%           plot_stepSize -- the step size sequences of mySolver and the
%               matlab solver you chose to compare with.
%           plot_errorReal -- the global error (of mySolver and the matlab
%               solver) calculated using the reference.
%           plot_errorController -- the error estimates given to the
%               controller inside mySolver.
%           plot_stepSizeRatios -- the step size ratios of mySolver.
%           plot_order -- the order of mySolver over time (this option is only
%               availible for the variable order solvers).
%           
%
%Returns
%-------
%statistics: struct
%   Matlab struct containing information about the run.
%myT : vector
%   Column matrix containing the time values corresponding to the solution
%   points in myX.
%myX : matrix
%   Matrix containing the solution points (solved by mySolver) to the
%   problem given.
%

warning('off','all')

% --------------------------------------------------------------------------
% Initialization part
% --------------------------------------------------------------------------

%Initialization
matlabSolver = str2func(matlabSolverName);
problemName = func2str(f);

%Default values on the extra arguments
matlabOptions = odeset('relTol',tol,'absTol',tol*1e-3);
relerr = 0;
jac = @(t,y) solverFunctions.getJac(f,t,y);
normFunc = @(x) norm(x,Inf);
plotList = ones(1,8); %As default we plot everything

%Check for user arguments overriding the default values
for j = 1:2:length(varargin)
    name = varargin{j};
    value = varargin{j+1};
    switch lower(name)
        case 'refoptions'
            optionsReference = value;
        case 'matlaboptions'
            matlabOptions = odeset(matlabOptions,value);
        case 'relerr'
            relerr = value;
        case 'jac'
            jac = value;
        case 'normfunc'
            normFunc = value;
        case 'plotlist'
            plotList = value;
    end
end

%Always turn on stats
matlabOptions = odeset(matlabOptions,'Stats','on');

%Check if reference is a matlab solver name or an analytical solution
%function
if(isa(reference,'char'))
    refSolver = str2func(reference);
    if ~exist('optionsReference')
        optionsReference = odeset('relTol',1e-13,'absTol',1e-16);
    end
else
    analyticalFunc = reference;
end

% --------------------------------------------------------------------------
% Solving the problem
% --------------------------------------------------------------------------

%My solution
[myT,myX,statistics] = mySolver(f,tspan,x0,tol,relerr,normFunc);
display('Done with solving using mySolver')

%Matlab's solution
%the use of evalc is just to suppress the output
[foo,matlabT,matlabX,matlabStats] = ...
    evalc('matlabSolver(f,tspan,x0,matlabOptions);');
display('Done with solving using matlab, first time')

%Reference solutions
if(exist('analyticalFunc'))
    [xRefMy] = analyticalFunc(myT);
    tRefMy = myT;
    [xRefMatlab] = analyticalFunc(matlabT);
    tRefMatlab = matlabT;
else
    [tRefMy,xRefMy] = refSolver(f,myT,x0,optionsReference);
    if length(tRefMy) ~= length(myT)
        ME = MException('Solver:BadSolution','myT is not as long as tRefMy');
        throw(ME);
    end
    display('Done with solving using matlab, second time')
    [tRefMatlab,xRefMatlab] = refSolver(f,matlabT,x0,optionsReference);
    if length(tRefMatlab) ~= length(matlabT)
        ME = MException('Solver:BadSolution','matlabT is not as long as tRefMatlab');
        throw(ME);
    end
    display('Done with solving using matlab, third time')
end

% --------------------------------------------------------------------------
% Calculations of results
% --------------------------------------------------------------------------
[~,nbrOfVars] = size(myX);

%For step size plot
matlabH = diff(matlabT);
myH = diff(myT);

%For step size ratio plot
matlabR = matlabH(2:end)./matlabH(1:end-1);
myR = myH(2:end)./myH(1:end-1);

%For error plot -- my solution
errorMy = [];
if relerr == 0
    for i=1:length(myT)
        errorMy(i,1) = normFunc(xRefMy(i,:) - myX(i,:));
    end
else
    for i=1:length(myT)
        errorMy(i,1) = normFunc(xRefMy(i,:) - myX(i,:))/(normFunc(xRefMy(i,:)) + 1e-16);
    end
end
midMy = (errorMy(1:end-1) + errorMy(2:end))/2;
try
    errorScalarMy = sum(myH.*midMy)/(diff(tspan) + 1e-16);
catch ME
    rethrow(ME)
end

%For error plot -- ref solution
errorMatlab = [];
if relerr == 0
    for i=1:length(matlabT)
        errorMatlab(i,1) = normFunc(xRefMatlab(i,:) - matlabX(i,:));
    end
else
    for i=1:length(matlabT)
        errorMatlab(i,1) = normFunc(xRefMatlab(i,:) - matlabX(i,:))/(normFunc(xRefMatlab(i,:)) + 1e-16);
    end
end
midMatlab = (errorMatlab(1:end-1) + errorMatlab(2:end))/2;
try
    errorScalarMatlab = sum(matlabH.*midMatlab)/(diff(tspan) + 1e-16);
catch ME
    disp(ME.message)
end

%For stiffness plot
sVec = tools.calsStiffnesOverTime(jac,tRefMy,xRefMy);
sMin = min(sVec);
sNormMin = sMin*diff(tspan);

%Add stuff to statistics
statistics.StiffnessValue = sMin;
statistics.NormStiffnessValue = sNormMin;
statistics.my_errorScalar = errorScalarMy;
statistics.matlab_solver = matlabSolverName;
statistics.matlab_nbrOfSteps = length(matlabT);
statistics.matlab_errorScalar = errorScalarMatlab;
statistics.matlab_rejectedSteps = matlabStats(2);
statistics.realErrors = errorMy;

% --------------------------------------------------------------------------
% Plotting part
% --------------------------------------------------------------------------

figNbr = 1;
%Plotting my solution
if(plotList(1))
    figure(figNbr)
    plot(myT,myX)
    hold on
    if (statistics.rejectedSteps > 0 && statistics.nbrStepsRTooBig > 0)
        p1 = plot(statistics.rejectedStepsData.t,statistics.rejectedStepsData.x,'rx');
        p2 = plot(statistics.nbrStepsRTooBigData.t,statistics.nbrStepsRTooBigData.x,'bo');
        legend([p1(1),p2(1)],'too small r (rejected step)','too big r')
    elseif (statistics.rejectedSteps == 0 && statistics.nbrStepsRTooBig > 0)
        p2 = plot(statistics.nbrStepsRTooBigData.t,statistics.nbrStepsRTooBigData.x,'bo');
        legend([p2(1)],{'too big r'})
    elseif (statistics.rejectedSteps > 0 && statistics.nbrStepsRTooBig == 0)
        p1 = plot(statistics.rejectedStepsData.t,statistics.rejectedStepsData.x,'rx');
        legend([p1(1)],{'too small r (rejected step)'})
    end
    
    title('solution mySolver')
    grid on
    hold off
    xlim(tspan)
    figNbr = figNbr + 1;
end
%dlmwrite('lotka-solution.txt',[myT,myX])

%Plotting ref's solution
if(plotList(2))
    figure(figNbr)
    plot(tRefMatlab,xRefMatlab)
    title('reference solution')
    grid on
    xlim(tspan)
    figNbr = figNbr + 1;
end

%Plotting stiffness using refSolution
if(plotList(3))
    figure(figNbr)
    plot(tRefMy,sVec)
    title('stiffness over time')
    grid on
    xlim(tspan)
    figNbr = figNbr + 1;
end
%dlmwrite('lotka-stiffness.txt',[myT,sVec'])


%Plotting the step sizes
if(plotList(4))
    figure(figNbr)
    semilogy(myT(1:end-1),myH,'b',matlabT(1:end-1),matlabH,'r')
    title('step-sizes')
    legend('mySolver','matlabSolver')
    xlim(tspan)
    grid on
    figNbr = figNbr + 1;
end
%dlmwrite('lotka-step-size.txt',[myT(1:end-1),myH])

%Plotting the real error
if(plotList(5))
    figure(figNbr)
    semilogy(myT(1:end),errorMy(1:end),'b',matlabT(1:end),...
        errorMatlab(1:end),'r')
    hold on
    semilogy(tspan,[tol,tol],'black')
    if(relerr)
        title('Relative error -- Real')
    else
        title('Absolute error -- Real')
    end
    legend('mySolver','matlabSolver')
    xlim(tspan)
    grid on
    hold off
    figNbr = figNbr + 1;
end
%dlmwrite('lotka-real-error.txt',[myT(1:end),errorMy(1:end)])

%Plotting the error estimate (the error the controller is given)
if(plotList(6))
    figure(figNbr)
    semilogy(myT,statistics.errorEstimate,'b')
    hold on
    if(relerr)
        title('Relative error -- Controller')
    else
        title('Absolute error -- Controller')
    end
    legend('mySolver')
    xlim(tspan)
    grid on
    hold off
    figNbr = figNbr + 1;
end
%dlmwrite('lotka-control-error.txt',[myT,statistics.errorEstimate'])


%Plotting the step size ratios
if(plotList(7))
    figure(figNbr)
    perc = statistics.perc;
    p1 = plot(myT(2:end-1),myR,'b');
    hold on
    p2 = plot(tspan,[perc(1), perc(1)],'black',tspan,[perc(2), perc(2)],'black');
    title('step size ratios')
    %legend([p1(1),p1(2),p2(1)],'mySolver','matlabSolver','perc')
    xlim(tspan)
    ylim([perc(1)*0.9,perc(2)*1.1])
    grid on
    hold off
    figNbr = figNbr + 1;
end
%dlmwrite('lotka-step-size-ratio.txt',[myT(2:end-1),myR])

if(isfield(statistics,'orders') && plotList(8))
    plot(myT,statistics.orders)
    title('order used over time')
    grid on
    figNbr = figNbr + 1;
end

end

