function [statisticsStruct] = accVsTol_loop(f,tspan,x0,nameMethodVec,nameFilterVec,minTol,maxTol,nbrOfTols,mySolver,refSolverName,varargin)
% Calculates the accuracy of a method-filter pair for a given set of tolerances.
%
% This function may be used to facilitate testing. It calculates the 
% accuracy of a given method-filter pair for a set of tolerances. The
% reference solution used to determine the accuracy is calculated by using
% Matlab's own solvers with high accuracy or, if applied, the analytical
% solution.
%
% Parameters
% ----------
% f : function handle
%   The RHS function
%
% tspan : vector
%   A vector with two elements, containing the initial and final time.
%
% x0 : (column) vector
%   The initial conditions.
%
% nameMethodVec : cell array of strings
%   Cell array containing the names of the methods.
%
% nameFilterVec : cell array of strings
%   Cell array containing the names of the filters.
%
% minTol : scalar
%   The value k where 10^{-k} is tha greatest tolerance.
%
% maxTol : scalar
%   The value k where 10^{-k} is tha smallest tolerance.
%
% nbrOfTols : scalar
%   The number of tolerances for which we want a solution.
%
% mySolver : function handle
%   The solver that you want to test, which takes the parameters
%   (f,tspan,x0,method,filter,tol,'norm',normFunc).
%
% refSolverName : string
%   The name of the matlab solver you want to use as reference.
%
% varargin : options list
%   Options replacing default values on the form of strings
%   with corresponding value. The following options are
%   available:
%
%   'refoptions'
%       The options that shall be given to the matlab reference solver. It
%       shall be on the form odeset('relTol',...,'absTol',...).
%
%   'errorcalcmode'
%       String that takes either 'relerr' or 'abserr' as a value depending
%       on if you want to calculate using a relative error or an absolute
%       error.
%
%   'analyticalfunction'
%       Function handle of the form f(x) that is the analytical solution
%       of RHS function.
%
%   'norm'
%      The norm used to calculate the error (used when going from a vector
%      with all error components to a scalar). Default is the 1-norm. To use
%      another norm supply a function handle.
%
% Returns
% -------
% statisticsStruct : struct
%   Struct containing structs with all the statistics from all solutions.

%Default values on the extra arguments
optionsRefSolver = odeset('relTol',1e-13,'absTol',1e-16);
errorcalcmode = 'abserr';
normFunc = @(x) norm(x,Inf);

%Check for user arguments overriding the default values
for j = 1:2:length(varargin)
    name = varargin{j};
    value = varargin{j+1};
    switch lower(name)
        case 'refoptions'
            optionsRefSolver = value;
        case 'errorcalcmode'
            errorcalcmode = value;
        case 'analyticalfunction'
            analyticalFunc = value;
        case 'normfunc'
            normFunc = value;
        otherwise
            msgID = 'accVsTol:BadOption';
            msg = 'This option does not exist.';
            ME = MException(msgID,msg);
            throw(ME);
    end
end

nbrOfMethods = length(nameMethodVec);
nbrOfFilters = length(nameFilterVec);

%Create tolerance vector
k = linspace(minTol,maxTol,nbrOfTols);
tolVec = zeros(1,nbrOfTols);
for i=1:nbrOfTols
    tolVec(i) = 10^(-k(i));
end

statisticsStruct = {};
for filterI=1:nbrOfFilters
    for methodI=1:nbrOfMethods
        %Method stuff
        methodName = nameMethodVec{methodI}
        
        %Filter stuff
        filterName = nameFilterVec{filterI}

        %Solve
        %If we have the analytical solution
        if (exist('analyticalFunc','var'))
            [statistics] = tools.accVsTol(f, tspan, x0, methodName, filterName, tolVec, mySolver, refSolverName,'errorcalcmode',errorcalcmode,'analyticalfunction',analyticalFunc,'normfunc',normFunc);
        %If we don't have the analytical solution we use a matlab solver as
        %reference
        else
            [statistics] = tools.accVsTol(f, tspan, x0, methodName, filterName, tolVec, mySolver, refSolverName, 'refoptions', optionsRefSolver,'errorcalcmode',errorcalcmode,'normfunc',normFunc);
        end
        statisticsStruct.([methodName,'_',filterName]) = statistics;
    end
end
end