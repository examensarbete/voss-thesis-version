function [r,a] = getEigenVals(parvec)
%Calculates the eigenvalues of a filter corresponding to parvec.
%
%Parameters
%----------
%parvec : vector
%   Row matrix containing all filter parameters, ordered acoording to:
%   [gamma_1, gamma_2, ..., gamma_k, -alpha_2, -alpha_3, ..., -alpha_k]
%   where gamma_i = beta_i*k and k is the order of the method used.
%
%Returns
%-------
%r : vector
%   Row matrix containing the roots of the filter.
%a : vector
%   Row matrix containing the absolute values of the roots of the filter.

pD = floor(length(parvec)/2) + 1;

switch pD
    case 1
        c1 = 1; 
        c2 = parvec(1) - 1; % = k*beta1 - 1
        pCooef = [c1 c2];
    case 2
        c1 = 1;
        c2 = -parvec(3) + parvec(1) - 1; % = alpha2 + k*beta1 - 1
        c3 = parvec(2) + parvec(3); % = k*beta2 - alpha2
        pCooef = [c1 c2 c3];
    case 3
        c1 = 1;
        c2 = -parvec(4) + parvec(1) - 1; % = alpha2 + k*beta1 - 1
        c3 = -parvec(5) + parvec(4) + parvec(2); % = alpha3 - alpha2 + k*beta2
        c4 = parvec(3) + parvec(5); % = k*beta3 - alpha3
        pCooef = [c1 c2 c3 c4];
end
r = roots(pCooef);
a = abs(r);
end

