function [C,x,y,z1,z2] = plotStabReg(alphas,betas,order,k)
%Plots the stability region of a the k-step LMM defined by 'alphas'
%and 'betas'.
%
%The LMM is given by:
%   alpha_{k}*x_{n} + ... + alpha_{1}*x_{n-k+1} + alpha_{0}*x_{n-k} 
%   = h (beta_{k}*f_{n} + ... + beta_{1}*f_{n-k+1} + beta_{0}*f_{n-k})
%
%Parameters
%----------
%alphas : vector
%   Row matrix (length = k+1) containing all alphas corresponding to the LMM.
%   The vector is ordered according to:
%   [alpha_{k}, alpha_{k-1}, ..., alpha_{1}, alpha_{0}].
%betas : vector
%   Row matrix (length = k+1) containing all betas corresponding to the LMM.
%   The vector is ordered according to:
%   [beta_{k}, beta_{k-1}, ..., beta_{1}, beta_{0}].
%order : int
%   The order of the LMM.
%k : int
%   The number of steps used by the LMM (k-step method).
%
%Returns
%-------
%C : float
%   The normalized error constant belonging to the LMM
%x : vector
%   The real part of the lotus curve values z.
%y : vector
%   The imaginary part of the lotus curve values z.
%z1 : function handle
%   z1 = @(w) (rho(w)/(sigma(w))).
%z2 : function handle
%   z2 = @(phi) (rho(exp(1i*phi))/(sigma(exp(1i*phi))));

%Calculate the characteristic polynomials 'rho' and 'sigma' belonging
%to the LMM.
vec = 0:k;
vec = flip(vec);
rho = @(w) sum(alphas.*(w.^(vec)));
sigma = @(w) sum(betas.*(w.^(vec)));

%Calculate the normalized error constant C = C_{p+1}/sigma(1)
C = tools.errorConstant(alphas,betas,order,k);

%Plot the lotus curve of the method
z1 = @(w) (rho(w)/(sigma(w)));
z2 = @(phi) z1(exp(1i*phi));
phi = linspace(0,2*pi,1000);
vals = zeros(size(phi));
for j=1:length(phi)
    vals(j) = z2(phi(j));
end
x = real(vals);
y = imag(vals);
plot(x,y)
grid on
end

