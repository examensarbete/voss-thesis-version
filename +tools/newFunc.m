function yd = newFunc(A, f, fd, nonLinPart, t, y)
%DOCUMENTATION NOT DONE!
%
%RHS function of a 'home made' test problem.
%
%Parameters
%----------
%A : matrix (size nxn)
%   Matrix belonging to the problem. The stiffness of the problem can be
%   changed by changing the eigenvalues of the matrix A.
%f : function handle
%   The analytical solution to this RHS function.
%fd : function handle
%   Row
%nonLinPart : matrix
%   The non-linear part of this RHS-function.
%t : float
%   The time point at which this RHS-function shall be calculated.
%y : vector
%   The solution at time t of the analytical function f.
%
%Returns
%-------
%yd : vector
%   The solution at t and y of this RHS-function.

%If y is not a column vector we transpose it
[row,col] = size(y);
trans = 0;
if(col > 1)
    y = y';
    trans = 1;
end

phi = @(y) A*y + nonLinPart(y);
yd = phi(y-f(t)') + fd(t)';

%If we transposed in the beginning, we transpose back.
if(trans)
    yd = yd';
end
end

