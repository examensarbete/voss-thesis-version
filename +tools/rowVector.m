function boolean = rowVector(vector)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[nbrOfRows,nbrOfCols] = size(vector);

if (nbrOfRows == nbrOfCols)
    boolean = true;
elseif (nbrOfRows < nbrOfCols)
    boolean = true;
else
    boolean = false;
end
end

