function legendNames = plotFromStruct(struct,xString,yString, plotly, matlabPlotFunction)
%Plots from the struct returned by 'codebase/+tools/accVsTol_loop.m' (in
%this package). The quantity that is plotted is 'yString' as a function of
%'xString' for all method-filter combinations in the struct.
%
%The quantities plotted are such that they have one value (float) for
%every tolerance (i.e. not a whole vector).
%
%Parameters
%----------
%struct : struct
%   The struct returned by 'codebase/+tools/accVsTol_loop.m'.
%
%xString : string
%   The name of what you want to plot. Availible things to plot are:
%       -'myX' (The solution points)
%       -'myH' (The step sizes)
%       -'myR' (The step size ratios)
%       -'errorVecFromAccVsTol' (The accuracy from the accVsTol-function)
%       -'errorEstimate' (The error estimates used inside the solver to
%                   controll the step size)
%   If none of these things are chosen, an error is thrown.
%
%yString : string
%   The name of what you want to plot. Availible things to plot are:
%       -'myX' (The solution points)
%       -'myH' (The step sizes)
%       -'myR' (The step size ratios)
%       -'errorVecFromAccVsTol' (The accuracy from the accVsTol-function)
%       -'errorEstimate' (The error estimates used inside the solver to
%                   controll the step size)
%   If none of these things are chosen, an error is thrown.
%
%plotly : boolean
%   If true, the plot is sent to plotly (and plotted in matlab),
%   otherwise it is just plotted in matlab. For it to work with plotly yo
%   have to set up an account at plot.ly and follow the instructions on how
%   to sync plots with plotly using matlab.
%
%matlabPlotFunction : string
%   The name of the matlab plot function you want to use. Availible are:
%       -'semilogx'
%       -'semilogy'
%       -'loglog'
%       -'plot'
%
%Returns
%-------
%legendNames : cell array
%   Cell array containing the names (strings) of all legends in the plot.
%
%Raises
%------
%'PlotFunction:BadXString'
%   If the value of yString is now allowed to plot, this error will be
%   thrown.
%
%'PlotFunction:BadYString'
%   If the value of yString is now allowed to plot, this error will be
%   thrown.

clf
plotTitle = struct.plotTitle;
names = fieldnames(struct);
name = names(1);
name = name{1};
len = length(names);
legendNames = {};

switch yString
    case 'cpuTime'
        yLable = 'CPU time'
    case 'rejectedSteps'
        yLable = 'The number of rejected steps'
    case 'numberOfSteps'
        yLable = 'The total number of steps'
    case 'nbrStepsRTooBig'
        yLable = 'The number of steps that resulted in a too big step size ratio r'
    case 'succeededTols'
        yLable = 'Tolerance'
    case 'errorFromAccVsTol'
        errorMode = struct.([name]).errorMode;
        yLable = strcat('The error from accVsTols, errorMode=', errorMode)
    otherwise
        ME = MException('PlotFunc:BadYString','This yString can not be',...
            'chosen. Try another one!');
        throw(ME);
end

switch xString
    case 'cpuTime'
        xLable = 'CPU time'
    case 'rejectedSteps'
        xLable = 'The number of rejected steps'
    case 'numberOfSteps'
        xLable = 'The total number of steps'
    case 'nbrStepsRTooBig'
        xLable = 'The number of steps that resulted in a too big step size ratio r'
    case 'succeededTols'
        xLable = 'Tolerance'
    case 'errorFromAccVsTol'
        errorMode = struct.([name]).errorMode;
        xLable = strcat('The error from accVsTols, errorMode: ', errorMode)
    otherwise
        ME = MException('PlotFunc:BadXString','This xString can not be',...
            'chosen. Try another one!');
        throw(ME);
end
    
for i = 1:len-1
    name = names(i);
    name = name{1};
    try
        x = struct.([name]).([xString]);
        y = struct.([name]).([yString]);
    catch ME
        name
        disp(ME.message)
        continue;
    end
    %To plot, the following has to be true  
    if ((length(x) == length(y)) && (length(x) > 0))
        switch matlabPlotFunction
            case 'semilogx'
                semilogx(x,y)
            case 'semilogy'
                semilogy(x,y)
            case 'loglog'
                loglog(x,y)
            case 'plot'
                plot(x,y)
            otherwise
                ME = MException('PlotFunc:BadMatlabPlotFunction','This matlabPlotFunction does not exist',...
                    'chosen. Availible are: "semilogx", "semilogy", "loglog" and "plot"!');
                throw(ME);
        end
        hold on
        legendNames{end + 1,1} = name;
    end
    
end

titleName = strcat(plotTitle,', ',' y = ',yString,', ',' x = ',xString)
%titleName = 'Brusselator, nbrOfTols=100, pmme, factor=2^(p+1)-1, y=nbrOfSteps, x=tol'
title(titleName)
legend(legendNames)
xlabel(xLable); % x-axis label
ylabel(yLable); % y-axis label

%Note
dummy = struct.([name]);
try
    line1 = sprintf('MySolver: %s \n',dummy.MySolver);
    str = [line1];
    line2 = sprintf('Reference: %s \n',dummy.Reference);
    str = [str,line2];
    line3 = sprintf('errorMode: %s \n',dummy.errorMode);
    str = [str,line3];
    line4 = sprintf('perc: [%g,%g] \n',dummy.perc(1,1),dummy.perc(1,2));
    str = [str,line4];
    line5 = sprintf('unit: %d \n',dummy.errorPerUnitStep(1));
    str = [str,line5];
    line6 = sprintf('useBypass: %d \n',dummy.usebypass);
    str = [str,line6];
    line7 = sprintf('predFirstStep: %d \n',dummy.predFirstStep);
    str = [str,line7];
    line8 = sprintf('norm: %s \n',func2str(dummy.norm));
    str = [str,line8];
    line9 = sprintf('factorFunc: %s \n',dummy.factorFunc);
    str = [str,line9];
    line10 = sprintf('alpha: %s \n',dummy.alpha);
    str = [str,line10];
catch ME
    display(ME.message);
end
xl = xlim();
yl = ylim();

text(xl(1), yl(2), str);
%

grid on
fig2plotly();
end

