function [C,x,y,z1,z2] = stabRegion(theta,solverName)
%Wrapper method used to plot the stability region of a method corresponding
%to the supplied theta-vector and solverName. See documentation for
%'getAlphasAndBetas_pmme.m', 'getAlphasAndBetas_pmmip.m',
%'getAlphasAndBetas_pmmi.m' and 'plotStabReg.m' for more information.
%
%Parameters
%----------
%theta : vector
%   Row matrix (length = k-1 or k depending on solverName) containing all
%   thetas corresponding to the LMM. The vector is ordered according to:
%   [theta_{1}, theta_{2}, ...].
%solverName : string
%   The name of the solver used. Options: 'pmme', 'pmmip' or 'pmmi'.
%
%Returns
%-------
%C : float
%   The normalized error constant belonging to the LMM
%x : vector
%   The real part of the lotus curve values z.
%y : vector
%   The imaginary part of the lotus curve values z.
%z1 : function handle
%   z1 = @(w) (rho(w)/(sigma(w))).
%z2 : function handle
%   z2 = @(phi) (rho(exp(1i*phi))/(sigma(exp(1i*phi))));
%
%Raises
%------
%'Solver:NoSuchSolver'
%   Is raised if solverName is neither 'pmme', 'pmmip' nor 'pmmi'.

switch lower(solverName)
    case 'pmme'
        [alphas,betas] = tools.getAlphasAndBetas_pmme(theta);
        order = length(theta) + 1;
        k = order;
    case 'pmmi'
        [alphas,betas] = tools.getAlphasAndBetas_pmmi(theta);
        order = length(theta);
        k = order;
    case 'pmmip'
        [alphas,betas] = tools.getAlphasAndBetas_pmmip(theta);
        order = length(theta) + 2;
        k = order - 1;
    otherwise
        ME = MException('Solver:NoSuchSolver',...
                sprintf('The solver "%s" does not exist.',solverName));
        throw(ME);
end
[C,x,y,z1,z2] = tools.plotStabReg(alphas, betas,order,k);
end

