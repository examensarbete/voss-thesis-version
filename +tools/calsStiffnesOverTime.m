function [sVec,t] = calsStiffnesOverTime(jac,t,y)
%Calculates the stiffnes over time for a problem.
%
%Parameters
%----------
%jac : function handle
%   The jacobian of the RHS function corresponding to the function for
%   which we want to calculate the stiffness.
%t : vector
%   Vector containing the time values at which the stiffness shall be
%   calculated.
%y : matrix
%   The solution points at the time values in t of the function
%   for which we want to calculate the stiffness. Let the solutions for
%   different time points be added row wise, i.e.
%       y = [y1(t1) y2(t1) ...
%            y1(t2) y2(t2) ...
%            y1(t3) y2(t3) ...
%              .       .   ...
%              .       .   ...
%              .       .   ...]
%
%Returns
%-------
%sVec : vector
%   Vector containing the stiffness values at the time points t.
%t : vector
%   Vector containing the time values at which the stiffness was
%   calculated. This is the same t vector as was supplied to the function.

[nbrOfSteps, nbrOfVars] = size(y);
sVec = [];
for i = 1:nbrOfSteps
    J = jac(t(i),y(i,:)');
    s = tools.localStiffness(J);
    sVec = [sVec, s];
end

