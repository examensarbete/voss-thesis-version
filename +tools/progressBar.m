function numChars = progressBar(progress, varargin)
%DOCUMENTATION NOT FINISHED!
%
%Function used by the accVsTol-functions. It is a bar showing the
%percentage of the progress of the accVsTol-function.
%
%If the accVsTol is supposed to calculate the solution of 10 tolerances for
%one method-filter combination, then for every finished tolerance, the
%progressbar is added by 10 percent units.
%
%Parameters
%----------
%progress : vector
%   Row matrix 
%varargin : vector
%   Row matrix
%
%Returns
%-------
%numChars : float
%   The normalized error constant belonging to the LMM

len = 60;
percentage = false;
char = '=';
numChars = -1;

for i = 1:2:length(varargin)
    name = varargin{i};
    value = varargin{i+1};
    switch name
        case 'length'
            len = value;
        case 'percentage'
            percentage = value;
        case 'character'
            char = value;
        case 'numChars'
            numChars = value;
    end
end

unitSize = 1/len;
units = floor(progress/unitSize);

if progress == 0
    numChars = -1;
end

if percentage
    removeLength = len+7;
else
    removeLength = len+2;
end

if units > numChars
    if progress > 0
        fprintf(1,repmat('\b',1,removeLength));
    end
    
    fprintf(1,'[');
    
    fprintf(1,repmat(char,1,units));
    fprintf(1,repmat(' ',1,len-units));
    fprintf(1,']');
    if percentage
        fprintf(1,'%3.0f %%',progress*100);
    end
end
numChars = units;
end