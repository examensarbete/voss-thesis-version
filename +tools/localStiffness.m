function s = localStiffness(jac)
%Calculates the local stiffness s of a system of equations.
%
%Parameters
%----------
%jac : matrix
%   The jacobian at a time point t (and solution x) belonging to a RHS
%   function.
%
%Returns
%-------
%s : float
%   The local stiffness value.

jacSym = (jac + jac')/2;
lambda = eig(jacSym);
lambda_min = min(lambda);
lambda_max = max(lambda);
s = (lambda_min + lambda_max)/2;
end

