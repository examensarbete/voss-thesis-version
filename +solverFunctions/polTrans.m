function pNew = polTrans(p,a)
%Takes the coefficients for a polynomial and translates it to new
%coordinates xNew, such that xNew = x - a.
%
%
%Parameters
%----------
%p : row vector
%   The coefficients [a_n, a_(n-1),..., a_0] for the polynomial
%   p(x) = a_n x^n + a_(n-1) x^(n-1) + ... + a_0, which is to be translated
%
%a : float
%   The amount with which the polynomial is to be translated
%
%Returns
%-------
%pNew : row vector
%   The new coefficients for the polynomial in the new coordinate system

l = length(p);
pNew = zeros(1,l);
div = 1;

pNew(l) = polyval(p,-a);
for i=1:l-1
    p = polyder(p);
    div = div*i;
    pNew(l-i) = polyval(p,-a)/div;
end

end
