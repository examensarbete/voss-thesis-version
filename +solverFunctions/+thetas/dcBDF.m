function thetas = dcBDF(p,pmin,pmax)
% Returns the theta parameters for the Adams-Moulton method of order p
%
% Used to generate a theta vector for Adams-Moulton methods of order p.
% This is a test version which also includes the option to define the
% minimal and maximal order available.
%
% Parameters
% ----------
% p : int
%   The order for which the theta vector is to be generated
% pmin : int
%   The minimal available order
% pmax : int
%   The maximal available order
%
% Returns
% -------
% thetas : vector
%   A 1x(p-1) vector containing the theta parameters
%
% Raises
% ------
% 'Thetas:NoMethod'
%   If no method of order p is available
%
% 'Thetas:TypeError'
%   If p is not an integer greater than 0

if mod(p,1) ~= 0 || p < 0
    ME = MException('Thetas:TypeError',['order p must be a positive'...
    'integer']);
    throw(ME);
end
if p < pmin || p > pmax || p < 2
    ME = MException('Thetas:NoMethod',['No dcBDF method of'...
        ' order %d is available.'], p);
    throw(ME);
else
    thetas = (1:p-2)/(p);
    thetas = ones(1,p-2)*pi/2;
end
