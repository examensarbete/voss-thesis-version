function [jac] = getJac(f,t,x)
%Calculates the Jacobian for the function f in the point x
%using central finite differences.
%
%Parameters
%----------
%f : function handle
%   The (RHS) function for which the Jacobian is to be calculated.
%x : row vector
%   The solution point belonging to the time point t at which the Jacobian
%   is to be calculated.
%t : float
%   The time point t at which the Jacobian is to be calculated.
%
%Returns
%-------
%jac : matrix
%   The calculated Jacobian.

m = length(x);
n = length(f(t,x));
jac = zeros(m,n);
e_i = zeros(m,1);
for i=1:m
    h_i = 1e-8;
    e_i(i) = 1;
    jac(i,:) = (f(t,x + h_i*e_i) - f(t,x - h_i*e_i))/(2*h_i);
    e_i(i) = 0;
end
jac = jac';
end
