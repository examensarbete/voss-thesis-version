function p = polE(theta,h,x,xDot)
%Returns polynomial coefficients for explicit parameterized multistep
%methods of order k.
%
%Returns the polynomial coefficients used to calculate the next step when
%using the parameterized formulasion of multistep methods. This function is
%used for explicit k-step methods of order k. The local coordinates for the
%polynomial are such that P(0) corresponds to the most recently calculated
%step and P(h) corresponds to the step which is to be calculated.
%
%Parameters
%----------
%theta : vector
%   The vector containing the theta parameters defining the method. For a
%   k-step method this vector has length k-1.
%h : vector
%   A vector containing at least the previous k step lengths
%x : matrix
%   A matrix containing at least the previous k sets of solution values.
%   The sets of solution values are stored row-wise.
%xDot : matrix
%   A matrix containing at least the previous k sets of values for the
%   derivative of the solution values (the same as the rhs evaluations of
%   the solution). The sets are stored row-wise.
%
%Returns
%-------
%p : matrix
%   A (k+1)xn matrix, where n is the number of unknowns in the system of
%   differential equations which is solved. Each column contains the
%   coefficients for the polynomial used to calculate the next value for
%   each unknown.

k = length(theta)+1;
len = length(x(end,:));
c = cos(theta);
s = sin(theta);
A = zeros(k+1,k+1);
A(k,k) = 1;
A(k+1,k+1) = 1;
A(1:k-1,k+1) = c';
rhs = zeros(k+1,len);
rhs(k,:) = xDot(end,:);
rhs(k+1,:) = x(end,:);
S=zeros(1,k-1);
if k>1
    S(1) = -h(end);
end
for j = 1:k-2
    S(j+1) = S(j)-h(end-j);
end
for i=1:k-1
    for j=1:k
        A(i,j) = c(i)*S(i)^(k-j+1)+(k+1-j)*s(i)*h(end-i+1)*S(i)^(k-j);
    end
    rhs(i,:)= c(i)*x(end-i,:)+s(i)*h(end-i+1)*xDot(end-i,:);
end
p = A\rhs;
end
