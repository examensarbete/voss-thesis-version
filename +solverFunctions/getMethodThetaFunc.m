function [getThetas] = getMethodThetaFunc(method)
%Creates a function handle to a function returning a theta vector for a
%method of a certain order.
%
%This function acts as a wrapper function for selecting different getTheta
%functions used in the solvers. It takes the name of a method and returns a
%function handle to the corresponding function which returns theta vectors
%for this method.
%
%Parameters
%----------
%method : string
%   The name of the method followed by the lower and upper order limit on
%   the form: name_lowerOrder-upperOrder. For example, to get
%   Adams-Bashforth 2-12 input 'ab_2-12'. The following k-step methods are
%   available:
%
%       + Explicit methods of order k:
%
%           - ab (Adams-Bashforth) Lower order limit: 1 Upper order limit:
%           none
%           - edf Lower order limit: 1 Upper order limit: none
%           - nystrom Lower order limit: 3 Upper order limit: 5
%
%       + Implicit methods of order k:
%
%           - bdf (Backward differentiation formul) Lower order limit: 1
%           Upper order limit: 5
%
%       + Implicit methods of order k+1:
%           - am (Adams-Moulton) Lower order limit: 3 Upper order limit:
%           none
%
%Returns
%-------
%getThetas : function handle
%   to a function getThetas(k) which returns the theta paramaters for an
%   exDotlicit method of order k. The function corresponding to this handle
%   throws an exception with id 'Thetas:NoMethod' if a method of order k is
%   unavailable.
%
%Raises
%------
%'Method:NoSuchMethod'
%   If the name of the requested method does not exist
%
%'Method:NoSuchOrders'
%   If the orders requested does not exist

str = strsplit(method,'_');
name = str{1};
str = strsplit(str{2},'-');
low = str2num(str{1});
upp = str2num(str{2});

if upp < low
    ME = MException('Method:NoSuchOrders',['Upper order limit can not be'...
        'lower than the lower order limit']);
    throw(ME);
end

if (mod(low,1) ~= 0 || low < 0) || (mod(upp,1) ~= 0 || upp < 0)
    ME = MException('Method:NoSuchOrders',['order limits must be '...
        'positive integers']);
    throw(ME);
end

switch lower(name)
    case 'ab'
        if low < 1
            ME = MException('Method:NoSuchOrders',['The minimum '...
                'lower limit for the Adams-Bashforth method is 1']);
            throw(ME);
        end
        getThetas = @(k) solverFunctions.thetas.AB(k,low,upp);

    case 'edf'
        if low < 1
            ME = MException('Method:NoSuchOrders',['The minimum '...
                'lower limit for the EDF method is 1']);
            throw(ME);
        end
        getThetas = @(k) solverFunctions.thetas.EDF(k,low,upp);

    case 'nystrom'
        if low < 3
            ME = MException('Method:NoSuchOrders',['The minimum '...
                'lower limit for the Nystrom method is 3']);
            throw(ME);
        end
        if upp > 5
            ME = MException('Method:NoSuchOrders',['The maximum '...
                'upper limit for the Nystrom method is 5']);
            throw(ME);
        end
        getThetas = @(k) solverFunctions.thetas.Nystrom(k,low,upp);

    case 'am'
        if low < 2
            ME = MException('Method:NoSuchOrders',['The minimum '...
                'lower limit for the Adams-Moulton method is 2']);
            throw(ME);
        end
        getThetas = @(k) solverFunctions.thetas.AM(k,low,upp);

    case 'dcbdf'
        if low < 2
            ME = MException('Method:NoSuchOrders',['The minimum '...
                'lower limit for the dcBDF method is 2']);
            throw(ME);
        end
        getThetas = @(k) solverFunctions.thetas.dcBDF(k,low,upp);

    case 'bdf'
        if low < 1
            ME = MException('Method:NoSuchOrders',['The minimum '...
                'lower limit for the Backward differentiation formula '...
                'is 1']);
            throw(ME);
        end
        if low > 5
            ME = MException('Method:NoSuchOrders',['The maximum upper '...
                'limit for the Backward differentiation formula is 5']);
            throw(ME);
        end
        getThetas = @(k) solverFunctions.thetas.BDF(k,low,upp);

    otherwise
        ME = MException('Method:NoSuchMethod',['The requested method '...
            sprintf('''%s''',name), ' does not exist']);
        throw(ME);
end
