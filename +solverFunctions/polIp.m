function [p,iters] = polIp(f,jac,theta,h,x,xDot,t,coeffs,TOL)
%Returns polynomial coefficients for implicit parameterized k-step multistep
%methods of order k+1
%
%Returns the polynomial coefficients used to calculate the next step when
%using the parameterized formulasion of multistep methods. This function is
%used for implicit k-step methods of order k+1. The local coordinates for
%the polynomial are such that P(0) corresponds to the most recently
%calculated step and P(h) corresponds to the step which is to be calculated.
%
%Parameters
%----------
%f : function handle
%   The right hand side function of the system of equations which is solved
%
%jac : function handle
%   The Jacobian of the right hand side function
%
%theta : vector
%   The vector containing the theta parameters defining the method. For a
%   k-step method this vector has length k-1.
%
%h : vector
%   A vector containing at least the previous k step lengths
%
%x : matrix
%   A matrix containing at least the previous k sets of solution values.
%   The sets of solution values are stored row-wise.
%
%xDot : matrix
%   A matrix containing at least the previous k sets of values for the
%   derivative of the solution values (the same as the rhs evaluations of
%   the solution). The sets are stored row-wise.
%
%t : float
%   The time point for the new step which is to be calculated
%
%coeffs : matrix
%   A (k+2)xn matrix containing the coefficients for the previous
%   polynomials (see the return value for more information). This is used as
%   starting values for the Newton iterations.
%
%TOL : float
%   The tolerance used to determine when to stop the Newton iterations
%
%Returns
%-------
%p : matrix
%   A (k+2)xn matrix, where n is the number of unknowns in the system of
%   differential equations which is solved. Each column contains the
%   coefficients for the polynomial used to calculate the next value for
%   each unknown.
%
%iters : int
%   The number of Newton iterations performed

c = cos(theta);
s = sin(theta);
k = length(theta) + 1;
%   simplified-Newton iteration
m = length(x(1,:));
hn = h(end);
%Reshape the coeffecient matrix to a column vector and remove the two last
%coefficients (not necessary, due to the structure we already know them)
w = coeffs(1:end-2,:);
w = reshape(flip(w)',k*m,1);

%Construct the Jacobian to the system which is to be solved
J = jacobian(jac,  t, x(end,:)', h, c, s, m,k);
for j = 1:12
    %Create the F-matrix to the system which is to be solved
    F = Fnewton(f, t, x, xDot, w, h, c, s, m,k);
    %Take a step in the Newton iteration
    delta = -J\F;
    w = w + delta;
    if norm(delta,inf) < TOL/10
        break
    end
end


%Reshape the solution to the correct form and
p = reshape(w,m,k)';
%Add the last to coefficients (due to the structure we know that these are x
%and xDot)
p = flip(p);
%Add the last to coefficients (due to the structure we know that these are x
%and xDot)
p(end+1:end+2,:) =[xDot(end,:);x(end,:)];
iters = j;
end

function J = jacobian(jac,tn,x,h,c,s,m,k)
d = jac(tn,x);
I = eye(m);
S = 0;
J = zeros(k*m,k*m);
for i = 1:k-1
    ci = c(i);
    hn = h(end-i);
    sh = s(i)*hn;
    S = S-hn;
    for j = 1:k
        J((i-1)*m+1:i*m,(j-1)*m+1:j*m) = S^j*(ci*S+(j+1)*sh)*I;
    end
end
for j = 1:k
    J((k-1)*m+1:k*m,(j-1)*m+1:j*m) = h(end)^j*(h(end)*d-(j+1)*I);
end
end

function F = Fnewton(f,tn,x,xDot,w,h,c,s,m,k)
F=zeros(k*m,1);
x = x';
xDot = xDot';
S = 0;
for i=1:k-1
    ci = c(i);
    sh = s(i)*h(end-i);
    S = S-h(end-i);
    Fs = (ci*S+sh*(k+1))*w((k-1)*m+1:k*m);
    for j=2:k
        Fs = S*Fs + (ci*S+(k+2-j)*sh)*w((k-j)*m+1:(k+1-j)*m) ;
    end
    F((i-1)*m+1:i*m) = S*Fs+ci*(x(:,end)-x(:,end-i)+S*xDot(:,end))+...
        sh*(xDot(:,end)-xDot(:,end-i));
end
hn = h(end);
sn = w((k-1)*m+1:k*m);
for n = k-1:-1:1
    sn = hn*sn + w((n-1)*m+1:n*m);
end
sn = hn*sn + xDot(:,end);
yn = hn*sn + x(:,end);
sn = (k+1)*w((k-1)*m+1:k*m);
for n = k-1:-1:1
    sn = hn*sn + (n+1)*w((n-1)*m+1:n*m);
end
ypn = hn*sn +xDot(:,end);
F((k-1)*m+1:k*m) = f(tn,yn) - ypn;
end
