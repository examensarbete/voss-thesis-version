function [theta] = getMethodVec(method)
%Creates a vector containing the method parameters.
%
%Parameters
%----------
%method : string
%   The name of the wanted method. The following k-step methods are
%   available:
%
%       + Explicit methods of order k:
%
%           - ab2, ab3, ab4,... (Adams-Bashforth 2-infinity)
%           - edf3, edf4, edf5,...
%           - nystrom3, nystrom4, nystrom5 (Nyström methods)
%           - edc22, edc23, edc33, edc24, edc34, edc45
%
%       + Implicit methods of order k:
%
%           - kregel
%           - bdf2, bdf3, bdf4, bdf5, bdf6
%
%       + Implicit methods of order k+1:
%
%           - milne2, milne4
%           - idc23, idc24, idc34, idc45, idc56
%           - am2, am3, am4,... (Adams-Moulton 2-infinity)
%           - dcbdf2, dcbdf3, dcbdf4,...
%
%Returns
%-------
%theta : row vector
%   The method parameters

method = lower(method);

% --------------------------------------------------------------------------
% Methods belonging to E_k (explicit order k)
% --------------------------------------------------------------------------
switch method
    %Nyström methods
    case 'nystrom3'
        theta = [atan(-2/3), pi/2];
    case 'nystrom4'
        theta = [atan(-5/3), pi/2, pi/2];
    case 'nystrom5'
        theta = [atan(-133/45), pi/2, pi/2, pi/2];
    
    %EDC methods
    case 'edc22'
        theta = [atan(14/3), pi/2];
    case 'edc23'
        theta = [atan(49/6), pi/2, pi/2];
    case 'edc33'
        theta = [atan(7/2), atan(39/4), pi/2];
    case 'edc24'
        theta = [atan(1121/90), pi/2, pi/2, pi/2];
    case 'edc34'
        theta = [atan(53/10), atan(219/10), pi/2, pi/2];
    case 'edc45'
        theta = [atan(193/45), atan(121/10), atan(692/15), pi/2, pi/2];
end

%AB methods
if(strncmp(method,'ab',2))
    order = str2num(method(3:end));
    theta = ones(1,order-1)*pi/2;
%EDF methods
elseif (strncmp(method,'edf',3))
    order = str2num(method(4:end));
    theta = atan(2:order);
end

% --------------------------------------------------------------------------
% Methods belonging to I_k (implicit order k)
% --------------------------------------------------------------------------
switch method
    case 'kregel'
        theta = [atan(154/543), atan(-11/78), 0];
end

%BDF methods
if(strncmp(method,'bdf',3))
    order = str2num(method(4:end));
    if order <= 6
        theta = zeros(1,order);
    else
        ME = MException('Solver:BadMethodName',['This method does not exist'...
            'We do not use BDF methods with order >= 6']);
        throw(ME);
    end
end

% --------------------------------------------------------------------------
% Methods belonging to I_k^+ (implicit order k+1)
% --------------------------------------------------------------------------
switch method
    case 'milne2'
        theta = [atan(1/3)];
    case 'milne4'
        theta = [atan(4/15), pi/2, pi/2];
    case 'idc23'
        theta = [atan(7/6), pi/2];
    case 'idc24'
        theta = [atan(26/15), pi/2, pi/2];
    case 'idc34'
        theta = [atan(4/5), atan(33/20), pi/2];
    case 'idc45'
        theta = [atan(28/45), atan(11/10), atan(32/15), pi/2];
    case 'idc56'
        theta = [atan(43/84), atan(6/7), atan(29/21), atan(55/21), pi/2];
end

%AM methods
if(strncmp(method,'am',2))
    k = str2num(method(3:end));
    theta = ones(1,k-1)*pi/2;
%dcBDF methods
elseif (strncmp(method,'dcbdf',5))
    k = str2num(method(6:end));
    j = 1:k-1;
    theta = atan((j+1)/(k+1));
end

% --------------------------------------------------------------------------
% No method was found
% --------------------------------------------------------------------------
if exist('theta') == 0
    ME = MException('Solver:BadMethodName','This method does not exist');
    throw(ME);
end

end
