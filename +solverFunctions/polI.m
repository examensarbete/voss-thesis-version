function [p,iters] = polI(f,jac,theta,h,x,xDot,t,coeffs,TOL)
%Returns polynomial coefficients for implicit parameterized k-step multistep
%methods of order k.
%
%Returns the polynomial coefficients used to calculate the next step when
%using the parameterized formulasion of multistep methods. This function is
%used for implicit k-step methods of order k. The local coordinates for the
%polynomial are such that P(-h) corresponds to the most recently calculated
%step and P(0) corresponds to the step which is to be calculated.
%
%Parameters
%----------
%f : function handle
%   The right hand side function of the system of equations which is solved
%
%jac : function handle
%   The Jacobian of the right hand side function
%
%theta : vector
%   The vector containing the theta parameters defining the method. For a
%   k-step method this vector has length k-1.
%
%h : vector
%   A vector containing at least the previous k step lengths
%
%x : matrix
%   A matrix containing at least the previous k sets of solution values.
%   The sets of solution values are stored row-wise.
%
%xDot : matrix
%   A matrix containing at least the previous k sets of values for the
%   derivative of the solution values (the same as the rhs evaluations of
%   the solution). The sets are stored row-wise.
%
%t : float
%   The time point for the new step which is to be calculated
%
%coeffs : matrix
%   A (k+2)xn matrix containing the coefficients for the previous
%   polynomials (see the return value for more information). This is used as
%   starting values for the Newton iterations.
%
%TOL : float
%   The tolerance used to determine when to stop the Newton iterations
%
%Returns
%-------
%p : matrix
%   A (k+2)xn matrix, where n is the number of unknowns in the system of
%   differential equations which is solved. Each column contains the
%   coefficients for the polynomial used to calculate the next value for
%   each unknown.
%
%iters : int
%   The number of Newton iterations performed

c = cos(theta);
s = sin(theta);
k = length(theta);
%   simplified-Newton iteration
m = length(x(1,:));
hn = h(end);
%Reshape the coeffecient matrix to a column vector
w = reshape(coeffs',(k+1)*m,1);

k = length(theta);
m = length(w)/(k+1);
S=zeros(1,k);
S(1) = -h(end);
for j = 2:k
    S(j) = S(j-1)-h(end+1-j);
end
J = jacobian(jac, t, x, w, h,  S, c, s, k, m);
for j = 1:12
    F = Fnewton(f, t, x, xDot, w, h, S, c, s, k, m);
    delta = -J\F;
    w = w + delta;
    if norm(delta,inf) < TOL/10
        break
    end
end

%Reshape the solution to the correct form
p = reshape(w,m,k+1)';
iters = j;
end

function J = jacobian(df, tend,x, ~, h, S, c, s,k,m)
d = df(tend,x(end,:)');
I = eye(m);
J = zeros((k+1)*m,(k+1)*m);
for i = 1:k
    for j = 1:k
        ci = c(i); sh = s(i)*h(end+1-i); Si = S(i);
        J((i-1)*m+1:i*m,(j-1)*m+1:j*m) = Si^(k-j)*(ci*Si+sh*(k+1-j))*I;
    end
    J((i-1)*m+1:i*m,k*m+1:(k+1)*m) = ci*I;
end
J(k*m+1:(k+1)*m,(k-1)*m+1:k*m) = -I;
J(k*m+1:(k+1)*m,k*m+1:(k+1)*m) = d;
end

function F = Fnewton(f,tend,x,xDot,w,h,S,c,s,k,m)
F=zeros((k+1)*m,1);
x = x';
xDot = xDot';
for i=1:k
    ci = c(i); sh = s(i)*h(end+1-i); Si = S(i);
    Fs = (ci*Si+sh*k)*w(1:m);
    for j=2:k
        Fs = Si*Fs+(ci*Si+sh*(k+1-j))*w((j-1)*m+1:j*m);
    end
    F((i-1)*m+1:i*m) = Fs + ci*w(k*m+1:(k+1)*m)-ci*x(:,end+1-i)-sh*xDot(:,end+1-i);
end
F(k*m+1:(k+1)*m) = f(tend,w(k*m+1:(k+1)*m))-w((k-1)*m+1:k*m);
end
