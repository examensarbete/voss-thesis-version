function [t,x,statistics] = pmmip(f,tSpan,x0,varargin)
%Solver of non-stiff differential equations using a k-step implicit multistep method of
%order k+1.
%
%Integrates the system of differential equations dx/dt = f(t,x) from time
%tSpan(1) to time tSpan(2). The solver has (as default) variable step size.
%
%:Example:
%
%>>> f = @(t,x) [3*x(2);-2*x(1)];
%>>> jac = @(t,x) [0, 3; -2, 0];
%>>> x0 = [1;2];
%>>> tSpan = [0,1];
%>>> [t,x,statistics] =...
%   pmmip(f,tSpan,x0,'methodName','AM5','tol',1e-6,'perc',[0.9,1.1],'jac',jac);
%
%Parameters
%----------
%f : function handle
%   Function handle to the right hand side (RHS) function for the system of
%   differential equations. For a scalar t and a vector x it must return a
%   column vector corresponding to f(t,x).
%
%tSpan : vector (lenght 2, row matrix)
%   tSpan = [$T_{initial}$, $T_{final}$], where $T_{initial}$ is the initial
%   integration time and $T_{final}$ is the final integration time.
%
%x0 : vector (column matrix)
%   The initial condition x(0) = x0, given as a column matrix.
%
%varargin : options list
%   Options replacing default values on the form of strings
%   with corresponding value (see example above). The following options are
%   available:
%
%   'tol'
%       Tolerance, default 1e-6.
%   'methodname'
%       The name (string) of the method to be used. Default is 'ab4'. The
%       following methods are available:
%
%           - 'am2','am3','am4',... (Adams-Moulton 2-infinity)
%           - 'dcbdf2','dcbdf3','dcbdf4',...
%           - 'milne2', 'milne4'
%           - 'idc23', 'idc24', 'idc34', 'idc45', 'idc56'
%   'methodvec'
%       The method vector (row vector) of the method to be used.
%       THIS OPTION SHOULD ONLY BE USED IF YOU KNOW WHAT YOU ARE DOING!
%   'filtername'
%       The name (string) of the filter to be used to control the step
%       size. Default is 'PI3333'. The following filters are available:
%
%           - 'fixedStepSize'
%           - 'H211D','H211b','H211PI'
%           - 'PI3333'
%           - 'PI3040'
%           - 'PI4020'
%           - 'H312D','H312b','H312PID'
%           - 'H321D','H321'
%   'filtervec'
%       A filter vector (row matrix) of the filter to be used.
%       THIS OPTION SHOULD ONLY BE USED IF YOU KNOW WHAT YOU ARE DOING!
%   'perc'
%       perc = [r_{min}, r_{max}], where r_{min} is the minimum allowed
%       step size ratio and r_{max} is the maximum allowed step size ratio.
%       Default is [0.8,1.2].
%   'relerr'
%       A boolean. If true, relative error control is used, else, absolute
%       error control is used. Default is true.
%   'unit'
%       A boolean. If true, "error per unit step" is used in the step size
%       controller. Else, "error per step" is used instead. Default is
%       false.
%   'initialstep'
%       A scalar (real pos value). This is the initial step size used by
%       the solver. Default value is computed using "autostart".
%   'norm'
%       A function handle (f: R^n --> R). The norm used to calculate the
%       error. Default is the inf-norm.
%   'jac'
%       A function handle (taking the parameters (t,x)) to a function which
%       outputs the Jacobian of the RHS function. If none is provided the
%       Jacobian will be calculated numerically using finite differences.
%   'interpolation'
%       If true, the end point will be calculated using interpolation (the
%       previously calculated polynomial is evaluated in the end point). If
%       false, the end point will instead be calculated by recalculating
%       the last step using a shorter step size such that the end point is
%       hit. Default is true.
%   'usebypass'
%       A boolean. If true, bypass is used in the beginning and during the
%       integration, else, bypass is only used in the beginning as a
%       bootstrapping mechanism (since we have too little information).
%   'analyticsol'
%       A function handle. The analytical solution to the RHS function. If
%       this is supplied, the "prediction" at every step is the analytical
%       solution (i.e. no prediction), which further means that the
%       controller is using the real global error to control the step size.
%          
%Returns
%-------
%t : vector
%   All accepted time points.
%
%x : vector
%   The solution values in all time points.
%
%statistics : struct
%   A matlab struct containing different quantities of statistical interest.
%   The following fields are contained:
%
%   'solver'
%       The solver used, i.e., 'pmmip'.
%   'methodName'
%       The name of the method used.
%   'methodOrder'
%       The order of the method used.
%   'kStepMethod'
%       The number of steps used by the method.
%   'filterName'
%       The name of the filter used.
%   'tol'
%       The tolerance used.
%   'perc'
%       The minimum and maximum step size ratio allowed used.
%   'relativeError'
%       True or false depending on relative error or absolute error was
%       used to calculate the error estimation.
%   'errorPerUnitStep'
%       True or false depending on "error per unit step" or "error per
%       step" was used by the step size controller.
%   'usebypass'
%       True or false depending on "by pass" was used during the whole
%       integration or not. For more info see the 'usebypass' optional
%       in-parameter information above.
%   'predFirstStep'
%       True or false depending on wether the first step was predicted or
%       not. For more info see the 'predfirststep' optional in-parameter
%       information above.
%   'normFunc'
%       The name of norm function used during the integration to calculate
%       the error at every step.
%   'cpuTime'
%       The CPU time length of the integration.
%   'jacProvided'
%       True if a jacobian was provided by the user, otherwise false. In
%       this case the jacobian was calculated using central finite
%       differences.
%   'interpolationMode'
%       True or false depending on if interpolation-mode was used or not to
%       calculate the end point. See 'interpolation' in the options list of
%       varargin.
%   'numberOfSteps'
%       The number of (accepted) steps taken (the length of the t-vector).
%   'rejectedSteps'
%       The number of rejected steps.
%   'rejectedStepsData'
%       A struct with labels 't','r' and 'x' containing the time points, step
%       size ratios and calculated values for all points where a step was
%       rejected.
%   'nbrStepsRTooBig'
%       The number of steps where the suggested step size ratio was larger
%       than allowed (these steps are not rejected, but the actual step size
%       ratio used is modified).
%   'nbrStepsRTooBigData'
%       A struct with labels 't','r' and 'x' containing the time points, step
%       size ratios and calculated values for all points where the suggested
%       step size ratio was too large.
%   'errorEstimate'
%       A vector containing the calculated error estimates in each point
%   'newtonIters'
%       The total number of newton iterations used to solve all equation
%       systems (the systems to get the coefficients of the polynomials).
%Raises
%------
%'Solver:NoSuchOption'
%   If the a option is supplied in 'varargin' that is not allowed/does not
%   exist.
%'Solver:NoSolution'
%   If the step size becomes to small and the solver is unable to find a
%   solution.

import init.autostart;
import init.start;
import control.errorController;
import solverFunctions.polIp;
import solverFunctions.polE;
import solverFunctions.getJac;
import solverFunctions.getMethodVec;
import control.getFilterVec;

% --------------------------------------------------------------------------
% Initialization of variables and starting paramaters
% --------------------------------------------------------------------------

tic
%Initialize parameters
rejectedSteps = 0; %Number of rejected steps (for statistical purposes)
rejected = false; %Flag indicating if the last step was rejected
t(1) = tSpan(1); %Time points
x(1,:) = x0; %Solution points
xDot(1,:) = f(t(1),x0); %Derivative of solution points
numUnknowns = length(x0); %Number of unknowns in the system to be solved
errors = []; %estimated errors (for statistical purposes)
orderstats = []; %order were used to take a step (for statistical purposes)
nbrStepsRTooBig = 0; %counts the number of times the controller recommended
                     %a step size which was too large
nbrStepsRTooBig_at = []; %Whenever a step size ratio is too big, we
                         %add info about the step here.
rejectedSteps_at = []; %Whenever a step size ratio is too small, we add
                       %info about the step here.
newtonIters = []; %Number of Newton iterations in each step
bypass = -1;
xNew = x0; %Initialize the vectors used to store calculated solution values
xPred = x0; %Initialize the vectors used to store calculated solution values

%Default values of optional parameters
interpolation = true;
jac = @(t,x) getJac(f,t,x); %numerical calc. of jacobian as default
providedJacobian = false;
normFunc = @(x) norm(x,Inf);
TOL = 1e-6; %default tolerance
filterName = 'PI3333'; %default filter parameters
methodName = 'AM4';
perc = [0.8,1.2]; %default min/max step size ratio
relerr = true; %flag to indicate if relative error is to be used
unit = false; %flag to indicate if error per unit step is to be used
predFirstStep = false;
bypass = 0;
usebypass = true;

%Check for user arguments overriding the default values
for i = 1:2:length(varargin)
    name = varargin{i};
    value = varargin{i+1};
    switch lower(name)
        case 'tol'
            TOL = value;
        case 'methodname'
            methodName = value;
        case 'methodvec'
            methodVec = value;
        case 'filtername'
            filterName = value;
        case 'filtervec'
            filterVec = value;
        case 'perc'
            perc = value;
        case 'relerr'
            relerr = value;
        case 'unit'
            unit= value;
        case 'initialstep'
            h(1) = value;
        case 'norm'
            normFunc = value;
        case 'jac'
            jac = value;
            providedJacobian = true;
        case 'interpolation'
            interpolation = value;
        case 'usebypass'
            usebypass = value;
        case 'analyticsol'
            analyticSol = value;
        otherwise
            ME = MException('Solver:NoSuchOption',...
                sprintf('The option "%s" does not exist.',name));
            throw(ME);
    end
end

%Get method vector
if (exist('methodVec','var'))
    method = methodVec;
else
    method = getMethodVec(methodName);
end

%Get filter vector
if (exist('filterVec','var'))
    filter = filterVec;
else
    filter = getFilterVec(filterName);
end

%Order and steps of the method
k = length(method) + 1; %The number of steps used in the method (k-step method)
order = k + 1;

%Check if user set initial step size, otherwise use autostart to calculate
%it
if exist('h') == 0
    h(1) = autostart(f, tSpan, x0, TOL, order);
end

%Calculate k-1 additional points so that we can start using the k-step
%method
if(exist('analyticSol','var') == 0)
    [h,t,x,xDot] = start(f,h,t,x,xDot,TOL,k);
else
    [h,t,x,xDot] = start(f,h,t,x,xDot,TOL,k,'analyticsol', @(t)analyticSol(x0,t));
end
tNew = t(end) + h(end);
hNew = h(end);
rNew = 1;
newtonIters = zeros(1,k);

%Set how many solution points we have now
N = k;

%Calculate the digital filter order (how many errors we need to feed the
%controller)
PD = floor(length(filter)/2) + 1;

%Set initial errors to be fed to the controller
controlErrors = ones(1,PD);

%Assume that all initial points are exact
errors = zeros(1,N);

%Use ABk to initialize the newton iteration in the first step
predThetas = ones(1,k-1)*pi/2;
polCoeffOld = polE(predThetas,h,x,xDot);
    
%Calculate initial prediction point used to estimate the error
if (predFirstStep && exist('analyticSol','var') == 0)
    for i=1:numUnknowns
        xPred(i) = polyval(polCoeffOld(:,i),hNew);
    end
elseif (exist('analyticSol','var'))
    xPred = analyticSol(x0,tNew);
end
polCoeffOld = [zeros(1,numUnknowns);polCoeffOld];

% --------------------------------------------------------------------------
% Main loop used to calculate the solution
% --------------------------------------------------------------------------
while t(N) < tSpan(2)
% --------------------------------------------------------------------------
% Calculation of the solution and error estimate
% --------------------------------------------------------------------------
    %Calculate the new coefficients. If N==k it means that we calculate the
    %first step, and in such a case the number of coefficients are too low,
    %and we need to add a row of ones
    [polCoeff,iters] = polIp(f, jac, method,[h(1:N-1) hNew], x(1:N,:), xDot(1:N,:),...
        tNew,polCoeffOld, TOL);
    if(~isfinite(sum(polCoeff)))
        ME = MException('Solver:NoSolution',['polCoeff are not',...
            ' finite. t = %d'],t(end));
        throw(ME);
    end

    for i=1:numUnknowns
        xNew(i) = polyval(polCoeff(:,i),hNew);
    end

    
    %Estimate the new error
    if(predFirstStep || N > k)
        if relerr == 0
            controlErrors(end) = normFunc(xPred - xNew)/TOL;
        else
            controlErrors(end) = normFunc(xPred - xNew)/(normFunc(xNew)*TOL+1e-16);
        end

% --------------------------------------------------------------------------
% Step size control
% --------------------------------------------------------------------------
    %Feed data to controller
        if bypass < PD - 1
            rNew = controlErrors(end)^(-1/order);
        else
            rNew = errorController(filter, order, controlErrors,...
                [h(N - PD + 1:end), hNew], unit);
        end
    end

% --------------------------------------------------------------------------
% Step size rejection/acceptance
% --------------------------------------------------------------------------
    %If the suggested step size is too small, then the error in the newly
    %calculated step is too large, and we reject this step.
    if rNew < perc(1)
        if(~usebypass)
            bypass = 0;
        end
        rejectedSteps = rejectedSteps + 1;
        rejectedSteps_at(end + 1,:) = [tNew,rNew,xNew'];
        tNew = tNew - hNew;
        if ~rejected %First rejection (in row)
            if(length(h) > 1)
                hNew = h(N-1);
            end
            hNew = perc(1)*hNew;
        else %Multiple rejections (in row)
            hNew = hNew*0.95;
            if hNew < 1e-16*diff(tSpan)
                ME = MException('Solver:NoSolution',['Too small step',...
                    'size reached at t=%d. Try increasing TOL.'],tNew);
                throw(ME);
            end
        end
        tNew = tNew + hNew;
        rejected = true;

    %If the suggested step size is too large and we use the maximum step
    %size instead.
    elseif rNew > perc(2)
        %The step is accepted, so we save the new calculated values and
        %increment the number of steps taken.
        nbrStepsRTooBig = nbrStepsRTooBig + 1;
        nbrStepsRTooBig_at(end + 1,:) = [tNew,rNew,xNew'];
        N = N + 1;
        h(N - 1) = hNew;
        hNew = perc(2)*hNew;
        x(N,:) = xNew;
        xDot(N,:) = f(tNew,xNew);
        t(N) = tNew;
        tNew = tNew + hNew;
        errors(N) = controlErrors(end);
        controlErrors = circshift(controlErrors,-1,2);
        rNew = perc(2);
        rejected = false;
        bypass = bypass + 1;
        %We save the polynomial coefficients, in case the next step is
        %rejected and we need them to recalculate the predictor.
        polCoeffOld = polCoeff;
        newtonIters(N) = iters;

    %The step size was in the allowed range.
    else
        %The step is accepted, so we save the new calculated values and
        %increment the number of steps taken.
        N = N + 1;
        h(N - 1) = hNew;
        hNew = rNew*hNew;
        x(N,:) = xNew;
        xDot(N,:) = f(tNew,xNew);
        t(N) = tNew;
        tNew = tNew + hNew;
        errors(N) = controlErrors(end);
        controlErrors = circshift(controlErrors,-1,2);
        rejected = false;
        bypass = bypass + 1;
        %We save the polynomial coefficients, in case the next step is
        %rejected and we need them to recalculate the predictor.
        polCoeffOld = polCoeff;
        newtonIters(N) = iters;
    end
    %We calculate the predictor for the next step
    %If N==k it means that the first step has been rejected, due to how the
    %polynomial was constructed we then need to do use hNew instead of
    %hOld+hNew to predict the next step
    if(exist('analyticSol','var') == 0)
        if N == k
            for i=1:numUnknowns
                xPred(i) = polyval(polCoeffOld(:,i),hNew);
            end
        else
            for i=1:numUnknowns
                xPred(i) = polyval(polCoeffOld(:,i),h(N-1)+hNew);
            end
        end
    else
        xPred = analyticSol(xNew,hNew);
    end
end

% --------------------------------------------------------------------------
% Calculation of the last point (we want it to hit the end point)
% --------------------------------------------------------------------------
hNew = tSpan(2) - t(N-1);
if(interpolation)
    for i=1:numUnknowns
        xNew(i) = polyval(polCoeff(:,i),hNew);
    end
else
    [polCoeff,iters] = polIp(f, jac, getThetas(orders(2)),[h(1:N-2) hNew],...
    x(1:N-1,:), xDot(1:N-1,:),tSpan(2),polCoeffOld, TOL);
    for i=1:numUnknowns
        xNew(i) = polyval(polCoeff(:,i),hNew);
    end
end
t(N) = tSpan(2);
x(N,:) = xNew;
xDot(N,:) = f(tSpan(2),xNew);
newtonIters(N) = iters;

cpuTime = toc;

%Create the rejectedSteps_at-struct and the nbrStepsRTooBig_at-struct
if rejectedSteps ~= 0
    rejectedSteps_at = struct('t',rejectedSteps_at(:,1),'r',rejectedSteps_at(:,2),'x',rejectedSteps_at(:,3:end));
else
    rejectedSteps_at = [];
end

if nbrStepsRTooBig ~= 0
    nbrStepsRTooBig_at = struct('t',nbrStepsRTooBig_at(:,1),'r',nbrStepsRTooBig_at(:,2),'x',nbrStepsRTooBig_at(:,3:end));
else
    nbrStepsRTooBig_at = [];
end

%t should be a column vector!
[m,n] = size(t);
if(n > 1)
    t = t';
end

%Package statitistics in struct
statistics = struct('solver','pmmip',...
    'methodName',methodName,...
    'methodOrder',order,...
    'kStepMethod',k,...
    'filterName',filterName,...
    'tol',TOL,...
    'perc',perc,...
    'relativeError',relerr,...
    'errorPerUnitStep',unit,...
    'usebypass',usebypass,...
    'predFirstStep',predFirstStep,...
    'normFunc',func2str(normFunc),...
    'cpuTime',cpuTime,...
    'jacProvided', providedJacobian, ...
    'interpolationMode',interpolation,...
    'numberOfSteps',N,...
    'rejectedSteps',rejectedSteps,...
    'rejectedStepsData',rejectedSteps_at,...
    'nbrStepsRTooBig',nbrStepsRTooBig,...
    'nbrStepsRTooBigData',nbrStepsRTooBig_at,...
    'errorEstimate',errors,...
    'newtonIters',newtonIters);
end
