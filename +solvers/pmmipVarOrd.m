function [t,x,statistics] = pmmipVarOrd(f,tSpan,x0,varargin)
%Solve non-stiff differential equations, k-step implicit multistep method of
%order k+1.
%
%Integrates the system of differential equations dx/dt = f(t,x) from time
%tSpan(1) to time tSpan(2). The solver has variable step size.
%
%:Example:
%
%>>> f = @(t,x) [3*x(2);-2*x(1)];
%>>> x0 = [1;2];
%>>> tSpan = [0,1];
%>>> [t,x,statistics] =...
%   pmmip(f,tSpan,x0,'methodName',am_4-10,'tol',1e-6,'perc',[1,1]);
%
%Parameters
%----------
%f : function handle
%   Function handle to the right hand side function for the system of
%   differential equations. For a scalar t and a vector x it must return a
%   column vector corresponding to f(t,x).
%
%tSpan : vector
%   [T0, TFINAL] with initial and final time for the
%   solution
%
%x0 : vector
%   Initial conditions x(0) = x0, in the form of a column vector
%
%varargin : options list
%   Options replacing default values on the form of strings
%   with corresponding value (see example above). The following options are
%   available:
%
%   'jac'
%       A function handle (taking the parameters (t,x)) to a function which
%       outputs the Jacobian of the RHS function. If none is provided the
%       Jacobian will be calculated numerically using finite differences.
%   'method'
%      The name of the method to be used followed by the lower and upper
%      order limit on the form: name_lowerOrder-upperOrder. For example, to
%      get Adams-Moulton 4-12 input 'am_4-12'. The default method used is
%      am_4-12. The following k-step methods are available:
%
%           - am (Adams-Moulton) Lower order limit: 3 Upper order limit:
%           none
%   'filter'
%       The name of the filter used to control the step size. Default
%       'PI3333'. The following filters are available:
%
%           - H211PI
%           - H2111b
%           - PI3333
%           - PI3040
%           - PI4020
%           - H312b
%           - PID
%           - josefine1
%           - josefine2
%
%   'filter'
%       A filter vector, which if supplied overrides the predefined filter.
%       THIS OPTION SHOULD ONLY BE USED IF YOU KNOW WHAT YOU ARE DOING!
%   'tol'
%      Tolerance, default 1e-4
%   'perc'
%      gives min and max values allowed for step size ratios. Set to
%      [1,1] for fixed step size, default [0.8,1.2]
%   'relerr'
%      allows user to use only absolute error control by setting to false,
%      default true
%   'unit'
%      allows the use of error per unit step. To allow this option, set
%      value to true, default false
%   'initialStep'
%      default is computed using "autostart". May be overridded by assigning
%      any value.
%   'initialOrder'
%      The initial order to use. Default is to use the lowest order
%      available.
%   'norm'
%      The norm used to calculate the error (used when going from a vector
%      with all error components to a scalar). Default is the inf-norm. To use
%      another norm supply a function handle to the other norm function.
%
%Returns
%-------
%t : vector
%   All time points
%
%x : vector
%   The solution values in all points
%
%statistics : struct
%   A struct containing different quantities of statistical interest. It
%   contains the following fields:
%
%   'methodName'
%      The name of the method used
%   'initialOrder'
%      The initial order of the method used
%   'filterName'
%      The name of the filter used
%   'tol'
%      The tolerance used
%   'perc'
%      The minimum and maximum step size ratio used
%   'relativeError'
%      Wether or not the error estimates were calculated as relative or
%      absolute errors.
%   'errorPerUnitStep'
%      Wether or not the error estimates were calculated as error per unit
%      step
%   'normFunc'
%      The name of norm function
%   'cpuTime'
%      The amount of CPU time which passed during the integration
%   'numberOfSteps'
%      The number of (accepted) steps taken
%   'rejectedSteps'
%      The number of rejected steps
%   'rejectedStepsData'
%      A struct with labels 't','r' and 'x' containing the time points, step
%      size ratios and calculated values for all points where a step was
%      rejected.
%   'nbrStepsRTooBig'
%      The number of steps where the suggested step size ratio was larger
%      than allowed (these steps are not rejected, but the actual step size
%      ratio used is modified).
%   'nbrStepsRTooBigData'
%      A struct with labels 't','r' and 'x' containing the time points, step
%      size ratios and calculated values for all points where the suggested
%      step size ratio was too large.
%   'errorEstimate'
%      A vector containing the calculated error estimates in each point
%
%Raises
%------
%'Solver:NoSolution'
%   If the step size becomes to small and the solver is unable to find a
%   solution.
%
%'Solver:NoSuchOption'
%   If an option specified by the user does not exist.

import init.autostart;
import init.start;
import control.errorController;
import control.getWorkFactors;
import control.getFilterVec;
import solverFunctions.polE;
import solverFunctions.polIp;
import solverFunctions.getJac;
import solverFunctions.getMethodThetaFunc;

tic
%===========================================================================
% Initialization of variables and starting paramaters
%===========================================================================
%---------------------------------------------------------------------------
%Output variables
%---------------------------------------------------------------------------
%Time points
t(1) = tSpan(1);
%Solution points
x(1,:) = x0;

%---------------------------------------------------------------------------
%Variables used to keep track of different statistics
%---------------------------------------------------------------------------
%Number of rejected steps
rejectedSteps = 0;
%The estimated error in each point
errors = [];
%Whenever a step size ratio is too small, we add info about the step here.
rejectedSteps_at = [];
%counts the number of times the controller recommended a step size which was
%too large
nbrStepsRTooBig = 0;
%Whenever a step size ratio is too big, we add info about the step here.
nbrStepsRTooBig_at = [];
%Which order was used to calculate the value in a point
orderstats = [];
%Wether or not the user provided a Jacobian function. Default is false but
%will be set to true if a Jacobian is provided, when the options are parsed
providedJacobian = false;

%---------------------------------------------------------------------------
%Internal parameters
%---------------------------------------------------------------------------
%Derivative of solution points
xDot(1,:) = f(t(1),x(1,:)');
%Flag indicating if the last step was rejected
rejected = false;
%Number of unknowns in the system to be solved
numUnknowns = length(x0);
%The step size sequences for all three orders used, each column will contain
%the step size sequence for an order.
steps = [];
%The new step size ratio. Each element contains the new step size ratio for
%a different order
rNew = [];
%errors fed to the controller, each column will contain the error sequence
%for an order.
controlErrors = [];
%the latest calculated error (without scaling)
newErrors = [];
%indicates if the lowest possible order has been reached
pmin = false;
%indicates if the highest possible order has been reached
pmax = false;
%Cell containing the matrices with the polynomial coefficients, each cell
%element will contain a matrix with the polynomial coefficients (one column
%in this matrix corresponds to the polynomial used to calculate the solution
%of one unknown in the system of equations to be solved)
polCoeff = {};
%Cell containing the matrices with the previously used polynomial
%coefficients. These are used in case a step is rejected and the predictor
%has to be recalculated.
polCoeffOld = {};
%Cell containing the matrices with the polynomial coefficients which are
%used as an initial guess to start the Newton iterations.
polCoeffGuess = {};
%Indicates how many times a certain order has been bypassed when calculating
%the new step size ratio. When these counters has reached the needed amount
%of previous error estimates the higher order step size controller is used.
%The reason for the last element is -1 is that in the first step the higher
%order method is unavailable and therefore it takes one extra step to reach
%the needed amount of error estimates.
bypass = [0,0,0];
%Continuous order increment. This is used to decide when the order is to be
%changed.
dp = 0;

%---------------------------------------------------------------------------
%Default settings
%---------------------------------------------------------------------------
TOL = 1e-4; %default tolerance
methodName = 'am_4-12'; %name of the method used
filterName = 'PI3333'; %default filter
%The function calculating the Jacobian. Uses numerical calculations as
%default
jac = @(t,x) getJac(f,t,x);
%default min/max step size ratio
perc = [0.8,1.2]; %default min/max step size ratio
%function used to calculate the norm of the errors, default is the 1-norm
normFunc = @(x) norm(x,inf); %norm used to calculate error estimate
%if error is to be calculated as the relative error
relerr = true; %flag to indicate if relative error is to be used
%if error per unit step is to be used
unit = false; %flag to indicate if error per unit step is to be used

%===========================================================================
%Check for user defined options which override the default parameters
%===========================================================================
for i = 1:2:length(varargin)
    name = varargin{i};
    value = varargin{i+1};
    switch lower(name)
        case 'tol'
            TOL = value;
        case 'method'
            methodName = value;
        case 'filtername'
            filterName = value;
        case 'filtervec'
            filterVec = value;
        case 'perc'
            perc = value;
        case 'relerr'
            relerr = value;
        case 'unit'
            unit= value;
        case 'initialstep'
            h(1) = value;
        case 'norm'
            normFunc = value;
        case 'initialorder'
            orders = [value - 1, value, value + 1];
        case 'jac'
            jac = value;
            providedJacobian = true;
        otherwise
            ME = MException('Solver:NoSuchOption',...
                sprintf('The option "%s" does not exist.',name));
            throw(ME);
    end
end

%===========================================================================
%Calculate and set parameters which need the different options to be set
%===========================================================================
%The getThetas function returns the theta parameter vector correspnding to a
%certain order
getThetas = getMethodThetaFunc(methodName);
%---------------------------------------------------------------------------
%Determine the initial order
%---------------------------------------------------------------------------
%Check if initial order was set by user, otherwise find the lowest available
%order and set this as the initial order
i = 1;
while exist('orders') == 0
    orderAvailable = true;
    try
        getThetas(i);
    catch ME
        if (strcmp(ME.identifier,'Thetas:NoMethod'))
            orderAvailable = false;
        else
            rethrow(ME)
        end
    end
    if orderAvailable
        orders = [i - 1, i, i + 1];
    end
    i = i + 1;
end

initialOrder = orders(2);
%Check to see that the initial order does actually exist
try
    getThetas(initialOrder);
catch ME
    if (strcmp(ME.identifier,'Thetas:NoMethod'))
        ME = MException('Solver:InitialOrderDoesNotExist',...
                sprintf('The initial order %d does not exist.',...
                initialOrder));
            throw(ME);
    else
        rethrow(ME);
    end
end


%---------------------------------------------------------------------------
%Determine the filter to be used
%---------------------------------------------------------------------------

if (exist('filterVec','var'))
    filter = filterVec;
else
    filter = getFilterVec(filterName);
end
%Calculate the digital filter order (how many errors we need to feed the
%controller)
PD = floor(length(filter)/2) + 1;
%Initialize the control error vector to the correct size
controlErrors = ones(PD,3);

%---------------------------------------------------------------------------
%Calculate the first k-points which are needed to start the solver
%---------------------------------------------------------------------------
%The number of steps used in the initial method (k-step method)
k0 = initialOrder - 1;
%Check if user set initial step size, otherwise use autostart to calculate
%it
if exist('h') == 0
    h(1) = autostart(f, tSpan, x0, TOL, initialOrder);
end

%Calculate k-1 additional points so that we can start using the k-step
%method
[h,t,x,xDot] = start(f,h,t,x,xDot,TOL,k0);
tNew = t(end) + h(1);
%Set how many solution points we have now
N = k0;
%Initialize step sizes for all three orders
steps = ones(PD,3)*h(1);

%Assume that all initial points are exact
errors = zeros(1,N);

%No order specified in the beginning, set it to 0
orderstats = zeros(1,N);


%---------------------------------------------------------------------------
%Calculate the predictor used to estimate the error in the first step for
%the lower order methd
%---------------------------------------------------------------------------

%First we get the coefficients for the lower order method, and we check if
%it exists. If it does not, then we are already at the lowest possible
%order.
try
    thetasTmp = getThetas(orders(1));
catch ME
    if (strcmp(ME.identifier,'Thetas:NoMethod'))
        pmin = true;
    else
        rethrow(ME);
    end
end
if ~pmin
    %First we use AB of order k0-2 to construct a polynomial
    thetasAb = ones(1,k0 - 2)*pi/2;
    polCoeffTmp = polE(thetasAb,h(1:end-1),x(1:end-1,:),xDot(1:end-1,:));
    %We then add a higher order coefficient and set it to 0, thereby
    %extending the polynomial to a higher degree. This polynomial is then
    %used as an initial guess for the Newton iterations used to construct
    %the prediction polynomial for the lower order method
    polCoeffTmp = [zeros(1,numUnknowns);polCoeffTmp];
    polCoeffOld{1} = polIp(f, jac, getThetas(orders(1)),h, x(1:end-1,:),...
        xDot(1:end-1,:),t(end),polCoeffTmp,TOL);
    %The prediction polynomial is extended in the same way, and can now be
    %used as an initial guess for the current order method
    polCoeffOld{2} = [zeros(1,numUnknowns);polCoeffOld{1}];
    %We initialize the old coefficients for the higher order polynomial as
    %well, although this does not have any significance, they are only set
    %to avoid errors
    polCoeffOld{3} = [zeros(1,numUnknowns);polCoeffOld{2}];
else
    %First we use AB of order k0-1 to construct a polynomial
    thetasAb = ones(1,k0 - 1)*pi/2;
    %We then add a higher order coefficient and set it to 0, thereby
    %extending the polynomial to a higher degree. This polynomial is then
    %used as an initial guess for the Newton iterations used to construct
    polCoeffOld{2} = polE(thetasAb,h,x,xDot);
    polCoeffOld{2} = [zeros(1,numUnknowns);polCoeffOld{2}];
    %We initialize the old coefficients for the lower and higher order
    %polynomial as well, although this does not have any significance, they
    %are only set to avoid errors
    polCoeffOld{1} = polCoeffOld{2}(2:end,:);
    polCoeffOld{3} = [zeros(1,numUnknowns);polCoeffOld{2}];
end

for i=1:3
    for j=1:numUnknowns
        xPred(j,i) = polyval(polCoeffOld{i}(:,j),steps(end,2));
    end
end

%Initialize the polynomial coefficients for all orders, only used so that
%no exception is thrown if we are unable to calculate the coefficients in
%the beginning
polCoeff = polCoeffOld;
%Set the new step size ratio to 1 for all orders.
rNew(1:3) = 1;

%===========================================================================
% Main loop used to calculate the solution
%===========================================================================
while t(N) < tSpan(2)
    if steps(end,2)/(tSpan(2)-tSpan(1)) < 1e-16
        ME = MException('Solver:NoSolution',['Too small step',...
            'size reached at t=%d. Try increasing TOL.'],tNew);
        throw(ME);
    end
%===========================================================================
% Calculation of the solution and error estimate
%===========================================================================
    %Calculate new solution
    %If the step was not rejected calculate new coefficients, otherwise just
    %reuse the already existing coefficients
    for i=1:3
        %Do not calculate coefficients if we haven't generated enough
        %points yet (only applicable to the higher order)
        if i <= 2 || N >= k0 + 1
            try
                polCoeff{i} = polIp(f, jac, getThetas(orders(i)),...
                    [h steps(end,2)], x, xDot,tNew,polCoeffOld{i},...
                    TOL);
            catch ME
                if (strcmp(ME.identifier,'Thetas:NoMethod'))
                    %Check for which order the method was unavailable,
                    %if it is current order something is wrong
                    switch i
                        case 1
                            pmin = true;
                        case 2
                            rethrow(ME);
                        case 3
                            pmax = true;
                    end
                else
                    rethrow(ME);
                end
            end
            %Check to see that the calculations of the coefficients did
            %produce real numbers
            if(~isfinite(sum(polCoeff{i})))
                ME = MException('Solver:NoSolution',['polCoeff are',...
                    'not finite. t = %d'],t(end));
                throw(ME);
            end
        end
    end

    %Calculate the new solution for each order. This loop should always
    %evaluate, because there should always be coefficients for all orders
    %even if an order is unavailable. If this is the case the incorrect
    %results from this loop (and results derived from it) will not be used,
    %because the error estimates will either be bypassed, or in the case of
    %the order control pmax/pmin will prevent the results from influencing
    %the decision to change order.
    for i=1:3
        %Evaluate the polynomials in the new point for each unknown
        for j=1:numUnknowns
            xNew(j,i) = polyval(polCoeff{i}(:,j),steps(end,2));
        end
    end

    %Estimate the unscaled new error
    for i=1:3
        newErrors(:,i) = xPred(:,i) - xNew(:,i);
    end

    %Apply anti-windup
    if unit
        newErrors = newErrors*diag((steps(end,:)/steps(end,2)).^(orders));
    else
        newErrors = newErrors*diag((steps(end,:)/steps(end,2)).^(orders+1));
    end
    for i=1:3
        if relerr == 0
            controlErrors(end,i) = normFunc(newErrors(:,i))/(TOL);
        else
            controlErrors(end,i) = normFunc(newErrors(:,i))/...
                (normFunc(abs(xNew(:,i))*TOL+1e-16));
        end
    end

%===========================================================================
% Step size control
%===========================================================================
    %Feed data to controller
    for i=1:3
        %If we do not have enough previous values, bypass the controller
        %and set the new step size ratio to the scaled control error
        if bypass(i) < PD - 1
            rNew(i) = controlErrors(end,i)^-(1/orders(i));
            %Only increase the bypass counter if the step has not been
            %rejected (otherwise we would just recount the same step over
            %and over)
            if ~rejected
                bypass(i) = bypass(i) + 1;
            end
        else
            rNew(i) = errorController(filter, orders(i),...
                controlErrors(:,i)',steps(:,i)', unit);
        end
    end
    %If this is the first step we do not want to enable step size control,
    %so we override the values set by the controller, and reset the bypass
    %variables to 0
    if N==k0
        rNew(2:3) = 1;
        bypass(2:3) = 0;
    %If this is the second step we ignore the result from the controller for
    %the higher order method because it was calculated using incorrect error
    %estimates
    elseif N==k0+1
        rNew(3) = 1;
        bypass(3) = 0;
    end

%===========================================================================
% Step size rejection/acceptation
%===========================================================================
    %If the suggested step size is too small, then the error in the newly
    %calculated step is too large, and we reject this step.
    if rNew(2) < perc(1)
        rejectedSteps_at(end + 1,:) = [tNew,rNew(2),xNew(:,2)'];
        tNew = tNew - steps(end,2);
        if ~rejected
            steps(end,2) = h(end)*perc(1);
        else
            steps(end,2) = steps(end,2)*0.95;
        end
        rNew(2) = steps(end,2)/h(end);
        rejected = true;
        rejectedSteps = rejectedSteps + 1;
        %Because the control sequence is now destroyed, we bypass all orders
        bypass(:) = 0;
    else
        %If the suggested step size is too large and we use the maximum step
        %size instead.
        if rNew(2) > perc(2)
            nbrStepsRTooBig_at(end + 1,:) = [tNew,rNew(2),xNew(:,2)'];
            nbrStepsRTooBig = nbrStepsRTooBig + 1;
            rNew(2) = perc(2);
        end
        %The step is accepted, so we save the new calculated values and
        %increment the number of steps taken.
        N = N + 1;
        h(N - 1) = steps(end,2);
        %Shift the step size sequence, and calc new step size
        steps(:,2) = circshift(steps(:,2),-1,1);
        steps(end,2) = rNew(2)*steps(end-1,2);
        x(N,:) = xNew(:,2);
        xDot(N,:) = f(tNew,xNew(:,2));
        t(N,1) = tNew;
        errors(N) = controlErrors(end);
        orderstats(N) = orders(2);

        %Shift control error sequences
        for i=1:3
            controlErrors(:,i) = circshift(controlErrors(:,i),-1,1);
        end
        rejected = false;
        polCoeffOld = polCoeff;
        %If this is the first accepted step we do not have old coefficients
        %for the higher order which we can use as a guess for our Newton
        %iterations, so we need to deal with this as a special case
        if N == k0 + 1
            %For for our first guess for the coefficients for the higher
            %degree polynomial we take the polynomial of for the current
            %order and use this as our guess (and guessing that the leading
            %coeffiecients are 0).
            polCoeffOld{3} =[zeros(1,numUnknowns);polCoeffOld{2}];
        end
    end

%===========================================================================
% Order control
%===========================================================================
    %We do not advance the order control mechanism if the step was rejected.
    %If this is the first or second step (after the initialization of the
    %solver) we do not run the order control part, because we lack
    %solutions/predictions for the higher order solver. Also, if this step
    %is the last one (which has passed the end point) then we do not change
    %order, because we want to use the same order when we adjust the step so
    %it hits the end point.
    if ~rejected && N >= k0 + 3 && t(N) < tSpan(2)
        %We limit the step size ratio upwards, as to not overestimate the
        %benifits of changing the order. We also calculate the new step
        %sizes for the higher and lower order
        for i=[1,3]
            rNew(i) = min(rNew(i), perc(2));
            steps(:,i) = circshift(steps(:,i),-1,1);
            steps(end,i) = rNew(i)*steps(end-1,i);
        end

        workFactors = getWorkFactors(orders);
        %Calculate the relative efficiency for the lower and higher order
        lowEff = steps(end,1)/steps(end,2)/(workFactors(1)/workFactors(2));
        highEff = steps(end,3)/steps(end,2)/(workFactors(3)/workFactors(2));

        %Calculate the continuous order increments
        dpp = max(0, 4*(highEff - 1)/(2*highEff + 2));
        dpm = min(0, 4*(1 - lowEff)/(2*lowEff + 2));
        if (highEff - 1)*(lowEff - 1) < 0
            dppm = (highEff - lowEff)/(highEff + lowEff);
        else
            dppm = 0;
        end

        %If we have reached the lowest or highest possible order, modify the
        %contisuous order increments as to not get stuck on this order or
        %try to change to an order that does not exist
        if pmax
            dpp = 0;
            dppm = 0;
        end
        if pmin
            dpm = 0;
            dppm = 0;
        end

        %Integrate the continuous order increments
        dp = dp + dpp + dpm + dppm;

        %Calculate the discrete order increment which is used to check if
        %there is to be an order change
        increment = round(dp);

        if increment > 0
            increment = 1;
        elseif increment < 0
            increment = -1;
        end

        %Check if the order should be chacnged.
        if increment > 0 && rNew(3) > perc(1) && highEff > 1.1
            %Change the order
            orders = orders + increment;
            %Reset the continuous order change variable
            dp = 0;
            %Shift the step size sequences and the coefficient guesses, and
            %the old coefficients
            for i=1:2
                steps(:,i) = steps(:,i+1);
                polCoeffOld{i} = polCoeffOld{i+1};
                bypass(i) = bypass(i+1);
            end
            %We do not have previous coefficients for the higher order so
            %we do the same as in the beginning and use the corresponding
            %explicit method to calculate new coefficients
            try
                explThetas = getThetas(orders(3));
                explThetas = [explThetas, pi/2];
                polCoeffOld{3} = polE(explThetas,h,x,xDot);               
                for i=1:numUnknowns
                    polCoeffOld{3}(:,i) = solverFunctions.polTrans(...
                        polCoeffOld{3}(:,i),-h(N-1));
                end
            catch ME
                if (strcmp(ME.identifier,'Thetas:NoMethod'))
                    pmax = true;
                else
                    rethrow(ME);
                end
            end
            %if we increase the order we can no longer have minimum order,
            %so set this to false
            pmin = false;
            %we do not have a previous step size sequence for the higher
            %order, so we need to bypass this at first
            bypass(3) = 0;
        elseif increment < 0 && rNew(1) > perc(1) && lowEff > 1.1
            %Change the order
            orders = orders + increment;
            %Reset the continuous order change variable
            dp = 0;
            %Shift the step size sequences and the coefficient guesses, and
            %the old coefficients
            for i=3:-1:2
                steps(:,i) = steps(:,i-1);
                polCoeffOld{i} = polCoeffOld{i-1};
                bypass(i) = bypass(i-1);
            end
            %We do not have previous coefficients for the higher order so
            %we do the same as in the beginning and use the corresponding
            %explicit method to calculate new coefficients
            try
                explThetas = getThetas(orders(1));
                explThetas = [explThetas, pi/2];
                polCoeffOld{1} = polE(explThetas,h,x,xDot);
                for i=1:numUnknowns
                    polCoeffOld{1}(:,i) = solverFunctions.polTrans(...
                        polCoeffOld{1}(:,i),-h(N-1));
                end
            catch ME
                if (strcmp(ME.identifier,'Thetas:NoMethod'))
                    pmax = true;
                else
                    rethrow(ME);
                end
            end
            %if we decrease the order we can no longer have maximum order,
            %so set this to false
            pmax = false;
            %we do not have a previous step size sequence for the lower
            %order, so we need to bypass this at first
            bypass(1) = 0;
        end
    %If this is the step before we are going to enable order control and
    %step size control set the step size sequences for the higher order to
    %be the same as the current order and enable bypass for the higher order
    %method
    elseif N == k0 + 2
        steps(:,3) = steps(:,2);
        bypass(3) = 0;
    end
    %We recalculate the predictor for the next step.
    %If N==k it means that the first step has been rejected, due to how
    %the polynomial was constructed we then need to do use hNew instead
    %of hOld+hNew to predict the next step
    tNew = tNew + steps(end,2);
    for i=1:3
        for j=1:numUnknowns
            xPred(j,i) = polyval(polCoeffOld{i}(:,j),steps(end,2)...
                +h(N-1));
        end
    end
end

%%Calculate the step size from the second to last point so it hits the end
%%point
hNew = tSpan(2) - t(N-1);
% %Use the last calculated polynomial but with the step size which hits the
% %end point.
h(N-1) = hNew;
polCoeff = polIp(f, jac, getThetas(orders(2)),h, x(1:N-1,:),...
     xDot(1:N-1,:),tSpan(2),polCoeffOld{2}, TOL);
% %Over write the value which overshot the end point
for i=1:numUnknowns
        xNew(i,2) = polyval(polCoeff(:,i),hNew);
end
t(N) = tSpan(2);
x(N,:) = xNew(:,2);
xDot(N,:) = f(tSpan(2),xNew(:,2));

cpuTime = toc;
%Create the rejectedSteps_at-struct and the nbrStepsRTooBig_at-struct
if rejectedSteps ~= 0
    rejectedSteps_at = struct('t',rejectedSteps_at(:,1),'r',...
        rejectedSteps_at(:,2),'x',rejectedSteps_at(:,3:end));
else
    rejectedSteps_at = [];
end

if nbrStepsRTooBig ~= 0
    nbrStepsRTooBig_at = struct('t',nbrStepsRTooBig_at(:,1),'r',...
        nbrStepsRTooBig_at(:,2),'x',nbrStepsRTooBig_at(:,3:end));
else
    nbrStepsRTooBig_at = [];
end
%Package statitistics in struct
statistics = struct('solver','pmmipVarOrd',...
    'methodName',methodName,...
    'initialOrder',initialOrder,...
    'orders',orderstats,...
    'filterName',filterName,...
    'tol',TOL,...
    'perc',perc,...
    'relativeError',relerr,...
    'jacProvided', providedJacobian, ...
    'errorPerUnitStep',unit,...
    'normFunc',func2str(normFunc),...
    'cpuTime',cpuTime,...
    'numberOfSteps',N,...
    'rejectedSteps',rejectedSteps,...
    'rejectedStepsData',rejectedSteps_at,...
    'nbrStepsRTooBig',nbrStepsRTooBig,...
    'nbrStepsRTooBigData',nbrStepsRTooBig_at,...
    'errorEstimate',errors);
end
