function [t,x,statistics] = pmmeVarOrd(f,tSpan,x0,varargin)
%Solve non-stiff differential equations, k-step explicit multistep method of
%order k.
%
%Integrates the system of differential equations dx/dt = f(t,x) from time
%tSpan(1) to time tSpan(2). The solver has variable step size.
%
%:Example:
%
%>>> f = @(t,x) [3*x(2);-2*x(1)];
%>>> x0 = [1;2];
%>>> tSpan = [0,1];
%>>> [t,x,statistics] =...
%   pmmeVarOrd(f,tSpan,x0,'method','ab_2-8','tol',1e-6,'perc',[1,1]);
%
%Parameters
%----------
%f : function handle
%   Function handle to the right hand side function for the system of
%   differential equations. For a scalar t and a vector x it must return a
%   column vector corresponding to f(t,x).
%
%tSpan : vector
%   [T0, TFINAL] with initial and final time for the
%   solution
%
%x0 : vector
%   Initial conditions x(0) = x0, in the form of a column vector
%
%varargin : options list
%   Options replacing default values on the form of strings
%   with corresponding value (see example above). The following options are
%   available:
%
%   'method'
%      The name of the method to be used followed by the lower and upper
%      order limit on the form: name_lowerOrder-upperOrder. For example, to
%      get Adams-Bashforth 3-12 input 'ab_3-12'. The default method used is
%      ab_2-12. The following k-step methods are available:
%
%           - ab (Adams-Bashforth) Lower order limit: 1 Upper order
%           limit: none
%           - edf Lower order limit: 1 Upper order limit: none
%           - nystrom Lower order limit: 3 Upper order limit: 5
%
%   'filterName'
%       The name of the filter used to control the step size. Default
%       'PI3333'. The following filters are available:
%
%           - H211PI
%           - H2111b
%           - PI3333
%           - PI3040
%           - PI4020
%           - H312b
%           - PID
%           - josefine1
%           - josefine2
%
%   'filtervec'
%       A filter vector, which if supplied overrides the predefined filter.
%       THIS OPTION SHOULD ONLY BE USED IF YOU KNOW WHAT YOU ARE DOING!
%   'tol'
%      Tolerance, default 1e-4
%   'perc'
%      gives min and max values allowed for step size ratios. Set to
%      [1,1] for fixed step size, default [0.8,1.2]
%   'relerr'
%      allows user to use only absolute error control by setting to false,
%      default true
%   'unit'
%      allows the use of error per unit step. To allow this option, set
%      value to true, default false
%   'initialStep'
%      default is computed using "autostart". May be overridded by assigning
%      any value.
%   'initialOrder'
%      The initial order to use. Default is to use the lowest order
%      available.
%   'norm'
%      The norm used to calculate the error (used when going from a vector
%      with all error components to a scalar). Default is the inf-norm. To use
%      another norm supply a function handle to the other norm function.
%
%Returns
%-------
%t : vector
%   All time points
%
%x : vector
%   The solution values in all points
%
%statistics : struct
%   A struct containing different quantities of statistical interest. It
%   contains the following fields:
%
%   'methodName'
%      The name of the method used
%   'initialOrder'
%      The initial order of the method used
%   'filterName'
%      The name of the filter used
%   'tol'
%      The tolerance used
%   'perc'
%      The minimum and maximum step size ratio used
%   'relativeError'
%      Wether or not the error estimates were calculated as relative or
%      absolute errors.
%   'errorPerUnitStep'
%      Wether or not the error estimates were calculated as error per unit
%      step
%   'normFunc'
%      The name of norm function
%   'cpuTime'
%      The amount of CPU time which passed during the integration
%   'numberOfSteps'
%      The number of (accepted) steps taken
%   'rejectedSteps'
%      The number of rejected steps
%   'rejectedStepsData'
%      A struct with labels 't','r' and 'x' containing the time points, step
%      size ratios and calculated values for all points where a step was
%      rejected.
%   'nbrStepsRTooBig'
%      The number of steps where the suggested step size ratio was larger
%      than allowed (these steps are not rejected, but the actual step size
%      ratio used is modified).
%   'nbrStepsRTooBigData'
%      A struct with labels 't','r' and 'x' containing the time points, step
%      size ratios and calculated values for all points where the suggested
%      step size ratio was too large.
%   'errorEstimate'
%      A vector containing the calculated error estimates in each point
%
%Raises
%------
%'Solver:NoSolution'
%   If the step size becomes to small and the solver is unable to find a
%   solution.
%
%'Solver:NoSuchOption'
%   If an option specified by the user does not exist.

import init.autostart;
import init.start;
import control.errorController;
import solverFunctions.polE;
import solverFunctions.polI;
import solverFunctions.getJac;
import control.getWorkFactors;
import control.getFilterVec;
import solverFunctions.getMethodThetaFunc;
tic

%===========================================================================
% Initialization of variables and starting paramaters
%===========================================================================
%---------------------------------------------------------------------------
%Output variables
%---------------------------------------------------------------------------
%Time points
t(1) = tSpan(1);
%Solution points
x(1,:) = x0;

%---------------------------------------------------------------------------
%Variables used to keep track of different statistics
%---------------------------------------------------------------------------
%Number of rejected steps
rejectedSteps = 0;
%The estimated error in each point
errors = [];
%Whenever a step size ratio is too small, we add info about the step here.
rejectedSteps_at = [];
%counts the number of times the controller recommended a step size which was
%too large
nbrStepsRTooBig = 0;
%Whenever a step size ratio is too big, we add info about the step here.
nbrStepsRTooBig_at = [];
%Which order was used to calculate the value in a point
orderstats = [];

%---------------------------------------------------------------------------
%Internal parameters
%---------------------------------------------------------------------------
%Derivative of solution points
xDot(1,:) = f(t(1),x(1,:)');
%Flag indicating if the last step was rejected
rejected = false;
%Number of unknowns in the system to be solved
numUnknowns = length(x0);
%The step size sequences for all three orders used, each column will contain
%the step size sequence for an order.
steps = [];
%The new step size ratio. Each element contains the new step size ratio for
%a different order
rNew = [];
%errors fed to the controller, each column will contain the error sequence
%for an order.
controlErrors = [];
%the latest calculated error (without scaling)
newErrors = [];
%indicates if the lowest possible order has been reached
pmin = false;
%indicates if the highest possible order has been reached
pmax = false;
%Cell containing the matrices with the polynomial coefficients, each cell
%element will contain a matrix with the polynomial coefficients (one column
%in this matrix corresponds to the polynomial used to calculate the solution
%of one unknown in the system of equations to be solved)
polCoeff = {};
%Cell containing the matrices with the previously used polynomial
%coefficients. These are used in case a step is rejected and the predictor
%has to be recalculated.
polCoeffOld = {};
%Indicates how many times a certain order has been bypassed when calculating
%the new step size ratio. When these counters has reached the needed amount
%of previous error estimates the higher order step size controller is used.
%The reason for the last element is -1 is that in the first step the higher
%order method is unavailable and therefore it takes one extra step to reach
%the needed amount of error estimates.
bypass = [0,0,0];
%Continuous order increment. This is used to decide when the order is to be
%changed.
dp = 0;

%---------------------------------------------------------------------------
%Default settings
%---------------------------------------------------------------------------
TOL = 1e-4;
methodName = 'ab_2-12';
filterName = 'PI3333';
%default min/max step size ratio
perc = [0.8,1.2];
%function used to calculate the norm of the errors, default is the inf-norm
normFunc = @(x) norm(x,inf);
%if error is to be calculated as the relative error
relerr = true;
%if error per unit step is to be used
unit = false;


%===========================================================================
%Check for user defined options which override the default parameters
%===========================================================================
for i = 1:2:length(varargin)
    name = varargin{i};
    value = varargin{i+1};
    switch lower(name)
        case 'tol'
            TOL = value;
        case 'method'
            methodName = value;
        case 'filtername'
            filterName = value;
        case 'filtervec'
            filterVec = value;
        case 'perc'
            perc = value;
        case 'relerr'
            relerr = value;
        case 'unit'
            unit= value;
        case 'initialstep'
            h(1) = value;
        case 'norm'
            normFunc = value;
        case 'initialorder'
            orders = [value - 1, value, value + 1];
        otherwise
            ME = MException('Solver:NoSuchOption',...
                sprintf('The option "%s" does not exist.',name));
            throw(ME);
    end
end
%===========================================================================
%Calculate and set parameters which need the different options to be set
%===========================================================================

%The getThetas function returns the theta parameter vector correspnding to a
%certain order
getThetas = getMethodThetaFunc(methodName);

%---------------------------------------------------------------------------
%Determine the initial order
%---------------------------------------------------------------------------
%Check if initial order was set by user, otherwise find the lowest available
%order and set this as the initial order
i = 1;
while exist('orders') == 0
    orderAvailable = true;
    try
        getThetas(i);
    catch ME
        if (strcmp(ME.identifier,'Thetas:NoMethod'))
            orderAvailable = false;
        else
            rethrow(ME)
        end
    end
    if orderAvailable
        orders = [i - 1, i, i + 1];
    end
    i = i + 1;
end
%Save the initial order (used in the beginning to determine if we have early
%special cases needed to take care of, such as too few steps to calculate a
%solution of a higher order etc.). Also used for statistical purposes.
initialOrder = orders(2);
%Check to see that the initial order does actually exist
try
    getThetas(initialOrder);
catch ME
    if (strcmp(ME.identifier,'Thetas:NoMethod'))
        ME = MException('Solver:InitialOrderDoesNotExist',...
                sprintf('The initial order %d does not exist.',...
                initialOrder));
            throw(ME);
    else
        rethrow(ME);
    end
end

%---------------------------------------------------------------------------
%Determine the filter to be used
%---------------------------------------------------------------------------
if (exist('filterVec','var'))
    filter = filterVec;
else
    filter = getFilterVec(filterName);
end
%Calculate the digital filter order (how many errors we need to feed the
%controller)
PD = floor(length(filter)/2) + 1;
%Initialize the control error vector to the correct size
controlErrors = ones(PD,3);

%---------------------------------------------------------------------------
%Calculate the first k-points which are needed to start the solver
%---------------------------------------------------------------------------
%Check if user set initial step size, otherwise use autostart to calculate
%it
if exist('h') == 0
    h(1) = autostart(f, tSpan, x0, TOL, initialOrder);
end
%Calculate k-1 additional points so that we can start using the k-step
%method
[h,t,x,xDot] = start(f,h,t,x,xDot,TOL,initialOrder);
%Do not include the last (not yet accepted) step size
%%h = h(1:end-1);
tNew = t(end) + h(end);
%Set how many solution points we have now
N = initialOrder;
%Initialize step sizes for all three orders
steps = ones(PD,3)*h(1);

%Assume that all initial points are exact
errors = zeros(1,N);

%No order specified in the beginning, set it to 0
orderstats = zeros(1,N);


%---------------------------------------------------------------------------
%Calculate the predictor used to estimate the error in the first step
%---------------------------------------------------------------------------
%Calculate the standard predictor for the lower order
try
    polCoeffOld{1} = polE(getThetas(orders(1)),h(1:end-1),x(1:end-1,:),...
        xDot(1:end-1,:));
catch ME
    %We may start on the lowest available order, in that case set pmin=true
    %to indicate this and also put some values in polCoeffOld, to not
    %interupt calculations further down (the actual values do not matter)
    if (strcmp(ME.identifier,'Thetas:NoMethod'))
        pmin = true;
        polCoeffOld{1} = ones(length(orders(1))+1,numUnknowns);
    end
end

xPred = zeros(numUnknowns,3);
%Calculate the prediction for the lower order method
if ~pmin
    for i=1:numUnknowns
        xPred(i,1) = polyval(polCoeffOld{1}(:,i),h(end) + steps(end,2));
    end
end
%Set the old coefficients for all methods, as to not get an error
polCoeffOld{2} = polCoeffOld{1};
polCoeffOld{3} = polCoeffOld{1};
%Initialize the polynomial coefficients for all orders, only used so that
%no exception is thrown if we are unable to calculate the coefficients in
%the beginning
polCoeff = polCoeffOld;
%Set the new step size ratio to 1 for all orders.
rNew(1:3) = 1;

%===========================================================================
% Main loop used to calculate the solution
%===========================================================================
while t(N) < tSpan(2)
    if steps(end,2)/(tSpan(2)-tSpan(1)) < 1e-16
        ME = MException('Solver:NoSolution',['Too small step',...
            'size reached at t=%d. Try increasing TOL.'],tNew);
        throw(ME);
    end
%===========================================================================
% Calculation of the solution and error estimate
%===========================================================================
    %Calculate new solution
    %If the step was not rejected calculate new coefficients, otherwise just
    %reuse the already existing coefficients
    if ~rejected
        for i=1:3
            %For the first points we only calculate coefficients for the
            %current order, because we have not yet started with the order
            %control
            if N >= initialOrder + 1 || i < 3
                try
                    polCoeff{i} = polE(getThetas(orders(i)),h,x,xDot);
                catch ME
                    if (strcmp(ME.identifier,'Thetas:NoMethod'))
                        %Check for which order the method was unavailable,
                        %if it is current order something is wrong
                        switch i
                            case 1
                                pmin = true;
                            case 2
                                rethrow(ME);
                            case 3
                                pmax = true;
                        end
                    else
                        rethrow(ME);
                    end
                end
                %Check to see that the calculations of the coefficients did
                %produce real numbers
                if(~isfinite(sum(polCoeff{i})))
                    ME = MException('Solver:NoSolution',['polCoeff are',...
                        'not finite. t = %d'],t(end));
                    throw(ME);
                end
            end
        end
    end

    %Calculate the new solution for each order. This loop should always
    %evaluate, because there should always be coefficients for all orders
    %even if an order is unavailable. If this is the case the incorrect
    %results from this loop (and results derived from it) will not be used,
    %because the error estimates will either be bypassed, or in the case of
    %the order control pmax/pmin will prevent the results from influencing
    %the decision to change order.
    for i=1:3
        %Evaluate the polynomials in the new point for each unknown
        for j=1:numUnknowns
            xNew(j,i) = polyval(polCoeff{i}(:,j),steps(end,2));
        end
    end

    %Estimate the unscaled new error
    for i=1:3
        newErrors(:,i) = xPred(:,i) - xNew(:,i);
    end

    %Apply anti-windup
    if unit
        newErrors = newErrors*diag((steps(end,:)/steps(end,2)).^(orders));
    else
        newErrors = newErrors*diag((steps(end,:)/steps(end,2)).^(orders+1));
    end
    for i=1:3
        if relerr == 0
            controlErrors(end,i) = normFunc(newErrors(:,i))/(TOL);
        else
            controlErrors(end,i) = normFunc(newErrors(:,i))/...
                (normFunc(abs(xNew(:,i))*TOL+1e-16));
        end
    end

%===========================================================================
% Step size control
%===========================================================================
    %Feed data to controller
    for i=1:3
        %If we do not have enough previous values, bypass the controller
        %and set the new step size ratio to the scaled control error
        if bypass(i) < PD - 1
            rNew(i) = controlErrors(end,i)^-(1/orders(i));
            %Only increase the bypass counter if the step has not been
            %rejected (otherwise we would just recount the same step over
            %and over)
            if ~rejected
                bypass(i) = bypass(i) + 1;
            end
        else
            rNew(i) = errorController(filter, orders(i),...
                controlErrors(:,i)',steps(:,i)', unit);
        end
    end
    %If this is the first step we ignore the result from the controller for
    %the current/higher order method because it was calculated using
    %incorrect error estimates
    if N==initialOrder
        rNew(2:3) = 1;
        bypass(2:3) = 0;
    %If this is the second step we ignore the result from the controller for
    %the higher order method because it was calculated using incorrect error
    %estimates.
    elseif N==initialOrder+1
        rNew(3) = 1;
        bypass(3) = 0;
    end

%===========================================================================
% Step size rejection/acceptation
%===========================================================================
    %If the suggested step size is too small, then the error in the newly
    %calculated step is too large, and we reject this step.
    if rNew(2) < perc(1)
        rejectedSteps_at(end + 1,:) = [tNew,rNew(2),xNew(:,2)'];
        tNew = tNew - steps(end,2);
        if ~rejected
            steps(end,2) = h(end)*perc(1);
        else
            steps(end,2) = steps(end,2)*0.95;
        end
        rNew(2) = steps(end,2)/h(end);
        rejected = true;
        rejectedSteps = rejectedSteps + 1;
        %Because the control sequence is now destroyed, we bypass all orders
        bypass(:) = 0;
    %The step is accepted
    else
        %If the new step size ratio is larger than allowed we override the
        %controller by setting rNew to its allowed maximum value
        if rNew(2) > perc(2)
            nbrStepsRTooBig_at(end + 1,:) = [tNew,rNew(2),xNew(:,2)'];
            %The step is accepted, so we save the new calculated values and
            %increment the number of steps taken.
            nbrStepsRTooBig = nbrStepsRTooBig + 1;
            rNew(2) = perc(2);
        end
        %The step is accepted, so we save the new calculated values and
        %increment the number of steps taken.
        N = N + 1;
        h(N - 1) = steps(end,2);
        %Shift the step size sequence, and calc new step size
        steps(:,2) = circshift(steps(:,2),-1,1);
        steps(end,2) = rNew(2)*steps(end-1,2);
        x(N,:) = xNew(:,2);
        xDot(N,:) = f(tNew,xNew(:,2));
        t(N,1) = tNew;
        errors(N) = controlErrors(end);
        orderstats(N) = orders(2);

        %Shift control error sequences
        for i=1:3
            controlErrors(:,i) = circshift(controlErrors(:,i),-1,1);
        end
        rejected = false;
        polCoeffOld = polCoeff;
    end

%===========================================================================
% Order control
%===========================================================================
    %We do not advance the order control mechanism if the step was rejected.
    %If this is the first or second step (after the initialization of the
    %solver) we do not run the order control part, because we lack
    %solutions/predictions for the higher order solver. Also, if this step
    %is the last one (which has passed the end point) then we do not change
    %order, because we want to use the same order when we adjust the step so
    %it hits the end point.
    if ~rejected && N >= initialOrder + 3 && t(N) < tSpan(2)
        %We limit the step size ratio upwards, as to not overestimate the
        %benifits of changing the order. We also calculate the new step
        %sizes for the higher and lower order
        for i=[1,3]
            rNew(i) = min(rNew(i), perc(2));
            steps(:,i) = circshift(steps(:,i),-1,1);
            steps(end,i) = rNew(i)*steps(end-1,i);
        end

        workFactors = getWorkFactors(orders);
        %Calculate the relative efficiency for the lower and higher order
        lowEff = steps(end,1)/steps(end,2)/(workFactors(1)/workFactors(2));
        highEff = steps(end,3)/steps(end,2)/(workFactors(3)/workFactors(2));

        %Calculate the continuous order increments
        dpp = max(0, 4*(highEff - 1)/(2*highEff + 2));
        dpm = min(0, 4*(1 - lowEff)/(2*lowEff + 2));
        if (highEff - 1)*(lowEff - 1) < 0
            dppm = (highEff - lowEff)/(highEff + lowEff);
        else
            dppm = 0;
        end

        %If we have reached the lowest or highest possible order, modify
        %the continuous order increments as to not get stuck on this
        %order or try to change to an order that does not exist
        if pmax
            dpp = 0;
            dppm = 0;
        end
        if pmin
            dpm = 0;
            dppm = 0;
        end

        %Integrate the continuous order increments
        dp = dp + (dpp + dpm + dppm);

        %Calculate the discrete order increment which is used to check
        %if there is to be an order change
        increment = round(dp);

        if increment > 0
            increment = 1;
        elseif increment < 0
            increment = -1;
        end

        if increment > 0 && rNew(3) > perc(1) && highEff > 1.1
            %Change the order
            orders = orders + increment;
            %Reset the continuous order change variable
            dp = 0;
            %Shift the step size sequences and the old coefficients
            for i=1:2
                steps(:,i) = steps(:,i+1);
                polCoeffOld{i} = polCoeffOld{i+1};
                bypass(i) = bypass(i+1);
            end
            %Calculate what the old coefficients would have been if we
            %used the newly introduced order (in case we get a rejected
            %step immediately). We also check if we have reached the
            %maximum order when we do this.
            try
                polCoeffOld{3} = polE(getThetas(orders(3)),...
                    h(1:end-1),x(1:end-1,:),xDot(1:end-1,:));
            catch ME
                if (strcmp(ME.identifier,'Thetas:NoMethod'))
                    pmax = true;
                else
                    rethrow(ME);
                end
            end
            %if we increase the order we can no longer have minimum order,
            %so set this to false
            pmin = false;
            %we do not have a previous step size sequence for the higher
            %order, so we need to bypass this at first
            bypass(3) = 0;
        elseif increment < 0 && rNew(1) > perc(1) && lowEff > 1.1
            %Change the order
            orders = orders + increment;
            %Reset the continuous order change variable
            dp = 0;
            %Shift the step size sequences and the old coefficients
            for i=3:-1:2
                steps(:,i) = steps(:,i-1);
                polCoeffOld{i} = polCoeffOld{i-1};
                bypass(i) = bypass(i-1);
            end
            %Calculate what the old coefficients would have been if we
            %used the newly introduced order (in case we get a rejected
            %step immediately). We also check if we have reached the
            %minimum order when we do this.
            try
                polCoeffOld{1} = polE(getThetas(orders(1)),...
                    h(1:end-1),x(1:end-1,:),xDot(1:end-1,:));
            catch ME
                if (strcmp(ME.identifier,'Thetas:NoMethod'))
                    pmin = true;
                else
                    rethrow(ME);
                end
            end
            %if we decrease the order we can no longer have maximum order,
            %so set this to false
            pmax = false;
            %we do not have a previous step size sequence for the lower
            %order, so we need to bypass this at first
            bypass(1) = 0;
        end
    end
    %We calculate the predictor for the next step. If N==initialOrder this
    %means that the first step after the initialization has been rejected.
    %If this is the case we need to take into account the special predictors
    %used in the beginning, and use different step lengths for different
    %orders.
    tNew = tNew + steps(end,2);
    for i=1:3
        for j=1:numUnknowns
            xPred(j,i) = polyval(polCoeffOld{i}(:,j),...
                h(N-1)+steps(end,2));
        end
    end
end
%===========================================================================
% Adjusting of the last step to hit the end point
%===========================================================================

%Calculate the step size from the second to last point so it hits the end
%point
steps(end,2) = tSpan(2) - t(N-1);
%Use the last calculated polynomial but with the step size which hits the
%end point.
for i=1:numUnknowns
    xNew(i,2) = polyval(polCoeff{2}(:,i),steps(end,2));
end
%Over write the value which overshot the end point
t(N) = tSpan(2);
x(N,:) = xNew(:,2);
xDot(N,:) = f(tNew,xNew(:,2));

%===========================================================================
% Gather and package statistics
%===========================================================================
cpuTime = toc;
%Create the rejectedSteps_at-struct and the nbrStepsRTooBig_at-struct
if rejectedSteps ~= 0
    rejectedSteps_at = struct('t',rejectedSteps_at(:,1),'r',...
        rejectedSteps_at(:,2),'x',rejectedSteps_at(:,3:end));
else
    rejectedSteps_at = [];
end

if nbrStepsRTooBig ~= 0
    nbrStepsRTooBig_at = struct('t',nbrStepsRTooBig_at(:,1),'r',...
        nbrStepsRTooBig_at(:,2),'x',nbrStepsRTooBig_at(:,3:end));
else
    nbrStepsRTooBig_at = [];
end

%Package statitistics in struct
statistics = struct('solver','pmmeVarOrd',...
    'methodName',methodName,...
    'initialOrder',initialOrder,...
    'orders',orderstats,...
    'filterName',filterName,...
    'tol',TOL,...
    'perc',perc,...
    'relativeError',relerr,...
    'errorPerUnitStep',unit,...
    'normFunc',func2str(normFunc),...
    'cpuTime',cpuTime,...
    'numberOfSteps',N,...
    'rejectedSteps',rejectedSteps,...
    'rejectedStepsData',rejectedSteps_at,...
    'nbrStepsRTooBig',nbrStepsRTooBig,...
    'nbrStepsRTooBigData',nbrStepsRTooBig_at,...
    'errorEstimate',errors);
end
