function [t,x,statistics] = ode45_mod (f,tspan,x0,tol,relerr)
absTol = 1e-30;
relTol = 1e-30;
if relerr
    relTol = tol;
else
    absTol = tol;
end
[t,x] = ode45(f,tspan,x0,struct('RelTol',relTol,'AbsTol',absTol));
statistics = struct('numberOfSteps', length(t) - 1);
