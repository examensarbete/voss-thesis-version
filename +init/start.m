function [h,t,x,xd] = start(f,h0,t0,x0,xd0,TOL,k,varargin)
% Calculates steps used to start a multistep method
%
% This function can be used in the beginning of a multistep method, before
% enough steps exists to start using the method. It generates k steps by
% using Matlab's solver ode45.
%
% Parameters
% ----------
% f : function handle
%    RHS of ODE u' = f(t,u)
% h0 : float
%    The initial step size
% t0 : float
%    The starting point of the integration interval
% x0 : vector
%    The initial solution point
% xd0 : vector
%    The solution of the RHS function at t0, x0
% TOL : float
%    The tolerance
% k : int
%    The number of steps to be generated
% varargin : options list
%    Options replacing default values on the form of strings
%    with corresponding value. The following options are
%    available:
%
%   'analyticsol'
%       Function handle of the analytical solution to the RHS function. If
%       this is supplied, this is used instead of ode45 to calculate the
%       first k steps.
%
% Returns
% -------
% h : vector
%    All step sizes (including h0)
% t : vector
%    All time points (including t0)
% x : vector
%    All solution values (including x0)
% xd : vector
%    All derivatives of the solution values (including xd0)

%Default values
matlabSolver = @ode45;
for i = 1:2:length(varargin)
    name = varargin{i};
    value = varargin{i+1};
    switch lower(name)
        case 'analyticsol'
            analyticSol = value;
        case 'matlabsolver'
            matlabSolver = str2func(value);
    end
end

if(k == 1)
    h = h0;
else
    h = ones(1,k-1)*h0;
end
t = [t0:h0:h0*(k-1) + t0]';
x = zeros(k,length(x0));
x(1,:) = x0;
xd = zeros(k,length(x0));
xd(1,:) = xd0;
for j=1:k-1
    options = odeset('relTol',TOL,'absTol',TOL*1e-3);
    if(exist('analyticSol','var') == 0)
        [~,Y] = matlabSolver(f,[t(j),t(j+1)],x(j,:),options);
        x(j+1,:) = Y(end,:);
    else
        x(j+1,:) = analyticSol(t(j+1));
    end
    xd(j+1,:) = f(t(j+1),x(j+1,:)')';
end
end
