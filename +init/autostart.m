function h = autostart(f, tspan, u0, tol, p)
% autostart is an auto-scaling version of protostart
%
% Proto-stepsize for ODE IVP solver
% Can be used as starting step size, or to prime a starter
%
% Internal parameters and features:
%
% * Offers RMS norm option to evaluate vector norms and estimate Lipschitz
% * constant in order to be useful also for PDE discretizations
% * Offers conservative stepsize output alternative as well
% * Written for adaptiveRK45group set of RK solvers
%
% (c) G Söderlind, LU, 2015-06-18, V1.3
%
% Parameters
% ----------
% f : function handle
%    RHS of ODE u' = f(t,u)
% tspan : vector
%    Vector of start/stop point of integration range [t0,tf]
% u0 : vector
%    Initial value
% tol : float
%    Absolute error tolerance (can be modified for other criteria)
% p : int
%    Method order
%
% Returns
% -------
% h : float
%    Proposed initial step size

pr = p + 1;
tolscale = tol^(1/pr);
normscale = 1;
% normscale = srqt(length(u0));       % RMS norm option (include/exclude)

t0 = tspan(1);
tf = tspan(2);
T = abs(tf - t0);                   % Length of integration interval
direction = sign(tf - t0);          % Keep track of integration direction

% Perturb initial condition and compute rough Lipschitz estimate
cent = norm(u0)/normscale/100;
v0 = u0 + cent*rand(size(u0));
u0prime = f(t0,u0);
v0prime = f(t0,v0);
Lip = norm(u0prime - v0prime)/norm(u0 - v0);

% Provisional step size used in estimation procedure has a conservative
% hL ~ 0.05 (due to underetsimation of L), subject to further limitations
% based on range of integration
h = direction*min(1e-3*T,max(1e-8*T,0.05/Lip));

% Estimation procedure ==================================================
% Step 1: Fwd Euler step
u1 = u0 + h*u0prime;
t1 = t0 + h;

% Step 2: Fwd Euler step in reverse back to t0
u1prime = f(t1,u1);
u0comp = u1 - h*u1prime;

% Step 3: Estimate of local error, proportional to h^2*norm(u'')
du = u0comp - u0;
dunorm = norm(du);                  % For use in Lipschitz estimator only
errnorm = dunorm/normscale;         % Norm of error

% Step 4: New estimate of Lipschitz constant and log Lipschitz constant
u0comprime = f(t0,u0comp);
L = norm(u0comprime - u0prime)/dunorm;
M = du'*(u0comprime - u0prime)/dunorm^2;

% Step 5: Construct a refined starting step size and output result
theta1 = tolscale/sqrt(errnorm);    % Account for accuracy
theta2 = tolscale/abs(h*(L + M/2)); % Account for stability
h = h*(theta1 + theta2)/2;          % Stabilized average of two criteria
h = direction*min(3e-3*T,abs(h));   % Safety net catching too large step
