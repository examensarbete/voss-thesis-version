function [h,t,x,xDot] = start2(f,h0,t0,x0,xd0,TOL,k)
import init.autostart;
import init.start;
import control.errorController;
import solverFunctions.polI;
import solverFunctions.polE;
import solverFunctions.getJac;
import solverFunctions.getMethodVec;
import control.getFilterVec;

% --------------------------------------------------------------------------
% Initialization of variables and starting paramaters
% --------------------------------------------------------------------------

%Initialize parameters
rejected = false; %Flag indicating if the last step was rejected
h(1) = h0;
t(1) = t0; %Time points
x(1,:) = x0; %Solution points
xDot(1,:) = xd0; %Derivative of solution points
numUnknowns = length(x0); %Number of unknowns in the system to be solved
stepLimit = k; %The number of steps we want

%Default values of optional parameters
jac = @(t,x) getJac(f,t,x); %numerical calc. of jacobian as default
normFunc = @(x) norm(x,Inf);
TOL = 1e-6; %default tolerance
method = [0]; %BDF1
perc = [0.8,1.2]; %default min/max step size ratio
relerr = false; %flag to indicate if relative error is to be used
unit = false; %flag to indicate if error per unit step is to be used
%Initialize the vectors used to store calculated solution values
xNew = x0;
xPred = x0;

%Order and steps of the method
order = 1;

%Calculate k-1 additional points so that we can start using the k-step
%method
tNew = t0 + h(1);
hNew = h(1);
rNew = 1;

%Set how many solution points we have now + 1
N = 1;

%Use AB1 to initialize the newton iteration in the first step
predThetas = [];
polCoeffOld = polE(predThetas,h,x,xDot);

%Calculate initial prediction point used to estimate the error
for i=1:numUnknowns
    xPred(i) = polyval(polCoeffOld(:,i),hNew);
end

% --------------------------------------------------------------------------
% Main loop used to calculate the solution
% --------------------------------------------------------------------------
while N <= stepLimit
% --------------------------------------------------------------------------
% Calculation of the solution and error estimate
% --------------------------------------------------------------------------
    %Calculate the new coefficients. If N==k it means that we calculate the
    %first step, and in such a case the number of coefficients are too low,
    %and we need to add a row of ones
    polCoeff = polI(f, jac, method,[h hNew], x, xDot,...
        tNew,polCoeffOld, TOL);
    if(~isfinite(sum(polCoeff)))
        ME = MException('Solver:NoSolution',['polCoeff are not',...
            ' finite. t = %d'],t(end));
        throw(ME);
    end
    for i=1:numUnknowns
        xNew(i) = polyval(polCoeff(:,i),0);
    end


    %Estimate the new error
    if relerr == 0
        controlError = 10*normFunc(xPred - xNew)/TOL;
    else
        controlError = 10*normFunc(xPred - xNew)/(normFunc(xNew)*TOL+1e-16);
    end
    
% --------------------------------------------------------------------------
% Step size control
% --------------------------------------------------------------------------
    %Feed data to controller
    rNew = controlError^(-1/order);

% --------------------------------------------------------------------------
% Step size rejection/acceptation
% --------------------------------------------------------------------------
    %If the suggested step size is too small, then the error in the newly
    %calculated step is too large, and we reject this step.
%     if rNew < perc(1)
    if controlError > 1
        tNew = tNew - hNew;
        if ~rejected %First rejection (in row)
            if(length(h) > 1)
                hNew = h(N-1);
            end
            hNew = perc(1)*hNew;
        else %Multiple rejections (in row)
            hNew = hNew*0.95;
            if hNew < 1e-16
                ME = MException('Solver:NoSolution',['Too small step',...
                    'size reached at t=%d. Try increasing TOL.'],tNew);
                throw(ME);
            end
        end
        tNew = tNew + hNew;
        rejected = true;

    %If the suggested step size is too large and we use the maximum step
    %size instead.
    elseif rNew > perc(2)
        %The step is accepted, so we save the new calculated values and
        %increment the number of steps taken.
        N = N + 1;
        h(N - 1) = hNew;
        hNew = perc(2)*hNew;
        x(N,:) = xNew;
        xDot(N,:) = f(tNew,xNew);
        t(N,1) = tNew;
        tNew = tNew + hNew;
        rNew = perc(2);
        rejected = false;
        %We save the polynomial coefficients, in case the next step is
        %rejected and we need them to recalculate the predictor.
        polCoeffOld = polCoeff;

    %The step size was in the allowed range.
    else
        %The step is accepted, so we save the new calculated values and
        %increment the number of steps taken.
        N = N + 1;
        h(N - 1) = hNew;
        hNew = rNew*hNew;
        x(N,:) = xNew;
        xDot(N,:) = f(tNew,xNew);
        t(N,1) = tNew;
        tNew = tNew + hNew;
        rejected = false;
        %We save the polynomial coefficients, in case the next step is
        %rejected and we need them to recalculate the predictor.
        polCoeffOld = polCoeff;
    end
    %We calculate the predictor for the next step.
    for i=1:numUnknowns
        xPred(i) = polyval(polCoeffOld(:,i),hNew);
    end
end
end
