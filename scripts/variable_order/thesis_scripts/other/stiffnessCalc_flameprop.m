addpath('../..')
warning('off','all')

% --------------------------------------------------------------------------
% Problem specifics
% --------------------------------------------------------------------------
problem = 'flameprop';
a=2;
[f,x0,tspan,jac,sol] = ode.getProblem(problem,a);

t = linspace(tspan(1),tspan(2),500)';
x = sol(t);
[sVec,tS] = tools.calsStiffnesOverTime(jac,t,x);
figure();
plot(tS,sVec);
figure();
plot(t,x);

%Below follows code to write the results to a file
dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
fileName = ...
    sprintf('data/%s/%s_stiffness_%s',...
    problem,problem,dateStr);
if(exist(fileparts(fileName),'dir') == 0)
    mkdir(fileparts(fileName));
end
fileID = fopen(fileName,'w');
fprintf(fileID,'# Problem: %s\n',problem);
titleStr = 't x s\n';
dataStr = '%.6e %.6e %.6e\n';
fprintf(fileID,titleStr);
data = [...
        t',
        x',
        sVec,
    ];
fprintf(fileID,dataStr ,data);
fclose(fileID);
