addpath('../..')
warning('off','all')

%Tolerances to solve for
%tols = [1e-4,1e-7,1e-10];

% --------------------------------------------------------------------------
% Problem specifics
% --------------------------------------------------------------------------
problem = 'lotka';
[f,x0,tspan,jac,sol] = ode.getProblem(problem,4);

statistics = {};
t = {};
x = {};
h = {};

% --------------------------------------------------------------------------
% Choose method
% --------------------------------------------------------------------------
%Choose method
%The following implicit methods exist:
%   *bdf_x-y (x is the lower order limit and y is the higher order limit)
% NOTE: x and y indicates order and not the number of steps used
method = 'am_2-8';
order = 2;

% --------------------------------------------------------------------------
% Choose filter
% --------------------------------------------------------------------------
%The following filters exist:
%   *'H211PI'
%   *'H2111b'
%   *'PI3333'
%   *'PI3040'
%   *'PI4020'
%   *'H312b'
%   *'PID'
filter ='PI3333';

%Choose perc
perc = [0.8,1.2];

%Choose if relative error is to be used
relerr = false;
%Choose if error per unit step is to be used
unit = false;

%Choose the p-norm in which to meauser the error
normFunc = @(x) norm(x,Inf);
% --------------------------------------------------------------------------
% Create mySolver
% --------------------------------------------------------------------------
tols = [];
for k=linspace(5,10,100)
    tols(end+1,1) = 10^(-k);
end
h0 = zeros(length(tols),1);

%Try to estimate a good initial step size
display('Calculating initial step sizes');
num = -1;
num = tools.progressBar(0,'percentage',true,'character','#','numChar',num);
for i=1:length(tols)
    [t,x,statistics] = solvers.pmmipVarOrd(f,tspan,x0,'TOL',tols(i),...
        'filterName',filter,'perc',perc,'unit',unit,'InitialOrder',...
        order, 'relerr',relerr,'method',method,'jac',jac);
    h = diff(t);
    h0(i) = h(2);
    num = tools.progressBar(i/length(tols),'percentage',true,'character',...
        '#','numChar',num);
end

solver = @(rhs,tspan,x0,method,filter,tol,relerr,normFunc,hInit) ...
        solvers.pmmipVarOrd(f,tspan,x0,'TOL',tol,'filterName',filter,...
        'perc',perc,'unit',unit,'InitialOrder',order, 'relerr',relerr,...
        'method',method,'norm',normFunc,'jac',jac,'initialStep',hInit);

if relerr
    errorcalcmode = 'relerr';
else
    errorcalcmode = 'abserr';
end

refSolver = 'ode113';

statistics = tools.accVsTolVarOrd(f,tspan,x0,method,filter,tols',solver,...
    refSolver,'errorcalcmode',errorcalcmode,'normFunc',normFunc,...
    'initialSteps',h0);

dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
fileName = sprintf('data/%s/%s_acc-tol_preset_h0_%s',problem,problem,dateStr);
if(exist(fileparts(fileName),'dir') == 0)
    mkdir(fileparts(fileName));
end
fileID = fopen(fileName,'w');
fprintf(fileID,'# Problem: %s\n',problem);
fprintf(fileID,'# Solver: pmmip\n');
fprintf(fileID,'# Method: %s\n',method);
fprintf(fileID,'# Initial order: %d\n', order);
fprintf(fileID,'# Filter: %s\n',filter);
fprintf(fileID,'# Initial step size was preset\n');
if relerr
    fprintf(fileID,'# Error mode: Relative error\n');
else
    fprintf(fileID,'# Error mode: Absolute error\n');
end
if unit
    fprintf(fileID,'# Error per unit step used\n');
else
    fprintf(fileID,'# Error per unit step not used\n');
end
fprintf(fileID,'# Norm-function: %s\n',func2str(normFunc));
fprintf(fileID,'# perc: [%f, %f]\n',perc);
fprintf(fileID,'# Time to generate data: %f s\n',statistics.totalCpuTime);

fprintf(fileID,'tol acc numSteps fail meanOrd time h0\n');
data = [...
    statistics.allTols;...
    statistics.errorFromAccVsTol;...
    statistics.numberOfSteps;...
    statistics.rejectedSteps;...
    statistics.meanOrder;...
    statistics.cpuTime;...
    h0';...
    ];
fprintf(fileID,'%.6e %.6e %.6e %.6e %.6e %.6e %.6e\n',data);
fclose(fileID);
