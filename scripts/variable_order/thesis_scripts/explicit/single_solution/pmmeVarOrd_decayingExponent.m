addpath('../..')
warning('off','all')

%Tolerances to solve for
tols = [1e-4,1e-7,1e-10];

% --------------------------------------------------------------------------
% Problem specifics
% --------------------------------------------------------------------------
problem = 'decayingExpo';
p=1;
[f,x0,tspan,jac,sol] = ode.getProblem(problem,p);

statistics = {};
t = {};
x = {};
h = {};

% --------------------------------------------------------------------------
% Choose method
% --------------------------------------------------------------------------
%Choose method
%The following implicit methods exist:
%   *bdf_x-y (x is the lower order limit and y is the higher order limit)
% NOTE: x and y indicates order and not the number of steps used
methodName = 'ab_1-8'
initialOrder = 1;

% --------------------------------------------------------------------------
% Choose filter
% --------------------------------------------------------------------------
%The following filters exist:
%   *'H211PI'
%   *'H2111b'
%   *'PI3333'
%   *'PI3040'
%   *'PI4020'
%   *'H312b'
%   *'PID'
filterName ='PI3333';

%Choose perc
perc = [0.8,1.2];

%Choose if relative error is to be used
relerr = false;
%Choose if error per unit step is to be used
unit = false;

%Choose the p-norm in which to meauser the error
pNorm = Inf;
% --------------------------------------------------------------------------
% Create mySolver
% --------------------------------------------------------------------------

mySolver = @(f,tspan,x0,tol,relerr,normFunc) solvers.pmmeVarOrd(f,tspan,...
    x0,'TOL',tol,'filterName',filterName,'perc',perc,'unit',unit,...
    'InitialOrder',initialOrder, 'relerr', relerr,'method',methodName);

% --------------------------------------------------------------------------
% Choose matlab solver to compare with
% --------------------------------------------------------------------------
matlabSolverName = 'ode15s';

% --------------------------------------------------------------------------
% Other manatory settings
% --------------------------------------------------------------------------
reference = sol; %Enter either the name of the matlab solver you
                         %want to use as reference (string) or the
                         %analytical solution (function handle).
                     
% --------------------------------------------------------------------------
% Other optional settings
% --------------------------------------------------------------------------
%We do not use the matlab solution, so set large tolerances so mainPlotFunc
%just solves quickly
matlabOptions =...
    odeset('relTol',10,'absTol',10,'BDF','on','NormControl','on');
optionsReference = odeset('relTol',1e-13,'absTol',1e-16);

% --------------------------------------------------------------------------
% Do not touch!
% --------------------------------------------------------------------------
for i=1:length(tols)
    [statistics{i},t{i},x{i}] = tools.mainPlotFunc(f,tspan,x0,tols(i),...
        mySolver,matlabSolverName,reference,...
        'refoptions',optionsReference,...
        'matlaboptions',matlabOptions,...
        'relerr',relerr,...
        'jac',jac,...
        'plotlist',zeros(1,8),...
        'normFunc',@(x)norm(x,pNorm));
    h{i} = [diff(t{i}); NaN];
end

if exist('plotInScript','var') && plotInScript
    %Plot the solutions
    figure();
    hold on;
    legendText = {};
    for i=1:length(tols)
        plot(t{i},x{i});
        legendText{i} = sprintf('tol=%.0e',tols(i));
    end
    legend(legendText);
    title(sprintf('%s, Solutions (pmme)',problem));
    hold off;

    %Plot the step sizes
    figure();
    hold on;
    legendText = {};
    for i=1:length(tols)
        plot(t{i},h{i});
        legendText{i} = sprintf('tol=%.0e',tols(i));
    end
    set(gca,'yscale','log')
    legend(legendText);
    title(sprintf('%s, Step sizes (pmme)',problem));
    hold off;

    %Plot the error
    figure();
    hold on;
    legendText = {};
    for i=1:length(tols)
        plot(t{i},statistics{i}.realErrors);
        legendText{i} = sprintf('tol=%.0e',tols(i));
    end
    set(gca,'yscale','log')
    legend(legendText);
    if relerr
        errorType = 'Relative';
    else
        errorType = 'Absolute';
    end
    title(sprintf('%s, %s errors measured by %s-norm (pmme)',...
        problem,errorType,num2str(pNorm)));
    hold off;

    %Plot the orders
    figure();
    hold on;
    legendText = {};
    for i=1:length(tols)
        plot(t{i},statistics{i}.orders);
        legendText{i} = sprintf('tol=%.0e',tols(i));
    end
    legend(legendText);
    title(sprintf('%s, Orders (pmme)',problem));
    hold off;
end

%Below follows code to write the results to a file
dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
for i=1:length(tols)
    fileName = ...
        sprintf('data/%s/%s_single_sol_tol_%.0e_%s',...
        problem,problem,tols(i),dateStr);
    if(exist(fileparts(fileName),'dir') == 0)
        mkdir(fileparts(fileName));
    end
    fileID = fopen(fileName,'w');
    fprintf(fileID,'# Problem: %s\n',problem);
    fprintf(fileID,'# Solver: pmme\n');
    fprintf(fileID,'# Method: %s\n',methodName);
    fprintf(fileID,'# Initial order: %d\n', initialOrder);
    fprintf(fileID,'# Filter: %s\n',filterName);
    if relerr
        fprintf(fileID,'# Error mode: Relative error\n');
    else
        fprintf(fileID,'# Error mode: Absolute error\n');
    end
    if unit
        fprintf(fileID,'# Error per unit step used\n');
    else
        fprintf(fileID,'# Error per unit step not used\n');
    end
    fprintf(fileID,'# Norm-function: %s-norm\n',num2str(pNorm));
    fprintf(fileID,'# perc: [%f, %f]\n',perc);
    %size(t{i})
    %size(x{i})
    %size(h{i})
    %size(statistics{i}.realErrors)
    %size(statistics{i}.orders)
    
    titleStr = 't ';
    dataStr = '%.6e ';
    solSize = size(x{i});
    for j=1:solSize(2)
        titleStr = [titleStr, sprintf('x%d ',j)];
        dataStr = [dataStr,'%.6e '];
    end
    titleStr = [titleStr, 'h e o\n'];
    dataStr = [dataStr, '%.6e %.6e %.6e\n'];
    fprintf(fileID,titleStr);
    data = [...
            t{i}',
            x{i}',
            h{i}',
            statistics{i}.realErrors',
            statistics{i}.orders,
        ];
    fprintf(fileID,dataStr ,data);
    fclose(fileID);
end
