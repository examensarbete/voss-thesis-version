folder  = pwd;
list    = dir(fullfile(folder, '*.m'));
nFile   = length(list);
success = false(1, nFile);
succeeded = {};
failed = {};
%Remove the script itself from the list
for k = 1:nFile
    file = list(k).name;
    if strcmp(strcat(mfilename,'.m'),file)
        list(k) = [];
        break;
    end
end
%Recalculate nFile
nFile = length(list);
for k = 1:nFile
	file = list(k).name;
        disp('----------------------------');
        fprintf('Running script %s (%d of %d)\n',file,k,nFile)
        disp('Output from script:')
        try
			run(fullfile(folder, file));
			success(k) = true;
			succeeded{end+1} = file;
		catch ME
			failed{end+1} = sprintf('%s: %s', file, ME.message);
        end	
        disp('----------------------------');
end
disp('============================');
disp('Succeded:')
for i=1:length(succeeded)
	disp(succeeded{i})
end
disp('============================');
disp('Failed:')
for i=1:length(failed)
	disp(failed{i})
end
disp('============================');
