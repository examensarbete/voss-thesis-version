warning('off','all');

[f,x0,tspan,jac,sol] = ode.getProblem('hires');

unit = false;
relerr = false;
perc =[0.8,1.2];
filter = 'PI3333';

%==========================================================
%Setup solver instances
%==========================================================
instances = {};

%----------------------------------------------------------
%PMMI
%----------------------------------------------------------
pmin=1;
pmax=5;
order = 1;
normFunc = @(x) norm(x,inf);
method = sprintf('bdf_%d-%d',pmin,pmax);

refSolver = 'ode15s';
refOptions = odeset('relTol',1e-13,'absTol',1e-16);

solver = @(rhs,tspan,x0,method,filter,tol,relerr,normFunc) ...
        solvers.pmmiVarOrd(f,tspan,x0,'jac',jac,'TOL',tol,'filterName',...
        filter,'perc',perc,'unit',unit,'InitialOrder',order, 'relerr',...
        relerr,'method',method,'norm',normFunc);

tolVec = [];
for k=linspace(5,10,100)
    tolVec(end+1) = 10^(-k);
end
if relerr
    errorcalcmode = 'relerr'
else
    errorcalcmode = 'abserr'
end
statistics = tools.accVsTolVarOrd(f,tspan,x0,method,filter,tolVec,solver,...
    refSolver,'errorcalcmode',errorcalcmode,'normFunc',normFunc);

if exist('plotInScript','var') && plotInScript
    figure();
    loglog(statistics.allTols,statistics.errorFromAccVsTol);
    title('HIRES, Accuracy vs tolerance (pmmi)');
    figure();
    loglog(statistics.allTols,statistics.numberOfSteps);
    title('HIRES, Number of steps vs tolerance (pmmi)');
    figure();
    semilogx(statistics.allTols,statistics.meanOrder);
    title('HIRES, Mean order vs tolerance (pmmi)');
    figure();
    semilogx(statistics.allTols,statistics.rejectedSteps);
    title('HIRES, Number of rejected steps vs tolerance');
end

%Below follows code to write the results to a file

dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
fileName = strcat('data/hires/hires_acc-tol_',dateStr);
if(exist(fileparts(fileName),'dir') == 0)
    mkdir(fileparts(fileName));
end
fileID = fopen(fileName,'w');
fprintf(fileID,'# Problem: HIRES\n');
fprintf(fileID,'# Solver: pmmi\n');
fprintf(fileID,'# Method: %s\n',method);
fprintf(fileID,'# Initial order: %d\n', order);
fprintf(fileID,'# Filter: %s\n',filter);
if relerr
    fprintf(fileID,'# Error mode: Relative error\n');
else
    fprintf(fileID,'# Error mode: Absolute error\n');
end
if unit
    fprintf(fileID,'# Error per unit step used\n');
else
    fprintf(fileID,'# Error per unit step not used\n');
end
fprintf(fileID,'# Norm-function: %s\n',func2str(normFunc));
fprintf(fileID,'# perc: [%f, %f]\n',perc);
fprintf(fileID,'# Time to generate data: %f s\n',statistics.totalCpuTime);

fprintf(fileID,'tol acc numSteps fail meanOrd time\n');
data = [...
    statistics.allTols;...
    statistics.errorFromAccVsTol;...
    statistics.numberOfSteps;...
    statistics.rejectedSteps;...
    statistics.meanOrder;...
    statistics.cpuTime;...
    ];
fprintf(fileID,'%.6e %.6e %.6e %.6e %.6e %.6e\n',data);
fclose(fileID);
