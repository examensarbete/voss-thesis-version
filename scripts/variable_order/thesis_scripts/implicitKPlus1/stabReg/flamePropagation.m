function flamePropagation()
x0 = 1e-2;
problem='flameprop';
[f,x0,tspan,jac,sol] = ode.getProblem(problem,x0);
tspan = [0,8/x0];

unit = false;
relerr = false;
perc = [0.8,1.2];
filters = {'PI3333'};
pmin=2;
pmax=8;
method = sprintf('am_%d-%d',pmin,pmax);
order = 2;

for i=1:length(filters) 
    filter = filters{i};
    display(filter)
    [t,x,statistics] = solvers.pmmipVarOrd(f,tspan,x0,'TOL',...
        1e-7,'filterName',filter,'perc',perc, 'unit',unit,'InitialOrder',...
        order, 'relerr', relerr,'method',method,'jac',jac);
    
    optionsReference = odeset('relTol',1e-13,'absTol',1e-15);
    [mlabT,mlabX] = ode45(f, t, x0, optionsReference);
    eVals = 2*mlabX - 3*mlabX.^2;
    h = diff(t);
    figure();
    plot(t(1:end-1),eVals(1:end-1).*h,t,stabLimit(statistics.orders));
    title(sprintf('Filter: %s',filter))
end

function limit = stabLimit(order)
limit = [];
for o=order
    switch o
        case 0
            limit(end + 1) = 0;
        case 1
            limit(end+1) = NaN;
        case 2
            limit(end+1) = -6;
        case 3
            limit(end+1) = -3;
        case 4
            limit(end+1) = -90/49;
        case 5
            limit(end+1) = -45/38;
        case 6
            limit(end+1) = -1890/2459;
        case 7
            limit(end+1) = -35/71;
        case 8
            limit(end+1) = -28350/91463;
end
end
