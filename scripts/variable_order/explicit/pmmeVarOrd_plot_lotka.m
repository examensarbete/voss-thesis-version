addpath('../..')
clear all

% --------------------------------------------------------------------------
% Problem specifics
% --------------------------------------------------------------------------
cycles = 4;
[f,x0,tspan,jac,sol] = ode.getProblem('lotka',cycles);

% --------------------------------------------------------------------------
% Choose method
% --------------------------------------------------------------------------
%Choose method
%The following explicit methods exist:
%   *ab_x-y (x is the lower order limit and y is the higher order limit)
%   *edf_x-y (x is the lower order limit and y is the higher order limit)
%   nystrom_x-y (x is the lower order limit and y is the higher order limit,the lower limit is 3 and the upper is 5)
% NOTE: x and y indicates order and not the number of steps used
methodName = 'ab_1-8'
initialOrder = 1;

% --------------------------------------------------------------------------
% Choose filter
% --------------------------------------------------------------------------
%The following filters exist:
%   *'H211PI'
%   *'H2111b'
%   *'PI3333'
%   *'PI3040'
%   *'PI4020'
%   *'H312b'
%   *'PID'
filterName ='H211b';

% --------------------------------------------------------------------------
% Create mySolver
% --------------------------------------------------------------------------

mySolver = @(f,tspan,x0,tol,relerr,normFunc) solvers.pmmeVarOrd(f,tspan,...
    x0,'TOL',tol,'filterName',filterName,'perc',[0.7, 1.1], 'unit',0,...
    'InitialOrder',initialOrder, 'relerr', relerr,'method',methodName);

% --------------------------------------------------------------------------
% Choose matlab solver to compare with
% --------------------------------------------------------------------------
matlabSolverName = 'ode113';

% --------------------------------------------------------------------------
% What do you want to plot
% --------------------------------------------------------------------------
%These are all availible things to plot. Change
%value from 'true' do 'false' if you want to exclude some plot.
plot_mySol = true;

plot_refSol = false;

plot_stiffness = false;

plot_stepSize = true;

plot_error = true; plot_controlError = false;

plot_stepSizeRatios = true;

plot_order = true;

plotList = [plot_mySol, plot_refSol, plot_stiffness, plot_stepSize,...
    plot_error, plot_controlError, plot_stepSizeRatios, plot_order];

% --------------------------------------------------------------------------
% Other manatory settings
% --------------------------------------------------------------------------
tol = 1e-6;      %Set the tolerance
reference = 'ode113'; %Enter either the name of the matlab solver you
                         %want to use as reference (string) or the
                         %analytical solution (function handle).
                     
% --------------------------------------------------------------------------
% Other optional settings
% --------------------------------------------------------------------------
matlabOptions = odeset('relTol',tol,'absTol',tol*1e-3);
optionsReference = odeset('relTol',1e-13,'absTol',1e-16);
relerr = 1;

% --------------------------------------------------------------------------
% Do not touch!
% --------------------------------------------------------------------------
[statistics,t,x] = tools.mainPlotFunc(f,tspan,x0,tol,mySolver,matlabSolverName,reference,...
    'refoptions',optionsReference,...
    'matlaboptions',matlabOptions,...
    'relerr',relerr,...
    'jac',jac,...
    'plotlist',plotList,...
    'normFunc',@(x)norm(x,'inf'));
statistics
