%alphas and betas for all explicit methods we use
%AB1-6
display('AB1')
alphas = [1,-1];
betas = [0,1];
AB1 = struct('alphas',alphas,'betas',betas);

display('AB2')
alphas = [1,-1,0];
betas = [0,3/2,-1/2];
AB2 = struct('alphas',alphas,'betas',betas);

display('AB3')
alphas = [1,-1,0,0];
betas = [0,23/12,-4/3,5/12];
AB3 = struct('alphas',alphas,'betas',betas);

display('AB4')
alphas = [1,-1,0,0,0];
betas = [0,55/24,-59/24,37/24,-3/8];
AB4 = struct('alphas',alphas,'betas',betas);

display('AB5')
alphas = [1,-1,0,0,0,0];
betas = [0,1901/720,-1387/360,109/30,-637/360,251/720];
AB5 = struct('alphas',alphas,'betas',betas);

display('AB6')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('AB6'));
AB6 = struct('alphas',alphas,'betas',betas);

AB = struct('AB1',AB1,'AB2',AB2,'AB3',AB3,'AB4',AB4,'AB5',AB5,'AB6',AB6);

%EDF1-6
display('EDF1')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('EDF1'));
EDF1 = struct('alphas',alphas,'betas',betas);

display('EDF2')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('EDF2'));
EDF2 = struct('alphas',alphas,'betas',betas);

display('EDF3')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('EDF3'));
EDF3 = struct('alphas',alphas,'betas',betas);

display('EDF4')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('EDF4'));
EDF4 = struct('alphas',alphas,'betas',betas);

% display('EDF5')
% [alphas,betas] =
% tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('EDF5'));
% EDF5 = struct('alphas',alphas,'betas',betas);
% 
% display('EDF6')
% [alphas,betas] =
% tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('EDF6'));
% EDF6 = struct('alphas',alphas,'betas',betas);

EDF = struct('EDF1',EDF1,'EDF2',EDF2,'EDF3',EDF3,'EDF4',EDF4);

%Nyström methods
display('nyström3')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('nystrom3'));
nystrom3 = struct('alphas',alphas,'betas',betas);

display('nyström4')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('nystrom4'));
nystrom4 = struct('alphas',alphas,'betas',betas);

display('nyström5')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('nystrom5'));
nystrom5 = struct('alphas',alphas,'betas',betas);

nystrom = struct('nystrom3',nystrom3,'nystrom4',nystrom4,'nystrom5',nystrom5);

%EDC methods
display('edc22')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('edc22'));
edc22 = struct('alphas',alphas,'betas',betas);

display('edc23')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('edc23'));
edc23 = struct('alphas',alphas,'betas',betas);

display('edc33')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('edc33'));
edc33 = struct('alphas',alphas,'betas',betas);

display('edc24')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('edc24'));
edc24 = struct('alphas',alphas,'betas',betas);

display('edc34')
[alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('edc34'));
edc34 = struct('alphas',alphas,'betas',betas);

% display('edc45')
% [alphas,betas] = tools.getAlphasAndBetas_pmme(solverFunctions.getMethodVec('edc45'));
% edc45 = struct('alphas',alphas,'betas',betas);

edc = struct('edc22',edc22,'edc23',edc23,'edc33',edc33,'edc24',edc24,'edc34',edc34);

%
explicitMethods = struct('AB',AB,'EDF',EDF,'nystrom',nystrom,'edc',edc);
