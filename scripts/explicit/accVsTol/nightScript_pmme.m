%Script to run over night

% clf
% clear all
% run pmme_accVsTol_decayingExpo.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/decayingExpo_pmme_p=',p,'_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myR')

% clf
% clear all
% run pmme_accVsTol_flameprop.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/flameprop_pmme_x0=',x0,'_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myR')

% clf
% clear all
% run pmme_accVsTol_Lotka.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/lotka_pmme_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myR')

% clf
% clear all
% run pmme_accVsTol_VanDerPol.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/vanDerPol_pmme_mu=',mu,'_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myR')

% clf
% clear all
% run pmme_accVsTol_B3.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/B3_pmme_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myR')


% clf
% clear all
% run pmme_accVsTol_pleiades.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/pleiades_pmme_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myR')

% clf
% clear all
% run pmme_accVsTol_rtbo1.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/rtbo1_pmme_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myR')


% clf
% clear all
% run pmme_accVsTol_lorenz.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/lorenz_pmme_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myR')

% clf
% clear all
% run pmme_accVsTol_brusselator.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/brusselator_pmme_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,methodVec,filterVec,[1e-7],'myR')