%Script to plot Lotka
clear all
addpath('../../..')
warning('off','all')

%Problem specific
p = 5;
[f,x0,tspan,jac,sol] = ode.getProblem('decayingExpo',[p])
tspan = [0,20];

%Create method name vector
nameMethodVec = {'ab4'};
%nameMethodVec = {'edc22','edc23','edc33','edc24', 'edc34','edc45','ab1','ab2','ab3','ab4','ab5','ab6','edf1','edf2','edf3','edf4','edf5','edf6'};

%Create filter name vector
%nameFilterVec = {'H2111b'};
nameFilterVec = {'H211PI','H2111b','PI3333','PI3040','PI4020','H312b','PID'};

%Tolerance defs.
minTol = 7;
maxTol = 7;
nbrOfTols = 1;

%How do you want to calc. the error?
%Availible alternitives are:
%   'abserr' and 'relerr'
errorCalcMode = 'abserr';

%Plot title
plotTitle = sprintf('Decaying exponent, p = %d, nbrOfTols = %d, pmme',p,nbrOfTols)

%Creating mySolver
mySolver = @(f,tspan,x0,method,filter,tol,relerr,norm) solvers.pmme(f,tspan,x0,'methodname',method,'tol',tol,'filtername',...
    filter,'perc',[0.8,1.2], 'unit',0, 'relerr', relerr,'norm',norm,'initialstep',2,'predfirststep',true);
            
%Creating refSolver
% refSolver = 'ode113';
% optionsRefSolver = odeset('relTol',1e-13,'absTol',1e-16);

%Main method
[statisticsStruct] = tools.accVsTol_loop(f,tspan,x0,nameMethodVec,nameFilterVec,minTol,maxTol,nbrOfTols,mySolver,'errorcalcmode',errorCalcMode,'analyticalfunction',sol);
H211PI_X = statisticsStruct.ab4_H211PI.myX{1};
H2111b_X = statisticsStruct.ab4_H2111b.myX{1};
PI3333_X = statisticsStruct.ab4_PI3333.myX{1};
PI3040_X = statisticsStruct.ab4_PI3040.myX{1};
PI4020_X = statisticsStruct.ab4_PI4020.myX{1};
H312b_X = statisticsStruct.ab4_H312b.myX{1};
PID_X = statisticsStruct.ab4_PID.myX{1};

H211PI_T = statisticsStruct.ab4_H211PI.myT{1};
H2111b_T = statisticsStruct.ab4_H2111b.myT{1};
PI3333_T = statisticsStruct.ab4_PI3333.myT{1};
PI3040_T = statisticsStruct.ab4_PI3040.myT{1};
PI4020_T = statisticsStruct.ab4_PI4020.myT{1};
H312b_T = statisticsStruct.ab4_H312b.myT{1};
PID_T = statisticsStruct.ab4_PID.myT{1};

H211PI_H = H211PI_T(2:end) - H211PI_T(1:end-1);
H2111b_H = H2111b_T(2:end) - H2111b_T(1:end-1);
PI3333_H = PI3333_T(2:end) - PI3333_T(1:end-1);
PI3040_H = PI3040_T(2:end) - PI3040_T(1:end-1);
PI4020_H = PI4020_T(2:end) - PI4020_T(1:end-1);
H312b_H = H312b_T(2:end) - H312b_T(1:end-1);
PID_H = PID_T(2:end) - PID_T(1:end-1);

lambda = -p;
limit = -3/10;

clf
figure(1)
plot(H211PI_T(1:end-1),H211PI_H*lambda,'-o','MarkerSize',2)
hold on
plot(H2111b_T(1:end-1),H2111b_H*lambda,'-*','MarkerSize',4)
plot(PI3333_T(1:end-1),PI3333_H*lambda,'-')
plot(PI3040_T(1:end-1),PI3040_H*lambda,'-.')
plot(PI4020_T(1:end-1),PI4020_H*lambda,':')
plot(H312b_T(1:end-1),H312b_H*lambda,'-x','MarkerSize',4)
plot(PID_T(1:end-1),PID_H*lambda,'--x','MarkerSize',4)

plot(tspan,[limit,limit])

legend(nameFilterVec);

figure(2)
plot(H211PI_T,H211PI_X)
hold on
plot(H2111b_T,H2111b_X)
plot(PI3333_T,PI3333_X)
plot(PI3040_T,PI3040_X)
plot(PI4020_T,PI4020_X)
plot(H312b_T,H312b_X)
plot(PID_T,PID_X)

T = linspace(tspan(1),tspan(2));
plot(T,sol(T))

legend(nameFilterVec,'real');