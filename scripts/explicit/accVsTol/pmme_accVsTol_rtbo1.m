%Problem: rtbo1
clear all
addpath('../../..')
warning('off','all')

%Problem specific
[f,x0,tspan,jac,sol] = ode.getProblem('rtbo1')

%Create method name vector
%nameMethodVec = {'edc22','edc23','edc33','edc24', 'edc34','edc45','ab1','ab2','ab3','ab4','ab5','ab6','edf1','edf2','edf3','edf4','edf5','edf6'};
nameMethodVec = {'ab2','ab3','ab4','ab5','edf2','edf3','edf4','edf5'};

%Create filter name vector
%nameFilterVec = {'H211PI','H2111b','PI3333','PI3040','PI4020','H312b'};
nameFilterVec = {'H211PI','H211b','PI3333','PI3040','PI4020','H312b'};

%Tolerance defs.
minTol = 4;
maxTol = 10;
nbrOfTols = 100;

%How do you want to calc. the error?
%Availible alternitives are:
%   'abserr' and 'relerr'
errorCalcMode = 'abserr';

%Plot title
plotTitle = sprintf('RTBo1, nbrOfTols = %d, pmme',nbrOfTols)

%Creating mySolver
mySolver = @(f,tspan,x0,method,filter,tol,relerr,norm) solvers.pmme(f,tspan,x0,...
    'methodname',method,...
    'tol',tol,...
    'filtername',filter,...
    'perc',[0.8,1.2],...
    'unit',0,...
    'relerr', relerr,...
    'norm',norm,...
    'usebypass',true);
            
%Creating refSolver
refSolver = 'ode113';
optionsRefSolver = odeset('relTol',1e-13,'absTol',1e-16);

%Main method
[statisticsStruct] = tools.accVsTol_loop(f,tspan,x0,nameMethodVec,nameFilterVec,minTol,maxTol,nbrOfTols,mySolver,refSolver,...
    'refoptions',optionsRefSolver,...
    'errorcalcmode',errorCalcMode);
statisticsStruct.plotTitle = plotTitle;