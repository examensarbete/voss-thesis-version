%Template script for mainPlotFunc.m
addpath('../..')
clear all

%Problem specifics
[f,x0,tspan,jac] = ode.getProblem('brusselator');

% --------------------------------------------------------------------------
% Choose method
% --------------------------------------------------------------------------
%The following explicit methods exist:
%   *'nystrom3','nystrom4','nystrom5'
%   *'edc22', 'edc23', 'edc33', 'edc24', 'edc34', 'edc45'
%   *'abx' (x \in Z^+, e.g., ab4)
%   *'edfx' (x \in Z^+, e.g., edf4)
methodName = 'AB5';

% --------------------------------------------------------------------------
% Choose filter
% --------------------------------------------------------------------------
%The following filters exist:
%   *'H211PI'
%   *'H211b'
%   *'PI3333'
%   *'PI3040'
%   *'PI4020'
%   *'H312b'
%   *'PID'
filterName ='H211PI';

% --------------------------------------------------------------------------
% Create mySolver
% --------------------------------------------------------------------------
mySolver = @(f,tspan,x0,tol,relerr,normFunc) solvers.pmme(f,tspan,x0,...
    'methodname',methodName,...
    'tol',tol,...
    'filtername',filterName,...
    'perc',[0.8,1.2],...
    'unit',0,...
    'relerr', relerr,...
    'norm',normFunc,...
    'predfirststep',false,...
    'usebypass',true);

% --------------------------------------------------------------------------
% Choose matlab solver to compare with
% --------------------------------------------------------------------------
matlabSolverName = 'ode45';

% --------------------------------------------------------------------------
% What do you want to plot
% --------------------------------------------------------------------------
%These are all availible things to plot. Change
%value from 'true' do 'false' if you want to exclude some plot.
plot_mySol = false;

plot_refSol = false;

plot_stiffness = false;

plot_stepSize = false;

plot_errorReal = true;

plot_errorController = true;

plot_stepSizeRatios = true;

plotList = [plot_mySol, plot_refSol, plot_stiffness, plot_stepSize,...
    plot_errorReal, plot_errorController, plot_stepSizeRatios];

% --------------------------------------------------------------------------
% Other manatory settings
% --------------------------------------------------------------------------
tol = 1e-6;      %Set the tolerance
reference = 'ode113'; %Enter either the name of the matlab solver you
                         %want to use as reference (string) or the
                         %analytical solution (function handle).
                     
% --------------------------------------------------------------------------
% Other optional settings
% --------------------------------------------------------------------------
matlabOptions = odeset('relTol',1e-60,'absTol',tol);
optionsReference = odeset('relTol',1e-13,'absTol',1e-16);
relerr = 0;

% --------------------------------------------------------------------------
% mainPlotFunc
% --------------------------------------------------------------------------
[statistics,t,x] = tools.mainPlotFunc(f,tspan,x0,tol,mySolver,matlabSolverName,reference,...
    'refoptions',optionsReference,...
    'matlaboptions',matlabOptions,...
    'relerr',relerr,...
    'jac',jac,...
    'plotlist',plotList);
statistics