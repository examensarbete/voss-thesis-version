%Plot stability region for explicit methods
classes = fieldnames(explicitMethods);
nbrOfClasses = length(names);
ax = [-2 0 -1 1];
errorConstants = explicitMethods;

for i=1:nbrOfClasses
    class = explicitMethods.([classes{i}]);
    methods = fieldnames(class);
    nbrOfMethods = length(methods);
    for j=1:nbrOfMethods
        figure(i)
        method = class.([methods{j}]);
        alphas = method.alphas;
        betas = method.betas;
        p = length(alphas) - 1;
        C = tools.plotStabReg(alphas, betas,p,p);
        errorConstants.([classes{i}]).([methods{j}]) = C;
        axis(ax)
        hold on
    end
end