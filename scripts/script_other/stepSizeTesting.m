f = @ode.lotka; tspan = [0,2]; x0 = [1;1];

%Our filters
parv{1} = [1 1 0]/6;     % H211PI
parv{2} = [1 1 -1]/4;    % H2111b
parv{3} = [2 -1 0]/3;    % PI3333
parv{4} = [7 -4 0]/10;   % PI3040
parv{5} = [3 -1 0]/5;    % PI4020

method = @(k) thetas.AB(k,1,20);
tolerances = [1e-3,1e-5,1e-7,1e-9,1e-11,1e-13,1e-15,1e-17];
orders = [4,5,6,7,8];
medel = [];
mest = [];
data{1, 1} = 'H211PI';
data{2, 1} = 'H2111b';
data{3, 1} = 'PI3333';
data{4, 1} = 'PI3040';
data{5, 1} = 'PI4020';

for i=1:length(parv)
    j = 0;
    for order=orders
        j = j + 1;
        for tol=tolerances
            tol
            order
            data{i,1}
            %Solve the problem using pmme
            failed = false;
            try 
                [myT,myX,statistics]=solvers.pmme(f,tspan,x0,method,'TOL',tol,'parvec',...
                    parv{i},'perc',[0.7,1.2], 'unit',1, 'InitialOrder', order, 'relerr', true);
            catch ME
                failed = true;
            end
            
            if(failed)
                mest(end+1)=NaN; %A measure of the stability
                medel(end+1)=NaN; %A measure of the stability
            else
                %Calculate the step sizes
                myH = diff(myT);
                
                %Remove all values before tLim
                tLim = 0.1;
                tFreq = myT(myT > tLim);
                tFreq = myT;
                tFreq = tFreq(1:end-2);
                hFreq = myH(length(myT) - length(tFreq) - 1:end-1);
                
                %Interpolate such that we get equidistant stepsizes
                %(otherwise we can't use fft)
                tInter = linspace(tFreq(1),tFreq(end),statistics.numberOfSteps*2);
                xInter = interp1(tFreq,hFreq,tInter);
                
                %Use FFT to analyze the stepsize sequence
                X = fft(xInter);
                X_abs = abs(X); %The amplitudes of the freqs.
                nbrOfBins = length(X_abs);
                X_abs = X_abs(1:nbrOfBins/2);   %Just want half of X_abs since
                nbrOfBins = nbrOfBins/2;        %it's mirrored
                fs = length(tInter)/(tInter(end) - tInter(1)); %The samp. freq.
                Freq = linspace(0,fs/2,nbrOfBins);
                mest(end+1)=max(Freq(X_abs/max(X_abs) > 0.0005)); %A measure of the stability
                medel(end+1)=mean(Freq(Freq < 500).*X_abs(Freq<500)/max(X_abs)); %A measure of the stability
            end
        end
        matrixMest(j,:) = mest;
        matrixMedel(j,:) = medel;
        mest = [];
        medel = [];
    end
    data{i,2} = matrixMest;
    data{i,3} = matrixMedel;
end
