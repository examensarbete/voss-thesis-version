% Script for running the testlibrary using the matlab solvers
%
% nbr of stiff: 10
% nbr of both stiff and non-stiff dep. on var.: 2
% nbr of non-stiff: 7

clear all
problem = 'twoexponentials'

switch lower(problem)
% --------------------------------------------------------------------------
% Stiff problems
% --------------------------------------------------------------------------
    %%Testproblem 1 -- HIRES (stiff)
    case 'hires'
        [f,x0,tspan] = ode.getProblem('hires');
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode45(f,tspan,x0, options);
        plot(T,Y);
        
    %%Testproblem 2 -- Pollution (stiff)
    case 'pollution'
        [f,x0,tspan] = ode.getProblem('pollution');
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode23s(f,tspan,x0, options);
        plot(T,Y);
        
    %%Testproblem 3 -- ringModulator (stiff)(get a solution that's almost the same as in the Bari-report)
    case 'ringmodulator' %Takes long time to solve
        [f,x0,tspan] = ode.getProblem('ringModulator');
        options = odeset('relTol',1e-4,'absTol',1e-2);
        [T,Y] = ode23s(f,tspan,x0, options);
        plot(T,Y);
        
    %%Testproblem 4 -- medicalAkzoNobel (stiff)
    case 'medicalakzonobel'
        n = 200;
        [f,x0,tspan] = ode.getProblem('medicalAkzoNobel',n);
        options = odeset('relTol',1e-3,'absTol',1e-6);
        [T,Y] = ode23s(f,tspan,x0, options);
        plot(T,Y);
        
    %%Testproblem 5 -- EMEP (stiff)
    case 'emep'
        [f,x0,tspan] = ode.getProblem('emep');
        options = odeset('relTol',1e-1,'absTol',1e-6);
        [T,Y] = ode23s(f,tspan,x0, options);
        plot(T,Y);
        %axis([1e-4 1e12 1e-25 1e0]);
        
    %%Testproblem 9 -- oregonator (stiff)
    case 'oregonator'
        [f,x0,tspan] = ode.getProblem('oregonator');
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode23s(f,tspan,x0, options);
        plot(T,Y);
        
    %%Testproblem 10 -- robertson (stiff)
    case 'robertson'
        [f,x0,tspan] = ode.getProblem('robertson');
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode23s(f,tspan,x0, options);
        semilogx(T,Y);
        axis([1e-4 1e12 0 1]);
    
    %%Testproblem 11 -- E5 (stiff) (Seems to work now, though you have to have really small tolerances)
    case 'e5'
        [f,x0,tspan] = ode.getProblem('E5');
        options = odeset('relTol',1e-6,'absTol',1e-24);
        [T,Y] = ode23s(f,tspan,x0, options);
        loglog(T,Y);
        axis([1e-4 1e12 1e-25 1e0]);
    
% --------------------------------------------------------------------------
% Stiff and non-stiff problems (dep. on var.)
% --------------------------------------------------------------------------
        
    %%Testproblem 8 -- vdpol (stiff and non-stiff depending on value of \mu)
    case 'vdpol'
        mu = 70;
        [f,x0,tspan] = ode.getProblem('vdpol',mu);
        %tspan(2) = 5*mu
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode23s(f,tspan,x0, options);
        plot(T,Y);
    
    %%Testproblem 13 -- flameProp (stiff and non-stiff depending on value of y0)
    case 'flameprop'
        x0 = 1e-5;
        [f,x0,tspan] = ode.getProblem('flameprop',x0);
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode23s(f,tspan,x0, options);
        plot(T,Y);
   
    %%Testproblem 14 -- decayingExponential (non-stiff)
    case 'decayingexpo'
        d = 10;
        [f,x0,tspan] = ode.getProblem('decayingExpo',d);
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode45(f,tspan,x0, options);
        plot(T,Y);
        
    %%Testproblem 15 -- twoExponentials (stiff) 
    case 'twoexponentials'
        lambda = [-10,-1000];
        [f,x0,tspan] = ode.getProblem('twoExponentials',lambda);
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode23s(f,tspan,x0, options);
        semilogx(T,Y);
        %axis([1e-6 1e1 0 2]);
        
    %%Testproblem f1 (stiff and non-stiff depending on value of mu)
    case 'f1'
        mu = 1;
        [f,x0,tspan,jac,sol] = ode.getProblem('f1',mu);
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode15s(f,tspan,x0, options);
        plot(T,Y);
        
% --------------------------------------------------------------------------
% Non-stiff problems
% --------------------------------------------------------------------------
        
    %%Testproblem 6 -- pleiades (non-stiff)
    case 'pleiades'
        [f,x0,tspan] = ode.getProblem('pleiades');
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode45(f,tspan,x0, options);
        plot(T,Y);
        
    %%Testproblem 7 -- Beam (non-stiff)(the solution is almost correct, my amplitudes are a bit higher then Bari's)
    case 'beam'
        n = 10;
        [f,x0,tspan] = ode.getProblem('beam',n);
        options = odeset('relTol',1e-6,'absTol',1e-10);
        [T,Y] = ode45(f,tspan,x0, options);
        plot(T,Y);
        
    %%Testproblem 12 -- lotka (non-stiff)
    case 'lotka'
        cycles = 4;
        [f,x0,tspan] = ode.getProblem('lotka',4);
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode45(f,tspan,x0, options);
        plot(T,Y);
    
    %%Testproblem 16 -- lorenz (non-stiff)
    case 'lorenz'
        [f,x0,tspan] = ode.getProblem('lorenz');
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode45(f,tspan,x0, options);
        plot(T,Y);
    
    %%Testproblem 17 -- brusselator (non-stiff)
    case 'brusselator'
        [f,x0,tspan] = ode.getProblem('brusselator');
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode45(f,tspan,x0, options);
        plot(T,Y);
     
    %%Testproblem RTBo1 (non-stiff)
    case 'rtbo1'
        [f,x0,tspan] = ode.getProblem('rtbo1');
        options = odeset('relTol',1e-6,'absTol',1e-6);
        [T,Y] = ode45(f,tspan,x0, options);
        plot(T,Y);
end