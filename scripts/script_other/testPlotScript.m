% plotscript1
tolerances = [1e-3,1e-5,1e-7,1e-9,1e-11,1e-13,1e-15,1e-17];
orders = [4,5,6,7,8];
styles = {'-','--',':','-.','-x'};

%Plot for filter H211PI
figure(1)
for i=1:5
    semilogx(tolerances,data{1,3}(i,:),styles{i})
    hold on
end
title('H211PI')
legend('order 4','order 5','order 6','order 7','order 8')
xlim([tolerances(end),tolerances(1)]);

%Plot for filter H211b
figure(2)
for i=1:5
    semilogx(tolerances,data{2,3}(i,:),styles{i})
    hold on
end
title('H211b')
legend('order 4','order 5','order 6','order 7','order 8')
xlim([tolerances(end),tolerances(1)]);

%Plot for filter PI3333
figure(3)
for i=1:5
    semilogx(tolerances,data{3,3}(i,:),styles{i})
    hold on
end
title('PI3333')
legend('order 4','order 5','order 6','order 7','order 8')
xlim([tolerances(end),tolerances(1)]);

%Plot for filter PI3040
figure(4)
for i=1:5
    semilogx(tolerances,data{4,3}(i,:),styles{i})
    hold on
end
title('PI3040')
legend('order 4','order 5','order 6','order 7','order 8')
xlim([tolerances(end),tolerances(1)]);

%Plot for filter PI4020
figure(5)
for i=1:5
    semilogx(tolerances,data{5,3}(i,:),styles{i})
    hold on
end
title('PI4020')
legend('order 4','order 5','order 6','order 7','order 8')
xlim([tolerances(end),tolerances(1)]);
