% File for running pmme for E(k) method, k=2-7
% k = length(theta) + 1;

warning('off','all')
clear all
close all

theta = thetas.getMethodVec('AB3')

order = length(theta) + 1;

%f = @ode.lotka;
% tspan = [0,2];
% x0 = [1;1];

mu=10;
f = @(t,y)ode.vdpol(t,y,mu);
tspan = [0,4*mu];
x0 = [2;0];

%f = @ode.pollution;
%tspan = [0,2];
%x0 = [0; 0.2; 0; 0.04; 0; 0; 0.1; 0.3; 0.01; 0; 0; 0; 0; 0; 0; 0; 0.007; 0; 0; 0];

% f = @ode.oregonator;
% tspan = [0,360];
% x0 = [1; 2; 3];

%f = @ode.pleiades;
%tspan = [0,3];
%x_0 = [3; 3; -1; -3; 2; -2; 2];
%y_0 = [3; -3; 2; 0; 0; -4; 4];
%xd_0 = [0; 0; 0; 0; 0; 1.75; -1.5];
%yd_0 = [0; 0; 0; -1.25; 1; 0; 0];
%x0 = [x_0; y_0; xd_0; yd_0];
 
% f = @ode.flameprop;
% x0 = 1e-3;
% tspan = [0,0.6*2/x0];

 parv = [1 1 0]/6;     % H211PI
% parv = [1 1 -1]/4;    % H211b
% parv = [2 -1 0]/3;    % PI3333
% parv = [7 -4 0]/10;   % PI3040
% parv = [3 -1 0]/5;    % PI4020

% a = 1;
% parv = [a -a/2 -a/2 0 0]; %Josefine's filter

%b = 8;
%parv = [1 2 1 -3 -1]/b; %H312b

method = @(k) theta;
tol = 1e-3;
%Start solving the problem
[myT,myX,statistics]=solvers.pmme(f,tspan,x0,method,'TOL',tol,'parvec',...
    parv,'perc',[0.7,1.2], 'unit',0, 'InitialOrder', order, 'relerr', true);
display('Done with solving using pmme')
options = odeset('relTol',tol,'absTol',tol*1e-3);
[matlabT,matlabX] = ode23s(f,tspan,x0,options);
display('Done with solving using matlab, first try')

matlabH=diff(matlabT);
mine_matlab = [statistics.numberOfSteps length(matlabT)]
rejected_steps = statistics.rejectedSteps
nbrOfStepsWithrTooBig = statistics.nbrStepsRTooBig
myH = diff(myT);

%Plotting the step sizes
figure(1)
semilogy(myT(1:end-1),myH,'b',matlabT(1:end-1),matlabH,'r')
title('step-sizes')
legend('mine','matlab')
ylim([1e-5 1e4])
grid on

%References
% optionsReference = odeset('relTol',1e-12,'absTol',1e-15);
% [matlabTRefMy,matlabXRefMy] = ode45(f,myT,x0,optionsReference);
% [matlabTRefMatlab,matlabXRefMatlab] = ode45(f,matlabT,x0,optionsReference);
% 
% rerrorMy = abs(matlabXRefMy-myX)./abs(matlabXRefMy);
% rerrorMatlab = abs(matlabXRefMatlab-matlabX)./abs(matlabXRefMatlab);

%Plotting the absolute error
% figure(2)
% semilogy(myT(1:end-1),rerrorMy(1:end-1,1),'b',matlabT(1:end-1),...
%     rerrorMatlab(1:end-1,1),'r')
% title('Absolute error')
% legend('mine','matlab')
% ylim([1e-16 1e0])
% grid on

% Hm   = 0.5*myX(:,1)-0.5*log(myX(:,1))+0.3*myX(:,2)-0.1*log(myX(:,2));
% H45 = 0.5*matlabX2(:,1)-0.5*log(matlabX2(:,1))+0.3*matlabX2(:,2)-0.1*log(matlabX2(:,2));
%
% % %Plotting the invariant
% figure(3)
% loglog(myT,abs(Hm/0.8-1),'b',myT,abs(H45/0.8-1),'r');
% title('Invariant Lotka-Volterra')
% legend('mine','matlab')
% grid on

% %Plotting the solution (phase-plot)
%  figure(4)
%  plot(myX(:,1),myX(:,2),'b.')
%  hold on
%  plot(matlabXRefMy(:,1),matlabXRefMy(:,2),'r.')
%  title('solution with IMMk-k')
%  legend('mine','matlab')
%  grid on

% %Plotting the solution
figure(5)
plot(myT,myX)
hold on
plot(matlabT,matlabX)
%ylim([0,12e4])
title('solution')
%legend('mine','matlab')

% hold off
% figure(2)
%tFreq = myT(myT > 0.2);
% tFreq = myT;
% tFreq = tFreq(1:end-2);
% hFreq = myH(length(myT) - length(tFreq) - 1:end-1);

% tInter = linspace(tFreq(1),tFreq(end),statistics.numberOfSteps*2);
% xInter = interp1(tFreq,hFreq,tInter);
% figure(6)
% plot(tInter,xInter)
%ylim([1e-5 1e4])
% hold on
% plot(t,x,'x')
% % 
% X = fft(xInter);
% X_abs = abs(X);
% nbrOfBins = length(X_abs);
% X_abs = X_abs(1:nbrOfBins/2);
% nbrOfBins = nbrOfBins/2;
% fs = length(tInter)/(tInter(end) - tInter(1));
% Freq = linspace(0,fs/2,nbrOfBins);
% plot(Freq,X_abs/max(X_abs))
% xlim([0,fs/2]);
% max(Freq(X_abs/max(X_abs) > 0.0005));
% mean(Freq(Freq < 500*tFreq(end)).*X_abs(Freq<500*tFreq(end))/max(X_abs));
