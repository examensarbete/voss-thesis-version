fs = 1000; %Sampling frequence
numSamples = 1000;
rNums = rand(1,numSamples);
t = [0];
for i=1:numSamples
    t(i+1) = sum(rNums(1:i));
end
t = t/numSamples;

f1 = 5; %frequance of sin-signal
f2 = 150;
A = 10; %amplitude of sin-signal
B = 0.5;
x = A*sin(2*pi*f1*t) + B*sin(2*pi*f2*t);
%plot(t,x)
% 
tDiff_min = min(diff(t));
tInter = 0:tDiff_min/4:t(end);
xInter = interp1(t,x,tInter);
% plot(tInter,xInter)
% hold on
% plot(t,x,'x')
% % 
X = fft(xInter);
X_abs = abs(X);
nbrOfBins = length(X_abs);
X_abs = X_abs(1:nbrOfBins/2);
nbrOfBins = nbrOfBins/2;
fs = length(tInter)/(tInter(end) - tInter(1));
Freq = linspace(0,fs/2,nbrOfBins);
plot(Freq,X_abs)
xlim([0,160]);