%Template script for mainPlotFunc.m
addpath('../..')
clear all

%Problem specifics
[f,x0,tspan,jac] = ode.getProblem('B3');

% --------------------------------------------------------------------------
% Choose method
% --------------------------------------------------------------------------
%The following explicit methods exist:
%   *'milne2','milne4'
%   *'idc23', 'idc24', 'idc34', 'idc45', 'idc56'
%   *amx (x \in Z^+)
%   *dcbdfx (x \in Z^+)
methodName = 'am5';

% --------------------------------------------------------------------------
% Choose filter
% --------------------------------------------------------------------------
%The following filters exist:
%   *'H211PI'
%   *'H2111b'
%   *'PI3333'
%   *'PI3040'
%   *'PI4020'
%   *'H312b'
%   *'PID'
filterName = 'H211PI';

% --------------------------------------------------------------------------
% Create mySolver
% --------------------------------------------------------------------------
mySolver = @(f,tspan,x0,tol,relerr,normFunc) solvers.pmmip(f,tspan,x0,...
    'methodname',methodName,...
    'tol',tol,...
    'filtername',filterName,...
    'perc',[0.8,1.2],...
    'unit',0,...
    'relerr', relerr,...
    'norm',normFunc,...
    'jac',jac,...
    'predfirststep',false,...
    'usebypass',true);

% --------------------------------------------------------------------------
% Choose matlab solver to compare with
% --------------------------------------------------------------------------
matlabSolverName = 'ode113';

% --------------------------------------------------------------------------
% What do you want to plot
% --------------------------------------------------------------------------
%These are all availible things to plot. Change
%value from 'true' do 'false' if you want to exclude some plot.
plot_mySol = false;

plot_refSol = false;

plot_stiffness = false;

plot_stepSize = false;

plot_errorReal = true;

plot_errorController = true;

plot_stepSizeRatios = true;

plotList = [plot_mySol, plot_refSol, plot_stiffness, plot_stepSize,...
    plot_errorReal, plot_errorController, plot_stepSizeRatios];

% --------------------------------------------------------------------------
% Other manatory settings
% --------------------------------------------------------------------------
tol = 1e-7;
reference = 'ode113'; %Enter either the name of the matlab solver you
                     %want to use as reference or the analytical solution
                     %as a function handle
                     
% --------------------------------------------------------------------------
% Other optional settings
% --------------------------------------------------------------------------
optionsReference = odeset('relTol',1e-13,'absTol',1e-16);
matlabOptions = odeset('relTol',tol,'absTol',tol*1e-3);
relerr = 0;

% --------------------------------------------------------------------------
% mainPlotFunc
% --------------------------------------------------------------------------
[statistics,t,x] = tools.mainPlotFunc(f,tspan,x0,tol,mySolver,matlabSolverName,reference,...
    'refoptions',optionsReference,...
    'matlaboptions',matlabOptions,...
    'relerr',relerr,...
    'plotlist',plotList,...
    'jac',jac);
statistics