%Scrit to run over night

clf
clear all
run pmmip_accVsTol_decayingExpo.m
% %dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% %fileName=strcat('~/mastersThesis/codeBase/data/decayingExpo_pmmip_p=',p,'_',dateStr);
% %fileName = strcat(fileName,'.mat');
% %save(fileName, 'statisticsStruct');
tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmip_accVsTol_flameprop.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/flameprop_pmmip_x0=',x0,'_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmip_accVsTol_Lotka.m
% %dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% %fileName=strcat('~/mastersThesis/codeBase/data/lotka_',dateStr);
% %fileName = strcat(fileName,'.mat');
% %save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmip_accVsTol_VanDerPol.m
% %dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% %fileName=strcat('~/mastersThesis/codeBase/data/vanDerPol_pmmip_mu=',mu,'_',dateStr);
% %fileName = strcat(fileName,'.mat');
% %save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmip_accVsTol_B3.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/B3_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmip_accVsTol_pleiades.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/pleiades_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmip_accVsTol_rtbo1.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/rtbo1_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmip_accVsTol_lorenz.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/lorenz_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmip_accVsTol_brusselator.m
% %dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% %fileName=strcat('~/mastersThesis/codeBase/data/brusselator_',dateStr);
% %fileName = strcat(fileName,'.mat');
% %save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')