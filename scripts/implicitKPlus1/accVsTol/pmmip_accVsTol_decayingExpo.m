%Script to plot Lotka
clear all
addpath('../../..')
warning('off','all')

%Problem specific
p = 1
[f,x0,tspan,jac,sol] = ode.getProblem('decayingExpo',p)

%Create method name vector
%All availible methods (for now) are:
%   {'milne2','milne4','idc23','idc24','idc34','idc45','idc56','am1','am2','am3','am4','am5','am6','dcbdf1','dcbdf2','dcbdf3','dcbdf4','dcbdf5'};
nameMethodVec = {'am2','am3','am4','am5'};

%Create filter name vector
%All availible filters (for now) are:
%   {'H211PI','H2111b','PI3333','PI3040','PI4020','H312b'}
nameFilterVec = {'H211PI','H211b','PI3333','PI3040','PI4020','H312b'};

%Tolerance defs.
minTol = 4;
maxTol = 10;
nbrOfTols = 100;

%How do you want to calc. the error?
%Availible alternitives are:
%   'abserr' and 'relerr'
errorCalcMode = 'abserr';

%Plot title
plotTitle = sprintf('Decaying exponent, p=%d, nbrOfTols = %d, pmmip',p,nbrOfTols)

%Creating mySolver
mySolver = @(f,tspan,x0,method,filter,tol,relerr,norm) solvers.pmmip(f,tspan,x0,...
    'methodname',method,...
    'tol',tol,...
    'filtername',filter,...
    'perc',[0.8,1.2],...
    'unit',0,...
    'relerr', relerr,...
    'norm',norm,...
    'jac',jac);

%Creating refSolver
refSolver = 'ode113';
optionsRefSolver = odeset('relTol',1e-13,'absTol',1e-16);

%Main method
[statisticsStruct] = tools.accVsTol_loop(f,tspan,x0,nameMethodVec,nameFilterVec,minTol,maxTol,nbrOfTols,mySolver,refSolver,...
    'refoptions',optionsRefSolver,...
    'errorcalcmode',errorCalcMode,...
    'analyticalfunction',sol);
statisticsStruct.plotTitle = plotTitle;