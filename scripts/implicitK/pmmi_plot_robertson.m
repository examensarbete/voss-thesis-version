% test script pmme van der pol
addpath('../..')
clear all

%Problem specifics
[f,x0,tspan,jac] = ode.getProblem('robertson');

% -------------------------------------------------------------------------
% Choose method
% --------------------------------------------------------------------------
%The following implicit methods exist:
%   *kregel
%   *bdfx (x \in Z^+, x < 7)
methodName = 'bdf5';

% --------------------------------------------------------------------------
% Choose filter
% --------------------------------------------------------------------------
%The following filters exist:
%   *'H211PI'
%   *'H211b'
%   *'PI3333'
%   *'PI3040'
%   *'PI4020'
%   *'H312b'
%   *'PID'
filterName ='H211PI';

% --------------------------------------------------------------------------
% Create mySolver
% --------------------------------------------------------------------------
mySolver = @(f,tspan,x0,tol,relerr,normFunc) solvers.pmmi(f,tspan,x0,...
    'methodname',methodName,...
    'tol',tol,...
    'filtername',filterName,...
    'perc',[0.8,1.2],...
    'unit',0,...
    'relerr',relerr,...
    'norm',normFunc,...
    'jac',jac,...
    'usebypass',true);

% --------------------------------------------------------------------------
% Choose matlab solver to compare with
% --------------------------------------------------------------------------
matlabSolverName = 'ode15s';

% --------------------------------------------------------------------------
% What do you want to plot
% --------------------------------------------------------------------------
%These are all availible things to plot. Change
%value from 'true' do 'false' if you want to exclude some plot.
plot_mySol = false;

plot_refSol = false;

plot_stiffness = false;

plot_stepSize = false;

plot_errorReal = true;

plot_errorController = true;

plot_stepSizeRatios = true;

plotList = [plot_mySol, plot_refSol, plot_stiffness, plot_stepSize,...
    plot_errorReal, plot_errorController, plot_stepSizeRatios];

% --------------------------------------------------------------------------
% Other manatory settings
% --------------------------------------------------------------------------
tol = 1e-7;
reference = 'ode15s'; %Enter either the name of the matlab solver you
                     %want to use as reference or the analytical solution
                     %as a function handle
                     
% --------------------------------------------------------------------------
% Other optional settings
% --------------------------------------------------------------------------
optionsReference = odeset('relTol',1e-13,'absTol',1e-16);
matlabOptions = odeset('relTol',1e-60,'absTol',tol,'BDF','on','MaxOrder',5);
relerr = 0;

% --------------------------------------------------------------------------
% mainPlotFunc
% --------------------------------------------------------------------------
[statistics,t,x] = tools.mainPlotFunc(f,tspan,x0,tol,mySolver,matlabSolverName,reference,...
    'refoptions',optionsReference,...
    'matlaboptions',matlabOptions,...
    'relerr',relerr,...
    'jac',jac,...
    'plotlist',plotList);
statistics