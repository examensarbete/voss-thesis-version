%Script to run over night

% clf
% clear all
% run pmmi_accVsTol_VanDerPol.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/VanDerPol_pmmi_mu=',mu,'_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmi_accVsTol_Robertson.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/Robertson_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmi_accVsTol_ringModulator.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/ringModulator_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmi_accVsTol_pollution.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/pollution_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmi_accVsTol_oregonator.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/decayingExpo_pmmi_p=',p,'_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmi_accVsTol_decayingExpo.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/decayingExpo_pmmi_p=',p,'_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmi_accVsTol_medAkzoNob.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/medAkzoNob_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmi_accVsTol_hires.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/hires_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmi_accVsTol_flameProp.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/flameProp_x0=',x0,'_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmi_accVsTol_emep.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/emep_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmi_accVsTol_e5.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/e5_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')

% clf
% clear all
% run pmmi_accVsTol_beam.m
% dateStr = datestr(now,'yyyy-mm-dd_hh:MM:ss');
% fileName=strcat('~/mastersThesis/codeBase/data/beam_',dateStr);
% fileName = strcat(fileName,'.mat');
% save(fileName, 'statisticsStruct');
% tools.plotFromStruct(statisticsStruct,'succeededTols','errorFromAccVsTol',1, 'loglog')
% tools.plotFromStruct(statisticsStruct,'succeededTols','numberOfSteps',1, 'loglog')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myH')
% tools.plotFromStructoverTime(statisticsStruct,nameMethodVec,nameFilterVec,[1e-7],'myR')