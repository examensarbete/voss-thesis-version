%Script to ...
clear all
clf
addpath('../../..')
warning('off','all')

%Problem specific
[f,x0,tspan,jac,sol] = ode.getProblem('oregonator')

%Create method name vector
%All availible methods (for now) are:
%   {'kregel','bdf1','bdf2','bdf3','bdf4','bdf5','bdf6'};
nameMethodVec = {'bdf3','bdf4','bdf5'};

%Create filter name vector
%All availible filters (for now) are:
%   {'H211PI','H211b','PI3333','PI3040','PI4020','H312b'}
nameFilterVec = {'H211PI','H211b','PI3333','PI3040','PI4020','H312b'};

%Tolerance defs.
minTol = 4;
maxTol = 10;
nbrOfTols = 100;

%How do you want to calc. the error?
%Availible alternitives are:
%   'abserr' and 'relerr'
errorCalcMode = 'abserr';

%Plot title
plotTitle = sprintf('Oregonator, nbrOfTols = %d, pmmi',nbrOfTols)

%Creating mySolver
mySolver = @(f,tspan,x0,method,filter,tol,relerr,norm) solvers.pmmi(f,tspan,x0,...
    'methodname',method,...
    'tol',tol,...
    'filtername',filter,...
    'perc',[0.8,1.2],...
    'unit',0,...
    'relerr', relerr,...
    'norm',norm);

%Creating refSolver
refSolver = 'ode15s';
optionsRefSolver = odeset('relTol',1e-13,'absTol',1e-16);

%Main method
[statisticsStruct] = tools.accVsTol_loop(f,tspan,x0,nameMethodVec,nameFilterVec,minTol,maxTol,nbrOfTols,mySolver,refSolver,...
    'refoptions',optionsRefSolver,...
    'errorcalcmode',errorCalcMode);
statisticsStruct.plotTitle = plotTitle;